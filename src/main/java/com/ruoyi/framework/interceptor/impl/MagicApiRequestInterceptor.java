package com.ruoyi.framework.interceptor.impl;

import org.springframework.stereotype.Component;
import org.ssssssss.magicapi.core.context.RequestEntity;
import org.ssssssss.magicapi.core.interceptor.RequestInterceptor;
import org.ssssssss.magicapi.core.model.ApiInfo;
import org.ssssssss.magicapi.core.model.Options;

import com.ruoyi.common.utils.SecurityUtils;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * magic-api 接口鉴权
 * @author baize
 *
 */
@Component
@Slf4j
public class MagicApiRequestInterceptor implements RequestInterceptor {

	/**
	 * 当返回对象时，直接将此对象返回到页面，返回null时，继续执行后续操作
	 * @param requestEntity	是本次请求中所有的参数信息封装的对象，可以获取实际请求参数等信息
	 */
	@Override
	public Object preHandle(RequestEntity requestEntity) throws Exception {
	 // 需要注意，ApiInfo 是页面上定义的信息，里面的值并不是实际请求的参数！！！
	       // 拦截请求并直接返回结果，不继续后续代码
	       // 需要注意的是，拦截器返回的结果不会被包裹一层json值，也不会走ResultProvider
	       // return new JsonBean<>(100,"拦截器返回");
	       // 放开请求，执行后续代码
	    String username = "";
		if(StpUtil.isLogin())
		{
			username = SecurityUtils.getLoginUser().getUserName();
		}
		ApiInfo info = requestEntity.getApiInfo();
		log.info("{} 请求接口：{}", username, info.getName());
		// 接口选项配置了需要登录
		if ("true".equals(info.getOptionValue(Options.REQUIRE_LOGIN))) {
			StpUtil.checkLogin();
		}
		String role = info.getOptionValue(Options.ROLE);
		if (StrUtil.isNotBlank(role)) {
			StpUtil.checkRole(role);
		}
		String permission = info.getOptionValue(Options.PERMISSION);
		if (StrUtil.isNotBlank(permission)) {
			StpUtil.hasPermission(permission);
		}
		return null;
	}

	 /**
     * 执行完毕之后执行
     * @param RequestEntity 是本次请求中所有的参数信息封装的对象，可以获取实际请求参数等信息
     * @return 返回到页面的对象,当返回null时执行后续拦截器，否则直接返回该值，不执行后续拦截器
     */
   @Override
   public Object postHandle(RequestEntity requestEntity, Object returnValue) {
       ApiInfo info = requestEntity.getApiInfo();
       log.info("{} 执行完毕，返回结果:{}", info.getName(), returnValue);
       // 需要注意，ApiInfo 是页面上定义的信息，里面的值并不是实际请求的参数！！！
       // 拦截请求并直接返回结果，不继续后续代码
       // 需要注意的是，拦截器返回的结果不会被包裹一层json值，也不会走ResultProvider
       // return new JsonBean<>(100,"拦截器返回");
       // 放开请求，执行后续代码
       return null;
   }
}