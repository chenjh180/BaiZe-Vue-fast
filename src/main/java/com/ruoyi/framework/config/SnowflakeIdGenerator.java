package com.ruoyi.framework.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import com.github.yitter.contract.IdGeneratorOptions;
import com.github.yitter.idgen.YitIdHelper;
import org.springframework.boot.CommandLineRunner;
/**
 * @author 作者： baize
 * @version 创建时间：2024年5月13日 上午1:17:33
 */
@Component
public class SnowflakeIdGenerator implements IdentifierGenerator,CommandLineRunner {
	
	@Value("${snowflake.workerId}")
    private static short workerId;
	
	@Value("${snowflake.datacenterId}")
    private short datacenterId;
	
	@Override
	public Long nextId(Object entity) {
		return nextId();
	}

	public Long nextId() {
		return YitIdHelper.nextId();
	}

	@Override
	public void run(String... args) throws Exception
	{
		IdGeneratorOptions options = new IdGeneratorOptions(workerId);
		YitIdHelper.setIdGenerator(options);
	}
}
