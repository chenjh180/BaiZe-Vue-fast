package com.ruoyi.framework.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.ToString;

/**
 * 微信小程序登录配置
 * 
 * @author baize
 */
@Component
@ConfigurationProperties(prefix = "wechat-mini")
@Data
@ToString
public class WeChatMiniConfig
{
    /**
     * 小程序 appId
     */
    private String appid;
    
    /**
     * 小程序 appSecret
     */
    private String secret;
}