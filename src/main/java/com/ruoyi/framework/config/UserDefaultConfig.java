package com.ruoyi.framework.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.ToString;

/**
 * 创建用户的默认信息
 * 
 * @author baize
 */
@Component
@ConfigurationProperties(prefix = "user-default")
@Data
@ToString
public class UserDefaultConfig
{
	/**
	 * 默认部门
	 */
	private Long dept;

	/**
	 * 默认角色
	 */
	private List<Long> roleIds = new ArrayList<>();
	
	/**
	 * 默认岗位
	 */
	private List<Long> postIds = new ArrayList<>();

	/**
	 * 默认性别：2 未知
	 */
	private String sex;

	/**
	 * 默认头像
	 */
	private String avatar;

	/**
	 * 默认昵称前缀
	 */
	private String nicknamePrefix;

	/**
	 * 默认昵称后缀类型 <br/> 
	 * str 随机字符串：大小写英文 <br/> 
	 * num 随机数字 <br/>
	 */
	private String nicknameSuffixType;

	/**
	 * 默认昵称后缀长度
	 */
	private Integer nicknameSuffixLength;
}