package com.ruoyi.framework.config;

import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.blueconic.browscap.BrowsCapField;
import com.blueconic.browscap.UserAgentParser;
import com.blueconic.browscap.UserAgentService;
import com.ruoyi.common.utils.http.UserAgent;

import lombok.extern.slf4j.Slf4j;

/**
 * @author 作者： baize
 * @version 创建时间：2024年5月24日 上午12:57:41
 */
@Configuration
@Slf4j
public class UserAgentConfig
{
	@Bean(name = "userAgent")
	public UserAgent userAgent()
	{
		try
		{
			UserAgentParser parser = new UserAgentService().loadParser(Arrays.asList(BrowsCapField.BROWSER, BrowsCapField.BROWSER_TYPE,
					BrowsCapField.BROWSER_MAJOR_VERSION, BrowsCapField.DEVICE_TYPE, BrowsCapField.PLATFORM,
					BrowsCapField.PLATFORM_VERSION, BrowsCapField.RENDERING_ENGINE_VERSION,
					BrowsCapField.RENDERING_ENGINE_NAME, BrowsCapField.PLATFORM_MAKER,
					BrowsCapField.RENDERING_ENGINE_MAKER));
			UserAgent agent = new UserAgent(parser);
			// 初次使用有大概10秒的初始化时间，所以先破一血
			final String userAgentStr = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.36";
			log.debug("init user before ...");
			log.debug("init user agent := {}" , agent.parseUserAgentString(userAgentStr));
			return agent;
		} catch (Exception e)
		{
			log.error("获取用户代理异常 {}", e);
		}
		return null;
	}
}
