package com.ruoyi.framework.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.ToString;

/**
 * 路由白名单
 * 
 * @author baize
 */
@Component
@ConfigurationProperties(prefix = "sa-router")
@Data
@ToString
public class SaRouterConfig
{
    /**
     * 白名单
     */
    private List<String> notMatch = new ArrayList<String>();
    
}