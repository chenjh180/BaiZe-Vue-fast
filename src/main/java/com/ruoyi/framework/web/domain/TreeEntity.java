package com.ruoyi.framework.web.domain;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Tree基类
 * 
 * @author ruoyi
 */
@Data
@ToString
@EqualsAndHashCode(callSuper=true)
@ApiModel(value = "TreeEntity" , description = "Tree基类")
public class TreeEntity<T> extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 父菜单名称 */
    @ApiModelProperty("父菜单名称")
    private String parentName;

    /** 父菜单ID */
    @ApiModelProperty("父菜单ID")
    private Long parentId;

    /** 显示顺序 */
    @ApiModelProperty("显示顺序")
    private Integer orderNum;

    /** 祖级列表 */
    @ApiModelProperty("祖级列表")
    private String ancestors;

    /** 子部门 */
    @ApiModelProperty("子部门")
    private List<T> children = new ArrayList<>();
}
