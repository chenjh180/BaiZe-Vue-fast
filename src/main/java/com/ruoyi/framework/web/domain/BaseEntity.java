package com.ruoyi.framework.web.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Entity基类
 * 
 * @author ruoyi
 */
@Data
@ToString
@EqualsAndHashCode(callSuper=true)
@ApiModel(value = "BaseEntity" , description = "Entity基类")
public class BaseEntity extends CommonEntity
{
    
    /**
     * 
     */
    private static final long serialVersionUID = 7512577503344036704L;
    public static final String CREATE_BY = "createBy";
    public static final String CREATE_TIME = "createTime";
    public static final String UPDATE_BY = "updateBy";
    public static final String UPDATE_TIME = "updateTime";

    /** 创建者 */
    @ApiModelProperty("创建者")
    @TableField(fill=FieldFill.INSERT)
    private String createBy;

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill=FieldFill.INSERT)
    private Date createTime;

    /** 更新者 */
    @ApiModelProperty("更新者")
    @TableField(fill=FieldFill.UPDATE)
    private String updateBy;

    /** 更新时间 */
    @ApiModelProperty("更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill=FieldFill.UPDATE)
    private Date updateTime;

    /** 备注 */
    @ApiModelProperty("备注")
    private String remark;
}
