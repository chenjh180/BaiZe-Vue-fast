package com.ruoyi.framework.web.domain.server;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/** 
* @author 作者： baize
* @version 创建时间：2024年5月23日 下午11:56:29 
*/
@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserAgentInfo
{
	private String userAgent;
	private String browser;
	private String browserType;
	private String browserMajorVersion;
	private String browserWithVersion;
	private String deviceType;
	private String platformVersion;
	private String renderingEngineMaker;
	private String renderingengineVersion;
	private String renderingEngineName;
	private String platformMaker;
	private String operatingSystem;
}
