package com.ruoyi.framework.web.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.ToString;

/**
 * 通用Entity
 * 
 * @author baize
 */
@Data
@ToString
@ApiModel(value = "CommonEntity" , description = "通用Entity")
public class CommonEntity implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = 181963532773932334L;

    /** 搜索值 */
    @JsonIgnore
    @ApiModelProperty("搜索值")
    private String searchValue;

    /** 请求参数 */
    @ApiModelProperty("请求参数")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @Getter(value=AccessLevel.NONE)
    private Map<String, Object> params;

    public Map<String, Object> getParams()
    {
        if (params == null)
        {
            params = new HashMap<>();
        }
        return params;
    }
}
