package com.ruoyi.framework.web.client;
/** 
* @author 作者： baize
* @version 创建时间：2024年5月19日 下午8:42:13 
*/

import com.dtflys.forest.annotation.Get;
import com.dtflys.forest.annotation.Query;
import com.ruoyi.framework.web.domain.Jscode2SessionResult;

/**
 * 微信小程序请求客户端
 * @author baize
 *
 */
public interface WeChatMiniClient
{
	@Get("https://api.weixin.qq.com/sns/jscode2session?grant_type=authorization_code")
	Jscode2SessionResult jscode2Session(@Query("appid") String appid,@Query("secret") String secret,@Query("js_code") String jscode);
}
