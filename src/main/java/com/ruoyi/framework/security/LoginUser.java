package com.ruoyi.framework.security;

import java.util.Set;

import com.alibaba.fastjson2.annotation.JSONField;
import com.ruoyi.project.system.domain.SysUser;

import lombok.Data;
import lombok.ToString;

/**
 * 登录用户身份权限
 * 
 * @author ruoyi
 */
@Data
@ToString
public class LoginUser {

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 部门ID
     */
    private Long deptId;

    /**
     * 用户唯一标识
     */
    private String token;

    /**
     * 登录时间
     */
    private Long loginTime;

    /**
     * 过期时间
     */
    private Long expireTime;

    /**
     * 登录IP地址
     */
    private String ipaddr;

    /**
     * 登录地点
     */
    private String loginLocation;

    /**
     * 浏览器类型
     */
    private String browser;

    /**
     * 操作系统
     */
    private String os;

    /**
     * 权限列表
     */
    private Set<String> permissions;

    /**
     * 用户信息
     */
    private SysUser user;
    
    /**
     * 登录设备类型
     */
    private String loginDevice;

    /**
     * 此token对应的LoginId
     */
    private String loginId;
    
    /**
     * 微信小程序的openid
     */
    private String openid;
    
    /**
     * 获取指定 token 剩余有效时间（单位: 秒，返回 -1 代表永久有效，-2 代表没有这个值）
     */
    private Long tokenTimeout;
    
    /**
     * 获取指定 token 剩余活跃有效期：这个 token 距离被冻结还剩多少时间（单位: 秒，返回 -1 代表永不冻结，-2 代表没有这个值或 token 已被冻结了）
     */
    private Long tokenActiveTimeout;
    
    /**
     * 原始的userAgent
     */
    private String userAgent;
    
    public LoginUser()
    {
    }

    public LoginUser(SysUser user, Set<String> permissions)
    {
        this.user = user;
        this.permissions = permissions;
    }

    public LoginUser(Long userId, Long deptId, SysUser user, Set<String> permissions)
    {
        this.userId = userId;
        this.deptId = deptId;
        this.user = user;
        this.permissions = permissions;
    }
    
    public LoginUser(Long userId, Long deptId, SysUser user)
    {
        this.userId = userId;
        this.deptId = deptId;
        this.user = user;
    }

    @JSONField(serialize = false)
    public String getPassword()
    {
        return user.getPassword();
    }

    public String getUsername()
    {
        return user.getUserName();
    }
}
