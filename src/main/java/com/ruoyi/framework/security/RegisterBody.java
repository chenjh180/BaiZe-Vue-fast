package com.ruoyi.framework.security;

import io.swagger.annotations.ApiModel;

/**
 * 用户注册对象
 * 
 * @author ruoyi
 */
@ApiModel(value = "RegisterBody" , description = "用户注册对象")
public class RegisterBody extends LoginBody
{

}
