package com.ruoyi.framework.security;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.ruoyi.common.xss.Xss;

import lombok.Data;
import lombok.ToString;

/**
 * 更新昵称
 * @author baize
 *
 */
@Data
@ToString
public class NickNameBody {
	/**
     * 昵称
     */
	@NotBlank(message = "昵称不能为空")
	@Xss(message = "用户昵称不能包含脚本字符")
    @Size(min = 0, max = 30, message = "用户昵称长度不能超过30个字符")
    private String nickName;
}
