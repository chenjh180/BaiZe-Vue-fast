package com.ruoyi.framework.security;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * 微信小程序登录参数
 * @author baize
 *
 */
@Data
@ToString
@ApiModel(value = "WeChatMiniLoginBody" , description = "微信小程序登录参数")
public class WeChatMiniLoginBody {
	/**
     * 登录 jscode
     */
	@NotBlank(message = "jscode不能为空")
	@ApiModelProperty("jscode")
    private String jscode;
}
