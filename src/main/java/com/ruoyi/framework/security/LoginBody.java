package com.ruoyi.framework.security;

import com.ruoyi.common.utils.sign.RsaUtils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.ToString;

/**
 * 用户登录对象
 * 
 * @author ruoyi
 */
@Data
@ToString
@ApiModel(value = "LoginBody" , description = "登录参数体")
public class LoginBody
{
    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    private String username;

    /**
     * 用户密码
     */
    @ApiModelProperty("用户密码")
    @Getter(value=AccessLevel.NONE)
    private String password;

    /**
     * 验证码
     */
    @ApiModelProperty("验证码")
    private String code;

    /**
     * 唯一标识
     */
    @ApiModelProperty("唯一标识")
    private String uuid;
    
    /**
     * 记住我
     */
    @ApiModelProperty("记住我")
    private Boolean rememberMe = false;

    public String getPassword() {
        try {
            return RsaUtils.decryptByPrivateKey(this.password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.password;
    }
}
