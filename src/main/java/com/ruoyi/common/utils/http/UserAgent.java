package com.ruoyi.common.utils.http;

import com.blueconic.browscap.BrowsCapField;
import com.blueconic.browscap.Capabilities;
import com.blueconic.browscap.UserAgentParser;
import com.ruoyi.framework.web.domain.server.UserAgentInfo;

import lombok.extern.slf4j.Slf4j;

/**
 * 浏览器用户代理解析
 * 
 * @author ruoyi
 */
@Slf4j
public class UserAgent
{
	private UserAgentParser userAgentParser;
	
	public UserAgent(UserAgentParser userAgentParser) {
		this.userAgentParser = userAgentParser;
	}
	// create a parser with the default fields
//	final UserAgentParser parser = new UserAgentService().loadParser(); // handle IOException and ParseException

	// or create a parser with a custom defined field list
	// the list of available fields can be seen inthe BrowsCapField enum
	

	// It's also possible to supply your own ZIP file by supplying a correct path to a ZIP file in the constructor.
	// This can be used when a new BrowsCap version is released which is not yet bundled in this package.
	// final UserAgentParser parser = new UserAgentService("E:\\anil\\browscap.zip").loadParser();

//	static
//    {
//        try
//        {
////        	parser = new UserAgentService().loadParser();
//            parser = new UserAgentService().loadParser(Arrays.asList(BrowsCapField.BROWSER, BrowsCapField.BROWSER_TYPE,
//	                BrowsCapField.BROWSER_MAJOR_VERSION,
//	                BrowsCapField.DEVICE_TYPE, BrowsCapField.PLATFORM, BrowsCapField.PLATFORM_VERSION,
//	                BrowsCapField.RENDERING_ENGINE_VERSION, BrowsCapField.RENDERING_ENGINE_NAME,
//	                BrowsCapField.PLATFORM_MAKER, BrowsCapField.RENDERING_ENGINE_MAKER));
//            // 初次使用有大概10秒的初始化时间，所以在这先使用一次
//            final String userAgentStr2 = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.36";
//            log.debug("init user agent :-> {} " , parseUserAgentString(userAgentStr2));
//        }
//        catch (Exception e)
//        {
//            log.error("获取用户代理异常 {}", e);
//        }
//    }

    public static void main(String[] args)
	{
//    	final String userAgentStr = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36";
//    	final String userAgentStr2 = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.36";
//    	UserAgentInfo userAgent = UserAgent.parseUserAgentString(userAgentStr);
//    	UserAgentInfo userAgent2 = UserAgent.parseUserAgentString(userAgentStr2);
//    	System.out.println(userAgent);
//    	System.out.println(userAgent2);
	}

	public UserAgentInfo parseUserAgentString(String userAgentString)
	{
		log.debug("UserAgent => " + userAgentString);
		final Capabilities capabilities = this.userAgentParser.parse(userAgentString);
		// the default fields have getters
		final String browser = capabilities.getBrowser();
		final String browserType = capabilities.getBrowserType();
		final String browserMajorVersion = capabilities.getBrowserMajorVersion();
		final String deviceType = capabilities.getDeviceType();
		final String platform = capabilities.getPlatform();
		final String platformVersion = capabilities.getPlatformVersion();
		// the custom defined fields are available
		final String renderingEngineMaker = capabilities.getValue(BrowsCapField.RENDERING_ENGINE_MAKER);
		final String renderingengineVersion = capabilities.getValue(BrowsCapField.RENDERING_ENGINE_VERSION);
		final String renderingEngineName = capabilities.getValue(BrowsCapField.RENDERING_ENGINE_NAME);
		final String platformMaker = capabilities.getValue(BrowsCapField.PLATFORM_MAKER);
		return UserAgentInfo.builder().userAgent(userAgentString).browser(browser).browserType(browserType)
				.browserMajorVersion(browserMajorVersion).deviceType(deviceType).platformVersion(platformVersion)
				.renderingEngineMaker(renderingEngineMaker).renderingengineVersion(renderingengineVersion)
				.renderingEngineName(renderingEngineName).platformMaker(platformMaker)
				.browserWithVersion(String.format("%s %s", browser, browserMajorVersion)).operatingSystem(platform)
				.build();
	}
}