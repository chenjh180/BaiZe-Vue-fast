package com.ruoyi.common.exception.user;

/**
 * 微信小程序登录失败
 * 
 * @author ruoyi
 */
public class WeChatMiniException extends UserException
{
    private static final long serialVersionUID = 1L;

    public WeChatMiniException()
    {
        super("wechatmini.login.error",null);
    }
}
