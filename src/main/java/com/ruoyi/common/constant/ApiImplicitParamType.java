package com.ruoyi.common.constant;


/**
 * ApiImplicitParam 注解 中 paramType 待选值
 * @author CR7
 *
 */
public class ApiImplicitParamType
{

    /**
     * 通过@RequestHeader获取
     */
    public static final String HEADER = "header";
    /**
     * 通过@RequestParam获取
     */
    public static final String QUERY = "query";
    /**
     * 通过@PathVariable获取
     */
    public static final String PATH = "path";
    /**
     * 通过@RequestBody获取
     */
    public static final String BODY = "body";
    /**
     * 通过@RequestParam获取
     */
    public static final String FORM = "form";
}
