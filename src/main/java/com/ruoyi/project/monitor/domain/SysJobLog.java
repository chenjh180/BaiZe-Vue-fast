package com.ruoyi.project.monitor.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.aspectj.lang.annotation.Excel.ColumnType;
import com.ruoyi.framework.web.domain.CommonEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 定时任务调度日志表 sys_job_log
 * 
 * @author ruoyi
 */
@Data
@ToString
@EqualsAndHashCode(callSuper=true)
@TableName("sys_job_log")
@ApiModel(value = "SysJobLog" , description = "定时任务调度日志")
public class SysJobLog extends CommonEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @ApiModelProperty("ID")
    @Excel(name = "日志序号", cellType = ColumnType.STRING)
    @TableId(type=IdType.ASSIGN_ID)
    private Long jobLogId;

    /** 任务名称 */
    @ApiModelProperty("任务名称")
    @Excel(name = "任务名称")
    private String jobName;

    /** 任务组名 */
    @ApiModelProperty("任务组名")
    @Excel(name = "任务组名")
    private String jobGroup;

    /** 调用目标字符串 */
    @ApiModelProperty("调用目标字符串")
    @Excel(name = "调用目标字符串")
    private String invokeTarget;

    /** 日志信息 */
    @ApiModelProperty("日志信息")
    @Excel(name = "日志信息")
    private String jobMessage;

    /** 执行状态（0正常 1失败） */
    @ApiModelProperty("执行状态（0正常 1失败）")
    @Excel(name = "执行状态", readConverterExp = "0=正常,1=失败")
    private String status;

    /** 异常信息 */
    @ApiModelProperty("异常信息")
    @Excel(name = "异常信息")
    private String exceptionInfo;

    /** 开始时间 */
    @ApiModelProperty("开始时间")
    private Date startTime;

    /** 停止时间 */
    @ApiModelProperty("停止时间")
    private Date stopTime;
    
    /** 创建时间 */
    @ApiModelProperty("创建时间")
    private Date createTime;
}
