package com.ruoyi.project.monitor.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * 当前在线会话
 * 
 * @author ruoyi
 */
@Data
@ToString
@ApiModel(value = "SysUserOnline" , description = "在线用户")
public class SysUserOnline
{
    /** 会话编号 */
    @ApiModelProperty("会话编号")
    private String tokenId;

    /** 部门名称 */
    @ApiModelProperty("部门名称")
    private String deptName;

    /** 用户名称 */
    @ApiModelProperty("用户名称")
    private String userName;

    /** 登录IP地址 */
    @ApiModelProperty("登录IP地址")
    private String ipaddr;

    /** 登录地址 */
    @ApiModelProperty("登录地址")
    private String loginLocation;

    /** 浏览器类型 */
    @ApiModelProperty("浏览器类型")
    private String browser;

    /** 操作系统 */
    @ApiModelProperty("操作系统")
    private String os;

    /** 登录时间 */
    @ApiModelProperty("登录时间")
    private Long loginTime;

    /**
     * 登录设备类型
     */
    @ApiModelProperty("登录设备类型")
    private String loginDevice;

    /**
     * 此token对应的LoginId
     */
    @ApiModelProperty("此token对应的LoginId")
    private String loginId;
    
    /**
     * 获取指定 token 剩余有效时间（单位: 秒，返回 -1 代表永久有效，-2 代表没有这个值）
     */
    @ApiModelProperty("剩余有效时间")
    private Long tokenTimeout;
    
    /**
     * 获取指定 token 剩余活跃有效期：这个 token 距离被冻结还剩多少时间（单位: 秒，返回 -1 代表永不冻结，-2 代表没有这个值或 token 已被冻结了）
     */
    @ApiModelProperty("剩余活跃有效期")
    private Long tokenActiveTimeout;
}
