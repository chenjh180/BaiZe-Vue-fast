package com.ruoyi.project.monitor.domain;

import com.ruoyi.common.utils.StringUtils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * 缓存信息
 * 
 * @author ruoyi
 */
@Data
@ToString
@ApiModel(value = "SysCache" , description = "缓存信息")
public class SysCache
{
    /** 缓存名称 */
    @ApiModelProperty("缓存名称")
    private String cacheName = "";

    /** 缓存键名 */
    @ApiModelProperty("缓存键名")
    private String cacheKey = "";

    /** 缓存内容 */
    @ApiModelProperty("缓存内容")
    private String cacheValue = "";

    /** 备注 */
    @ApiModelProperty("备注")
    private String remark = "";

    public SysCache()
    {

    }

    public SysCache(String cacheName, String remark)
    {
        this.cacheName = cacheName;
        this.remark = remark;
    }

    public SysCache(String cacheName, String cacheKey, String cacheValue)
    {
        this.cacheName = StringUtils.replace(cacheName, ":", "");
        this.cacheKey = StringUtils.replace(cacheKey, cacheName, "");
        this.cacheValue = cacheValue;
    }
}
