package com.ruoyi.project.monitor.domain;

import java.util.Date;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.framework.web.domain.CommonEntity;
import com.ruoyi.project.monitor.domain.read.BusiTypeStringNumberConverter;
import com.ruoyi.project.monitor.domain.read.OperTypeConverter;
import com.ruoyi.project.monitor.domain.read.StatusConverter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
/**
 * 操作日志记录表 oper_log
 * 
 * @author ruoyi
 */
@ExcelIgnoreUnannotated
@ColumnWidth(16)
@HeadRowHeight(14)
@HeadFontStyle(fontHeightInPoints = 11)
@Data
@ToString
@EqualsAndHashCode(callSuper=true)
@TableName("sys_oper_log")
@ApiModel(value = "SysOperLog" , description = "操作日志记录表")
public class SysOperLog extends CommonEntity
{
    private static final long serialVersionUID = 1L;

    /** 日志主键 */
    @ApiModelProperty("日志主键")
	@ExcelProperty(value = "操作序号")
    //@Excel(name = "操作序号", cellType = ColumnType.STRING)
    @TableId(type=IdType.ASSIGN_ID)
    private Long operId;

    /** 操作模块 */
    @ApiModelProperty("操作模块")
    //@Excel(name = "操作模块")
	@ExcelProperty(value = "操作模块")
    private String title;

    /** 业务类型（0其它 1新增 2修改 3删除） */
    @ApiModelProperty("业务类型（0其它 1新增 2修改 3删除）")
    //@Excel(name = "业务类型", readConverterExp = "0=其它,1=新增,2=修改,3=删除,4=授权,5=导出,6=导入,7=强退,8=生成代码,9=清空数据")
    @ExcelProperty(value = "业务类型", converter = BusiTypeStringNumberConverter.class)
	private Integer businessType;

    /** 业务类型数组 */
    @ApiModelProperty("业务类型数组")
    private Integer[] businessTypes;

    /** 请求方法 */
    @ApiModelProperty("请求方法")
    //@Excel(name = "请求方法")
	@ExcelProperty(value = "请求方法")
    private String method;

    /** 请求方式 */
    @ApiModelProperty("请求方式")
//    @Excel(name = "请求方式")
	@ExcelProperty(value = "请求方式")
    private String requestMethod;

    /** 操作类别（0其它 1后台用户 2手机端用户） */
    @ApiModelProperty("操作类别（0其它 1后台用户 2手机端用户）")
    //@Excel(name = "操作类别", readConverterExp = "0=其它,1=后台用户,2=手机端用户")
	@ExcelProperty(value = "操作类别", converter = OperTypeConverter.class)
    private Integer operatorType;

    /** 操作人员 */
    @ApiModelProperty("操作人员")
    //@Excel(name = "操作人员")
	@ExcelProperty(value = "操作人员")
    private String operName;

    /** 部门名称 */
    @ApiModelProperty("部门名称")
    //@Excel(name = "部门名称")
	@ExcelProperty(value = "部门名称")
    private String deptName;

    /** 请求url */
    @ApiModelProperty("请求url")
    //@Excel(name = "请求地址")
	@ExcelProperty(value = "请求地址")
    private String operUrl;

    /** 操作地址 */
    @ApiModelProperty("操作地址")
//    @Excel(name = "操作地址")
	@ExcelProperty(value = "操作地址")
    private String operIp;

    /** 操作地点 */
    @ApiModelProperty("操作地点")
    //@Excel(name = "操作地点")
	@ExcelProperty(value = "操作地点")
    private String operLocation;

    /** 请求参数 */
    @ApiModelProperty("请求参数")
    //@Excel(name = "请求参数")
	@ExcelProperty(value = "请求参数")
    private String operParam;

    /** 返回参数 */
    @ApiModelProperty("返回参数")
    //@Excel(name = "返回参数")
	@ExcelProperty(value = "返回参数")
    private String jsonResult;

    /** 操作状态（0正常 1异常） */
    @ApiModelProperty("操作状态（0正常 1异常）")
    //@Excel(name = "状态", readConverterExp = "0=正常,1=异常")
	@ExcelProperty(value = "状态", converter = StatusConverter.class)
    private Integer status;

    /** 错误消息 */
    @ApiModelProperty("错误消息")
//    @Excel(name = "错误消息")
	@ExcelProperty(value = "错误消息")
    private String errorMsg;

    /** 操作时间 */
    @ApiModelProperty("操作时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //@Excel(name = "操作时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	@ExcelProperty(value = "操作时间")
    private Date operTime;

    /** 消耗时间 */
    @ApiModelProperty("消耗时间")
    //@Excel(name = "消耗时间", suffix = "毫秒")
	@ExcelProperty(value = "消耗时间")
    private Long costTime;
}
