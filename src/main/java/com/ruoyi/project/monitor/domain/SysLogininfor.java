package com.ruoyi.project.monitor.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.aspectj.lang.annotation.Excel.ColumnType;
import com.ruoyi.framework.web.domain.CommonEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 系统访问记录表 sys_logininfor
 * 
 * @author ruoyi
 */
@Data
@ToString
@EqualsAndHashCode(callSuper=true)
@TableName("sys_logininfor")
@ApiModel(value = "SysLogininfor" , description = "系统访问记录表")
public class SysLogininfor extends CommonEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @ApiModelProperty("ID")
    @Excel(name = "序号", cellType = ColumnType.STRING)
    @TableId(type=IdType.ASSIGN_ID)
    private Long infoId;

    /** 用户账号 */
    @ApiModelProperty("用户账号")
    @Excel(name = "用户账号")
    private String userName;

    /** 登录状态 0成功 1失败 */
    @ApiModelProperty("登录状态 0成功 1失败")
    @Excel(name = "登录状态", readConverterExp = "0=成功,1=失败")
    private String status;

    /** 登录IP地址 */
    @ApiModelProperty("登录IP地址")
    @Excel(name = "登录地址")
    private String ipaddr;

    /** 登录地点 */
    @ApiModelProperty("登录地点")
    @Excel(name = "登录地点")
    private String loginLocation;

    /** 浏览器类型 */
    @ApiModelProperty("浏览器类型")
    @Excel(name = "浏览器")
    private String browser;

    /** 操作系统 */
    @ApiModelProperty("操作系统")
    @Excel(name = "操作系统")
    private String os;

    /** 提示消息 */
    @ApiModelProperty("提示消息")
    @Excel(name = "提示消息")
    private String msg;

    /** 访问时间 */
    @ApiModelProperty("访问时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "访问时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date loginTime;
    
    /**
     * 原始的userAgent
     */
    @ApiModelProperty("原始的userAgent")
    @Excel(name = "原始的userAgent")
    private String userAgent;
}