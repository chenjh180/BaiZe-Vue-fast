package com.ruoyi.project.monitor.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.R;
import com.ruoyi.framework.web.domain.Server;

import cn.dev33.satoken.annotation.SaCheckPermission;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 服务器监控
 * 
 * @author ruoyi
 */
@Api(tags="服务器监控")
@RestController
@RequestMapping("/monitor/server")
public class ServerController extends BaseController
{
    @ApiOperation("获取服务器信息")
    @SaCheckPermission("monitor:server:list")
    @GetMapping()
    public R<Server> getInfo() throws Exception
    {
        Server server = new Server();
        server.copyTo();
        return success(server);
    }
}
