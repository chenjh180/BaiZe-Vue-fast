package com.ruoyi.project.monitor.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ruoyi.common.constant.ApiImplicitParamType;
import com.ruoyi.common.constant.CacheConstants;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.R;
import com.ruoyi.project.monitor.domain.SysCache;
import com.ruoyi.project.system.domain.vo.CacheInfoVo;

import cn.dev33.satoken.annotation.SaCheckPermission;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * 缓存监控
 * 
 * @author ruoyi
 */
@Api(tags="缓存监控管理")
@RestController
@RequestMapping("/monitor/cache")
public class CacheController extends BaseController
{
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    private final static List<SysCache> caches = new ArrayList<SysCache>();
    {
        caches.add(new SysCache(CacheConstants.LOGIN_TOKEN_KEY, "用户信息"));
        caches.add(new SysCache(CacheConstants.SYS_CONFIG_KEY, "配置信息"));
        caches.add(new SysCache(CacheConstants.SYS_DICT_KEY, "数据字典"));
        caches.add(new SysCache(CacheConstants.CAPTCHA_CODE_KEY, "验证码"));
        caches.add(new SysCache(CacheConstants.REPEAT_SUBMIT_KEY, "防重提交"));
        caches.add(new SysCache(CacheConstants.RATE_LIMIT_KEY, "限流处理"));
        caches.add(new SysCache(CacheConstants.PWD_ERR_CNT_KEY, "密码错误次数"));
    }

    @ApiOperation("获取监控信息")
    @SaCheckPermission("monitor:cache:list")
    @GetMapping()
    public R<CacheInfoVo> getInfo() throws Exception
    {
        Properties info = (Properties) redisTemplate.execute((RedisCallback<Object>) connection -> connection.info());
        Properties commandStats = (Properties) redisTemplate.execute((RedisCallback<Object>) connection -> connection.info("commandstats"));
        Object dbSize = redisTemplate.execute((RedisCallback<Object>) connection -> connection.dbSize());

//        Map<String, Object> result = new HashMap<>(3);
//        result.put("info", info);
//        result.put("dbSize", dbSize);

        List<Map<String, String>> pieList = new ArrayList<>();
        commandStats.stringPropertyNames().forEach(key -> {
            Map<String, String> data = new HashMap<>(2);
            String property = commandStats.getProperty(key);
            data.put("name", StringUtils.removeStart(key, "cmdstat_"));
            data.put("value", StringUtils.substringBetween(property, "calls=", ",usec"));
            pieList.add(data);
        });
//        result.put("commandStats", pieList);
        return success(new CacheInfoVo(info, dbSize, pieList));
    }

    @ApiOperation("获取缓存名称")
    @SaCheckPermission("monitor:cache:list")
    @GetMapping("/getNames")
    public R<List<SysCache>> cache()
    {
        return success(caches);
    }

    @ApiOperation("获取缓存keys")
    @ApiImplicitParam(name = "cacheName", value = "缓存名称", required = true, dataType = "String", paramType = ApiImplicitParamType.PATH, dataTypeClass = String.class)
    @SaCheckPermission("monitor:cache:list")
    @GetMapping("/getKeys/{cacheName}")
    public R<TreeSet<String>> getCacheKeys(@PathVariable String cacheName)
    {
        Set<String> cacheKeys = redisTemplate.keys(cacheName + "*");
        return success(new TreeSet<>(cacheKeys));
    }

    @ApiOperation("获取缓存值")
    @SaCheckPermission("monitor:cache:list")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "cacheName", value = "缓存名称", dataType = "String", paramType = ApiImplicitParamType.PATH, dataTypeClass = String.class),
        @ApiImplicitParam(name = "cacheKey", value = "缓存key", dataType = "String", paramType = ApiImplicitParamType.PATH, dataTypeClass = String.class)
    })
    @GetMapping("/getValue/{cacheName}/{cacheKey}")
    public R<SysCache> getCacheValue(@PathVariable String cacheName, @PathVariable String cacheKey)
    {
        String cacheValue = redisTemplate.opsForValue().get(cacheKey);
        SysCache sysCache = new SysCache(cacheName, cacheKey, cacheValue);
        return success(sysCache);
    }

    @ApiOperation("根据名称清空缓存")
    @ApiImplicitParam(name = "cacheName", value = "缓存名称", dataType = "String", paramType = ApiImplicitParamType.PATH, dataTypeClass = String.class)
    @SaCheckPermission("monitor:cache:list")
    @DeleteMapping("/clearCacheName/{cacheName}")
    public R<String> clearCacheName(@PathVariable String cacheName)
    {
        Collection<String> cacheKeys = redisTemplate.keys(cacheName + "*");
        redisTemplate.delete(cacheKeys);
        return success();
    }

    @ApiOperation("根据key清空缓存")
    @ApiImplicitParam(name = "cacheKey", value = "缓存key", dataType = "String", paramType = ApiImplicitParamType.PATH, dataTypeClass = String.class)
    @SaCheckPermission("monitor:cache:list")
    @DeleteMapping("/clearCacheKey/{cacheKey}")
    public R<String> clearCacheKey(@PathVariable String cacheKey)
    {
        redisTemplate.delete(cacheKey);
        return success();
    }

    @ApiOperation("清空所有缓存")
    @SaCheckPermission("monitor:cache:list")
    @DeleteMapping("/clearCacheAll")
    public R<String> clearCacheAll()
    {
        Collection<String> cacheKeys = redisTemplate.keys("*");
        redisTemplate.delete(cacheKeys);
        return success();
    }
}
