package com.ruoyi.project.system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.file.MimeTypeUtils;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.config.RuoYiConfig;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.R;
import com.ruoyi.project.system.domain.SysUser;
import com.ruoyi.project.system.domain.vo.AvatarVo;
import com.ruoyi.project.system.domain.vo.ProfileVo;
import com.ruoyi.project.system.service.ISysUserService;

import cn.dev33.satoken.stp.StpUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * 个人信息 业务处理
 * 
 * @author ruoyi
 */
@Api(tags="个人信息 业务处理")
@RestController
@RequestMapping("/system/user/profile")
public class SysProfileController extends BaseController
{
    @Autowired
    private ISysUserService userService;

    /**
     * 个人信息
     */
    @ApiOperation("个人信息")
    @GetMapping
    public R<ProfileVo> profile()
    {
    	SysUser user = getLoginUser();
    	String roleGroup = userService.selectUserRoleGroup(user.getUserName());
        String postGroup = userService.selectUserPostGroup(user.getUserName());
        ProfileVo vo = ProfileVo.builder().user(user).roleGroup(roleGroup).postGroup(postGroup).build();
        return success(vo);
    }

    /**
     * 修改用户
     */
    @ApiOperation("修改用户")
    @Log(title = "个人信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public R<String> updateProfile(@RequestBody SysUser user)
    {
        SysUser sysUser = getLoginUser();
        user.setUserName(sysUser.getUserName());
		if (StringUtils.isNotEmpty(user.getPhonenumber()) && !userService.checkPhoneUnique(user))
		{
            return error("修改用户'" + user.getUserName() + "'失败，手机号码已存在");
        }
		if (StringUtils.isNotEmpty(user.getEmail()) && !userService.checkEmailUnique(user))
        {
            return error("修改用户'" + user.getUserName() + "'失败，邮箱账号已存在");
        }
        user.setUserId(sysUser.getUserId());
        user.setPassword(null);
		//user.setAvatar(null);
        //user.setDeptId(null);
        if (userService.updateUserProfile(user) > 0) {
            // 更新缓存用户信息
            sysUser.setNickName(user.getNickName());
            sysUser.setPhonenumber(user.getPhonenumber());
            sysUser.setEmail(user.getEmail());
            sysUser.setSex(user.getSex());
            StpUtil.getSession().set("userInfo", sysUser);
            return success();
        }
        return error("修改个人信息异常，请联系管理员");
    }

    /**
     * 重置密码
     */
    @ApiOperation("重置密码")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "oldPassword", value = "旧密码", dataType = "String", dataTypeClass = String.class),
        @ApiImplicitParam(name = "newPassword", value = "新密码", dataType = "String", dataTypeClass = String.class)
    })
    @Log(title = "个人信息", businessType = BusinessType.UPDATE)
    @PutMapping("/updatePwd")
    public R<String> updatePwd(String oldPassword, String newPassword)
    {
        SysUser loginUser = getLoginUser();
        String userName = loginUser.getUserName();
        String password = loginUser.getPassword();
        if (!SecurityUtils.matchesPassword(oldPassword, password))
        {
            return error("修改密码失败，旧密码错误");
        }
        if (SecurityUtils.matchesPassword(newPassword, password))
        {
            return error("新密码不能与旧密码相同");
        }
        newPassword = SecurityUtils.encryptPassword(newPassword);
        if (userService.resetUserPwd(userName, newPassword) > 0)
        {
            // 更新缓存用户密码
            getLoginUser().setPassword(newPassword);
            return success();
        }
        return error("修改密码异常，请联系管理员");
    }

    /**
     * 头像上传
     */
    @ApiOperation("头像上传")
    @ApiImplicitParam(name = "avatarfile", value = "文件", required = true, dataType = "MultipartFile",  dataTypeClass = MultipartFile.class)
    @Log(title = "用户头像", businessType = BusinessType.UPDATE)
    @PostMapping("/avatar")
    public R<AvatarVo> avatar(@RequestParam("avatarfile") MultipartFile file) throws Exception
    {
        if (!file.isEmpty())
        {
            SysUser loginUser = getLoginUser();
            String avatar = FileUploadUtils.upload(RuoYiConfig.getAvatarPath(), file, MimeTypeUtils.IMAGE_EXTENSION);
            if (userService.updateUserAvatar(loginUser.getUserName(), avatar))
            {
                // 更新缓存用户头像
                getLoginUser().setAvatar(avatar);
                return success(new AvatarVo(avatar));
            }
        }
        return error("上传图片异常，请联系管理员");
    }
}
