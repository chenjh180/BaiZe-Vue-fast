package com.ruoyi.project.system.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ruoyi.common.constant.CacheConstants;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.framework.redis.RedisCache;
import com.ruoyi.framework.security.LoginBody;
import com.ruoyi.framework.security.service.SysLoginService;
import com.ruoyi.framework.security.service.SysPermissionService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.R;
import com.ruoyi.project.system.domain.SysMenu;
import com.ruoyi.project.system.domain.SysUser;
import com.ruoyi.project.system.domain.vo.RouterVo;
import com.ruoyi.project.system.domain.vo.TokenVo;
import com.ruoyi.project.system.domain.vo.SysLoginUserInfoVo;
import com.ruoyi.project.system.service.ISysMenuService;

import cn.dev33.satoken.stp.StpUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 登录验证
 * 
 * @author ruoyi
 */
@RestController
@Api(tags="登录")
public class SysLoginController extends BaseController
{
    @Autowired
    private SysLoginService loginService;

    @Autowired
    private ISysMenuService menuService;

    @Autowired
    private SysPermissionService permissionService;

    @Autowired
    private RedisCache redisCache;
    
    /**
     * 登录方法
     * 
     * @param loginBody 登录信息
     * @return 结果
     */
    @PostMapping("/login")
    @ApiOperation("用户登录")
    public R<TokenVo> login(@RequestBody LoginBody loginBody)
    {
        // 生成令牌
        TokenVo token = loginService.login(loginBody);
        return success(token);
    }

    /**
     * 注销
     *
     * @return 结果
     */
    @ApiOperation("用户注销")
    @PostMapping("/logout")
    public R<String> logout() {
        String tokenId = StpUtil.getTokenValue();
        redisCache.deleteObject(CacheConstants.getTokenKey(tokenId));
        StpUtil.logout();
        return success();
    }

    /**
     * 获取用户信息
     * 
     * @return 用户信息
     */
    @ApiOperation("获取用户信息")
    @GetMapping("getInfo")
    public R<SysLoginUserInfoVo> getInfo()
    {
        SysUser user = SecurityUtils.getLoginUser();
        // 角色集合
        Set<String> roles = permissionService.getRolePermission(user);
        // 权限集合
        Set<String> permissions = permissionService.getMenuPermission(user);
        SysLoginUserInfoVo userInfo = SysLoginUserInfoVo.builder().user(user).roles(roles).permissions(permissions).build();
        return success(userInfo);
    }

    /**
     * 获取路由信息
     * 
     * @return 路由信息
     */
    @ApiOperation("获取路由信息")
    @GetMapping("getRouters")
    public R<List<RouterVo>> getRouters()
    {
        Long userId = SecurityUtils.getUserId();
        List<SysMenu> menus = menuService.selectMenuTreeByUserId(userId);
        return success(menuService.buildMenus(menus));
    }
    
    /**
     * 切换语言
     * @param lang
     * @return
     */
    @ApiOperation("切换语言")
    @GetMapping("/changeLanguage")
    public R<String> changeLanguage(String lang)
    {
        return success();
    }
}
