package com.ruoyi.project.system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.file.MimeTypeUtils;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.config.RuoYiConfig;
import com.ruoyi.framework.security.NickNameBody;
import com.ruoyi.framework.security.WeChatMiniLoginBody;
import com.ruoyi.framework.security.service.SysLoginService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.project.system.domain.SysUser;
import com.ruoyi.project.system.domain.vo.AvatarVo;
import com.ruoyi.project.system.domain.vo.TokenVo;
import com.ruoyi.project.system.service.ISysUserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

import com.ruoyi.framework.web.domain.R;
/**
 * 微信小程序登录
 * @author baize
 *
 */
@Api(tags = "微信小程序")
@RestController
@RequestMapping("/weChatMini")
public class WeChatMiniController extends BaseController{

	@Autowired
	private SysLoginService loginService;
	
	@Autowired
    private ISysUserService userService;
	
	/**
     * 登录方法
     * 
     * @param loginBody 登录信息
     * @return 结果
     */
	@ApiOperation("微信小程序登录")
    @PostMapping("/login")
    public R<TokenVo> login(@Validated @RequestBody WeChatMiniLoginBody loginBody)
    {
        // 生成令牌
        TokenVo token = loginService.weChatMiniLogin(loginBody);
        return success(token);
    }
    
    /**
     * 头像上传
     */
	@ApiOperation("头像上传")
	@ApiImplicitParam(name = "avatarfile", value = "文件", required = true, dataType = "MultipartFile",  dataTypeClass = MultipartFile.class)
    @Log(title = "微信小程序用户头像", businessType = BusinessType.UPDATE)
    @PostMapping("/avatar")
    public R<AvatarVo> avatar(@RequestParam("avatarfile") MultipartFile file) throws Exception
    {
        if (!file.isEmpty())
        {
            SysUser loginUser = getLoginUser();
            String avatar = FileUploadUtils.upload(RuoYiConfig.getAvatarPath(), file, MimeTypeUtils.IMAGE_EXTENSION);
            if (userService.updateMiniAppUserAvatar(loginUser.getUserName(), avatar))
            {
                AvatarVo avatarVo = AvatarVo.builder().imgUrl(avatar).build();
                // 更新缓存用户头像
                getLoginUser().setAvatar(avatar);
                // 已更新过用户头像
                getLoginUser().setInitAvatar(1);
                return success(avatarVo);
            }
        }
        return error("上传图片异常，请联系管理员");
    }
    
    /**
     * 更新昵称
     */
    @ApiOperation("更新昵称")
    @Log(title = "微信小程序用户昵称", businessType = BusinessType.UPDATE)
    @PostMapping("/nickName")
    public R<String> nickName(@Validated @RequestBody NickNameBody nickNameBody) throws Exception
    {
        SysUser loginUser = getLoginUser();
        if (userService.updateMiniAppUserNickName(loginUser.getUserName(), nickNameBody.getNickName()))
        {
            // 更新缓存用户头像
            getLoginUser().setNickName(nickNameBody.getNickName());
            // 已更新过用户昵称
            getLoginUser().setInitNickName(1);
            return success();
        }
        return error("昵称修改失败，请联系管理员");
    }
}
