package com.ruoyi.project.system.domain.vo;

import java.util.List;

import com.ruoyi.framework.web.domain.TreeSelect;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

/**
 * 获取对应角色部门树列表返回对象
 * 
 * @author baize
 */
@Data
@ToString
@ApiModel(value = "DeptTreeVo", description = "获取对应角色部门树列表返回对象")
@AllArgsConstructor
public class DeptTreeVo
{
    @ApiModelProperty("部门树信息")
    private List<Long> checkedKeys;
    
    @ApiModelProperty("部门树结构信息")
    private List<TreeSelect> depts; 
}
