package com.ruoyi.project.system.domain.vo;

import java.util.List;

import com.ruoyi.project.system.domain.SysRole;
import com.ruoyi.project.system.domain.SysUser;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

/**
 * 根据用户编号获取授权角色返回对象
 * 
 * @author baize
 */
@Data
@ToString
@ApiModel(value = "AuthRoleVo", description = "根据用户编号获取授权角色返回对象")
@AllArgsConstructor
public class AuthRoleVo
{
    @ApiModelProperty("用户信息")
    private SysUser user;
    
    @ApiModelProperty("角色信息")
    private List<SysRole> roles;
}
