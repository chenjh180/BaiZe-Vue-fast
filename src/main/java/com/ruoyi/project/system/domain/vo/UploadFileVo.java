package com.ruoyi.project.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 上传文件返回对象
 * 
 * @author baize
 */
@Data
@ToString
@ApiModel(value = "UploadFileVo", description = "上传文件返回对象")
@NoArgsConstructor
public class UploadFileVo
{
    @ApiModelProperty("文件url")
    private String url;
    
    @ApiModelProperty("文件名称")
    private String fileName;
    
    @ApiModelProperty("新文件名称")
    private String newFileName;
    
    @ApiModelProperty("源文件名称")
    private String originalFilename;
    
    @ApiModelProperty("多个文件url")
    private String urls;
    
    @ApiModelProperty("多个文件名称")
    private String fileNames;
    
    @ApiModelProperty("多个新文件名称")
    private String newFileNames;
    
    @ApiModelProperty("多个源文件名称")
    private String originalFilenames;
}
