package com.ruoyi.project.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * 用户和岗位关联 sys_user_post
 * 
 * @author ruoyi
 */
@Data
@ToString
@TableName("sys_user_post")
@ApiModel(value = "SysUserPost" , description = "用户和岗位关联")
public class SysUserPost
{
    /** 用户ID */
    @ApiModelProperty("用户ID")
    private Long userId;
    
    /** 岗位ID */
    @ApiModelProperty("岗位ID")
    private Long postId;
}
