package com.ruoyi.project.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * 用户头像上传返回对象
 * 
 * @author baize
 */
@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ApiModel(value = "AvatarVo", description = "用户头像上传返回对象")
@AllArgsConstructor
@Builder
public class AvatarVo
{
    @ApiModelProperty("头像地址")
    private String imgUrl;
}
