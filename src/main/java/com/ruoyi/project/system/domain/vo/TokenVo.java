package com.ruoyi.project.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * Token返回对象
 * 
 * @author baize
 */
@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ApiModel(value = "TokenVo", description = "token返回对象")
@AllArgsConstructor
@Builder
public class TokenVo
{
    @ApiModelProperty("token")
    private String token;
}
