package com.ruoyi.project.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * 角色和菜单关联 sys_role_menu
 * 
 * @author ruoyi
 */
@Data
@ToString
@TableName("sys_role_menu")
@ApiModel(value = "SysRoleMenu" , description = "角色和菜单关联")
public class SysRoleMenu
{
    /** 角色ID */
    @ApiModelProperty("角色ID")
    private Long roleId;
    
    /** 菜单ID */
    @ApiModelProperty("菜单ID")
    private Long menuId;
}
