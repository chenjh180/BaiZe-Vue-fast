package com.ruoyi.project.system.domain.vo;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ruoyi.project.system.domain.SysUser;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * 用户信息返回对象
 * 
 * @author baize
 */
@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ApiModel(value = "SysLoginUserInfoVo", description = "SysLoginController用户信息返回对象")
@AllArgsConstructor
@Builder
public class SysLoginUserInfoVo
{
    @ApiModelProperty("用户信息")
    private SysUser user;
    
    @ApiModelProperty("角色集合")
    private Set<String> roles;
    
    @ApiModelProperty("权限集合")
    private Set<String> permissions;
}
