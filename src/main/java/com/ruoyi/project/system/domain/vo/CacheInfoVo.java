package com.ruoyi.project.system.domain.vo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

/**
 * 用户头像上传返回对象
 * 
 * @author baize
 */
@Data
@ToString
@ApiModel(value = "CacheInfoVo", description = "缓存信息")
@AllArgsConstructor
public class CacheInfoVo
{
    @ApiModelProperty("属性")
    private Properties info;
    
    @ApiModelProperty("数据库大小")
    private Object dbSize;
    
    @ApiModelProperty("命令状态")
    private List<Map<String, String>> commandStats = new ArrayList<Map<String, String>>();
}
