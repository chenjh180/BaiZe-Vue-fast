package com.ruoyi.project.system.domain;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.xss.Xss;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.aspectj.lang.annotation.Excel.ColumnType;
import com.ruoyi.framework.aspectj.lang.annotation.Excel.Type;
import com.ruoyi.framework.aspectj.lang.annotation.Excels;
import com.ruoyi.framework.web.domain.BaseEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 用户对象 sys_user
 * 
 * @author ruoyi
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true)
@TableName("sys_user")
@ApiModel(value = "SysUser" , description = "用户对象")
public class SysUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户ID */
    @ApiModelProperty("用户ID")
    @Excel(name = "用户序号", type = Type.EXPORT, cellType = ColumnType.STRING, prompt = "用户编号")
    @TableId(type = IdType.ASSIGN_ID)
    private Long userId;

    /** 部门ID */
    @ApiModelProperty("部门ID")
    @Excel(name = "部门编号", type = Type.IMPORT)
    private Long deptId;

    /** 用户账号 */
    @ApiModelProperty("用户账号")
    @Excel(name = "登录名称")
    @Xss(message = "用户账号不能包含脚本字符")
    @NotBlank(message = "用户账号不能为空")
    @Size(min = 0, max = 30, message = "用户账号长度不能超过30个字符")
    private String userName;

    /** 用户昵称 */
    @ApiModelProperty("用户昵称")
    @Excel(name = "用户名称")
    @Xss(message = "用户昵称不能包含脚本字符")
    @Size(min = 0, max = 30, message = "用户昵称长度不能超过30个字符")
    private String nickName;

    /** 用户邮箱 */
    @ApiModelProperty("用户邮箱")
    @Excel(name = "用户邮箱")
    @Email(message = "邮箱格式不正确")
    @Size(min = 0, max = 50, message = "邮箱长度不能超过50个字符")
    private String email;

    /** 手机号码 */
    @ApiModelProperty("手机号码")
    @Excel(name = "手机号码", cellType = ColumnType.TEXT)
    @Size(min = 0, max = 11, message = "手机号码长度不能超过11个字符")
    private String phonenumber;

    /** 用户性别 */
    @ApiModelProperty("用户性别： 0=男,1=女,2=未知")
    @Excel(name = "用户性别", readConverterExp = "0=男,1=女,2=未知")
    private String sex;

    /** 用户头像 */
    @ApiModelProperty("用户头像")
    private String avatar;

    /** 密码 */
    @ApiModelProperty("密码")
    private String password;

    /** 帐号状态（0正常 1停用） */
    @ApiModelProperty("帐号状态：0=正常,1=停用")
    @Excel(name = "帐号状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    @ApiModelProperty("删除标志（0代表存在 2代表删除）")
    private String delFlag;

    /** 最后登录IP */
    @ApiModelProperty("最后登录IP")
    @Excel(name = "最后登录IP", type = Type.EXPORT)
    private String loginIp;

    /** 最后登录时间 */
    @ApiModelProperty("最后登录时间")
    @Excel(name = "最后登录时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Type.EXPORT)
    private Date loginDate;

    /** 部门对象 */
    @ApiModelProperty("部门对象")
    @Excels({
        @Excel(name = "部门名称", targetAttr = "deptName", type = Type.EXPORT),
        @Excel(name = "部门负责人", targetAttr = "leader", type = Type.EXPORT),
        @Excel(name = "部门状态", targetAttr = "status", dictType = "sys_normal_disable")
    })
    private SysDept dept;

    /** 角色对象 */
    @ApiModelProperty("角色对象")
    private List<SysRole> roles;

    /** 角色组 */
    @ApiModelProperty("角色组")
    private Long[] roleIds;

    /** 岗位组 */
    @ApiModelProperty("岗位组")
    private Long[] postIds;

    /** 角色ID */
    @ApiModelProperty("角色ID")
    private Long roleId;
    
    /**
     * 微信小程序的openid
     */
    @Excel(name = "微信openid")
    @ApiModelProperty("微信openid")
    private String openid;
    
    /**
     * 是否更新过微信昵称 0 否 1 是
     */
    @ApiModelProperty("是否更新过微信昵称 0 否 1 是")
    private Integer initNickName;
    
    /**
     * 是否更新过微信头像 0 否 1 是
     */
    @ApiModelProperty("是否更新过微信头像 0 否 1 是")
    private Integer initAvatar;

    public SysUser(Long userId)
    {
        this.userId = userId;
    }

    public boolean isAdmin()
    {
        return isAdmin(this.userId);
    }

    public static boolean isAdmin(Long userId)
    {
        return userId != null && 1L == userId;
    }
    
    public SysUser(String openid) {
    	this.openid = openid;
    }
}
