package com.ruoyi.project.system.domain.vo;

import java.util.List;

import com.ruoyi.project.tool.gen.domain.GenTable;
import com.ruoyi.project.tool.gen.domain.GenTableColumn;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

/**
 * 修改代码生成业务返回对象
 * 
 * @author baize
 */
@Data
@ToString
@ApiModel(value = "GenTableInfoVo", description = "修改代码生成业务返回对象")
@AllArgsConstructor
public class GenTableInfoVo
{
    @ApiModelProperty("当前表信息")
    private GenTable table;
    
    @ApiModelProperty("当前表的列信息")
    private List<GenTableColumn> list;
    
    @ApiModelProperty("所有表信息")
    private List<GenTable> tables;
}
