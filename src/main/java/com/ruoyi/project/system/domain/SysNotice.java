package com.ruoyi.project.system.domain;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.xss.Xss;
import com.ruoyi.framework.web.domain.BaseEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 通知公告表 sys_notice
 * 
 * @author ruoyi
 */
@Data
@ToString
@EqualsAndHashCode(callSuper=true)
@TableName("sys_notice")
@ApiModel(value = "SysNotice" , description = "通知公告")
public class SysNotice extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 公告ID */
    @ApiModelProperty("公告ID")
    @TableId(type=IdType.ASSIGN_ID)
    private Long noticeId;

    /** 公告标题 */
    @ApiModelProperty("公告标题")
    @Xss(message = "公告标题不能包含脚本字符")
    @NotBlank(message = "公告标题不能为空")
    @Size(min = 0, max = 50, message = "公告标题不能超过50个字符")
    private String noticeTitle;

    /** 公告类型（1通知 2公告） */
    @ApiModelProperty("公告类型（1通知 2公告）")
    private String noticeType;

    /** 公告内容 */
    @ApiModelProperty("公告内容")
    private String noticeContent;

    /** 公告状态（0正常 1关闭） */
    @ApiModelProperty("公告状态（0正常 1关闭）")
    private String status;
    
    /**
     * content_type 文本编辑器类型 1 其他编辑器 2 markdown 
     */
    @ApiModelProperty("文本编辑器类型 1 其他编辑器 2 markdown ")
    private String contentType;
    /**
     * notice_content_markdown Markdown格式内容
     */
    @ApiModelProperty("Markdown格式内容")
    private String noticeContentMarkdown;
    /**
     * platform 1 应用用 2 管理后台用
     */
    @ApiModelProperty("1 应用用 2 管理后台用")
    private String platform;
}
