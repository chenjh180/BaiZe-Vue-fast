package com.ruoyi.project.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * 角色和部门关联 sys_role_dept
 * 
 * @author ruoyi
 */
@Data
@ToString
@TableName("sys_role_dept")
@ApiModel(value = "SysRoleDept" , description = "角色和部门关联")
public class SysRoleDept
{
    /** 角色ID */
    @ApiModelProperty("角色ID")
    private Long roleId;
    
    /** 部门ID */
    @ApiModelProperty("部门ID")
    private Long deptId;
}
