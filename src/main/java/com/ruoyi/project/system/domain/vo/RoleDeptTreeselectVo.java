package com.ruoyi.project.system.domain.vo;

import java.util.List;

import com.ruoyi.framework.web.domain.TreeSelect;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

/**
 * 加载对应角色部门列表树返回对象
 * 
 * @author baize
 */
@Data
@ToString
@ApiModel(value = "RoleDeptTreeselectVo", description = "加载对应角色部门列表树返回对象")
@AllArgsConstructor
public class RoleDeptTreeselectVo
{
    @ApiModelProperty("部门树信息")
    private List<Long> checkedKeys;
    
    @ApiModelProperty("构建前端所需要下拉树结构")
    private List<TreeSelect> depts;
}
