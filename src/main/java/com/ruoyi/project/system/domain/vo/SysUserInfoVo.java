package com.ruoyi.project.system.domain.vo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ruoyi.project.system.domain.SysPost;
import com.ruoyi.project.system.domain.SysRole;
import com.ruoyi.project.system.domain.SysUser;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * SysUserController用户信息返回对象
 * 
 * @author baize
 */
@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ApiModel(value = "SysUserInfoVo", description = "SysUserController用户信息返回对象")
@NoArgsConstructor
public class SysUserInfoVo
{
    @ApiModelProperty("角色集合")
    private List<SysRole> roles;
    
    @ApiModelProperty("岗位集合")
    private List<SysPost> posts;
    
    @ApiModelProperty("岗位id")
    private List<Long> postIds;
    
    @ApiModelProperty("角色id")
    private List<Long> roleIds;
    
    @ApiModelProperty("用户")
    private SysUser sysUser;
}
