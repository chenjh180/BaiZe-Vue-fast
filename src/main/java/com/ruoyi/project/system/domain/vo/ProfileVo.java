package com.ruoyi.project.system.domain.vo;

import com.ruoyi.project.system.domain.SysUser;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * 个人信息返回对象
 * 
 * @author baize
 */
@Data
@ToString
@ApiModel(value = "ProfileVo", description = "个人信息返回对象")
@AllArgsConstructor
@Builder
public class ProfileVo
{
    @ApiModelProperty("用户")
    private SysUser user;

    @ApiModelProperty("用户所属角色组")
    private String roleGroup;
    
    @ApiModelProperty("用户所属岗位组")
    private String postGroup;
    
}
