package com.ruoyi.project.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * 用户和角色关联 sys_user_role
 * 
 * @author ruoyi
 */
@Data
@ToString
@TableName("sys_user_role")
@ApiModel(value = "SysUserRole" , description = "用户和角色关联")
public class SysUserRole
{
    /** 用户ID */
    @ApiModelProperty("用户ID")
    private Long userId;
    
    /** 角色ID */
    @ApiModelProperty("角色ID")
    private Long roleId;
}
