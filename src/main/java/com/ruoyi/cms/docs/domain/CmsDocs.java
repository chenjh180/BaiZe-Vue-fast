package com.ruoyi.cms.docs.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 文件对象 cms_docs
 * 
 * @author ning
 * @date 2023-06-25
 */
@Data
@ToString
@EqualsAndHashCode(callSuper=true)
@TableName("cms_docs")
@ApiModel(value = "CmsDocs" , description = "文件对象")
public class CmsDocs extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 文件id */
    @ApiModelProperty("文件id")
    @TableId(type=IdType.ASSIGN_ID)
    private Long docsId;

    /** 文件夹id */
    @ApiModelProperty("文件夹id")
    @Excel(name = "文件夹id")
    private Long folderId;

    /** 文件内容id */
    @ApiModelProperty("文件内容id")
    @Excel(name = "文件内容id")
    private Long contentId;

    /**
     * 内容
     */
    @ApiModelProperty("内容")
    @Excel(name = "内容")
    private String content;

    /**
     * Markdown格式内容
     */
    @ApiModelProperty("Markdown格式内容")
    @Excel(name = "Markdown格式内容")
    private String contentMarkdown;

    /** 文件名称 */
    @ApiModelProperty("文件名称")
    @Excel(name = "文件名称")
    private String docsName;

    /** 显示顺序 */
    @ApiModelProperty("显示顺序")
    @Excel(name = "显示顺序")
    private Integer orderNum;

    /** 文件类型 */
    @ApiModelProperty("文件类型")
    @Excel(name = "文件类型")
    private String docsType;

    /** 删除标志（0代表存在 2代表删除） */
    @ApiModelProperty("删除标志（0代表存在 2代表删除）")
    private String delFlag;

    @ApiModelProperty("内容大小")
    private String contentSize;
    
    @ApiModelProperty("文件夹路径")
    private String folderPath;
}
