package com.ruoyi.cms.docs.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.cms.docs.domain.CmsDocs;

/**
 * 文件Service接口
 * 
 * @author ning
 * @date 2023-06-25
 */
public interface ICmsDocsService extends IService<CmsDocs>
{
    /**
     * 查询文件
     * 
     * @param docsId 文件主键
     * @return 文件
     */
    public CmsDocs selectCmsDocsByDocsId(Long docsId);

    /**
     * 查询文件列表
     * 
     * @param cmsDocs 文件
     * @return 文件集合
     */
    public List<CmsDocs> selectCmsDocsList(CmsDocs cmsDocs);

    /**
     * 新增文件
     * 
     * @param cmsDocs 文件
     * @return 结果
     */
    public int insertCmsDocs(CmsDocs cmsDocs);

    /**
     * 修改文件
     * 
     * @param cmsDocs 文件
     * @return 结果
     */
    public int updateCmsDocs(CmsDocs cmsDocs);

    /**
     * 批量删除文件
     * 
     * @param docsIds 需要删除的文件主键集合
     * @return 结果
     */
    public int deleteCmsDocsByDocsIds(Long[] docsIds);

    /**
     * 删除文件信息
     * 
     * @param docsId 文件主键
     * @return 结果
     */
    public int deleteCmsDocsByDocsId(Long docsId);
}
