package com.ruoyi.cms.docs.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.cms.docs.domain.CmsDocs;
import com.ruoyi.cms.docs.mapper.CmsDocsMapper;
import com.ruoyi.cms.docs.service.ICmsDocsService;

/**
 * 文件Service业务层处理
 * 
 * @author ning
 * @date 2023-06-25
 */
@Service
public class CmsDocsServiceImpl extends ServiceImpl<CmsDocsMapper, CmsDocs> implements ICmsDocsService 
{
    /**
     * 查询文件
     * 
     * @param docsId 文件主键
     * @return 文件
     */
    @Override
    public CmsDocs selectCmsDocsByDocsId(Long docsId)
    {
        return baseMapper.selectCmsDocsByDocsId(docsId);
    }

    /**
     * 查询文件列表
     * 
     * @param cmsDocs 文件
     * @return 文件
     */
    @Override
    public List<CmsDocs> selectCmsDocsList(CmsDocs cmsDocs)
    {
        return baseMapper.selectCmsDocsList(cmsDocs);
    }

    /**
     * 新增文件
     * 
     * @param cmsDocs 文件
     * @return 结果
     */
    @Override
    public int insertCmsDocs(CmsDocs cmsDocs)
    {
//        cmsDocs.setCreateTime(DateUtils.getNowDate());
        return baseMapper.insertCmsDocs(cmsDocs);
    }

    /**
     * 修改文件
     * 
     * @param cmsDocs 文件
     * @return 结果
     */
    @Override
    public int updateCmsDocs(CmsDocs cmsDocs)
    {
//        cmsDocs.setUpdateTime(DateUtils.getNowDate());
        return baseMapper.updateCmsDocs(cmsDocs);
    }

    /**
     * 批量删除文件
     * 
     * @param docsIds 需要删除的文件主键
     * @return 结果
     */
    @Override
    public int deleteCmsDocsByDocsIds(Long[] docsIds)
    {
        return baseMapper.deleteCmsDocsByDocsIds(docsIds);
    }

    /**
     * 删除文件信息
     * 
     * @param docsId 文件主键
     * @return 结果
     */
    @Override
    public int deleteCmsDocsByDocsId(Long docsId)
    {
        return baseMapper.deleteCmsDocsByDocsId(docsId);
    }
}
