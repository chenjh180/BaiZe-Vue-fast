package com.ruoyi.cms.folder.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author: WangNing
 * @date: 2023-06-25
 **/
@Data
@ToString
@ApiModel(value = "CmsTreeSelect" , description = "CmsTreeSelect")
public class CmsTreeSelect implements Serializable {
    private static final long serialVersionUID = 1L;
    /** 节点ID */
    @ApiModelProperty("节点ID")
    private Long id;

    /** 节点名称 */
    @ApiModelProperty("节点名称")
    private String label;

    /** 子节点 */
    @ApiModelProperty("子节点")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<CmsTreeSelect> children = new ArrayList<CmsTreeSelect>();

    public CmsTreeSelect()
    {

    }

    public CmsTreeSelect(CmsFolder folder)
    {
        this.id = folder.getFolderId();
        this.label = folder.getFolderName();
        this.children = folder.getChildren().stream().map(CmsTreeSelect::new).collect(Collectors.toList());
    }

}
