package com.ruoyi.cms.folder.domain;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * 文件夹对象 cms_folder
 * 
 * @author ning
 * @date 2023-06-25
 */
@Data
@ToString
@EqualsAndHashCode(callSuper=true)
@TableName("cms_folder")
@ApiModel(value = "CmsFolder" , description = "文件夹对象")
public class CmsFolder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 文件夹id */
    @ApiModelProperty("文件夹id")
    @TableId(type=IdType.ASSIGN_ID)
    private Long folderId;

    /** 文件夹名称 */
    @ApiModelProperty("文件夹名称")
    @Excel(name = "文件夹名称")
    @NotBlank(message = "文件夹名称不能为空")
    @Size(min = 0, max = 30, message = "文件夹名称长度不能超过30个字符")
    private String folderName;

    /** 文件夹状态（0正常 1停用） */
    @ApiModelProperty("文件夹状态（0正常 1停用）")
    @Excel(name = "文件夹状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    @ApiModelProperty("删除标志（0代表存在 2代表删除）")
    private String delFlag;

    /** 父菜单名称 */
    @ApiModelProperty("父菜单名称")
    private String parentName;

    /** 父菜单ID */
    @ApiModelProperty("父菜单ID")
    private Long parentId;

    /** 显示顺序 */
    @ApiModelProperty("显示顺序")
    @NotNull(message = "显示顺序不能为空")
    private Integer orderNum;

    /** 祖级列表 */
    @ApiModelProperty("祖级列表")
    private String ancestors;

    /** 子文件夹 */
    @ApiModelProperty("子文件夹")
    private List<CmsFolder> children = new ArrayList<>();

}
