package com.ruoyi.cms.folder.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.cms.folder.domain.CmsFolder;
import org.apache.ibatis.annotations.Param;

/**
 * 文件夹Mapper接口
 * 
 * @author ning
 * @date 2023-06-25
 */
public interface CmsFolderMapper extends BaseMapper<CmsFolder>
{
    /**
     * 查询文件夹
     * 
     * @param folderId 文件夹主键
     * @return 文件夹
     */
    public CmsFolder selectCmsFolderByFolderId(Long folderId);

    /**
     * 查询文件夹列表
     * 
     * @param cmsFolder 文件夹
     * @return 文件夹集合
     */
    public List<CmsFolder> selectCmsFolderList(CmsFolder cmsFolder);

    /**
     * 新增文件夹
     * 
     * @param cmsFolder 文件夹
     * @return 结果
     */
    public int insertCmsFolder(CmsFolder cmsFolder);

    /**
     * 修改文件夹
     * 
     * @param cmsFolder 文件夹
     * @return 结果
     */
    public int updateCmsFolder(CmsFolder cmsFolder);

    /**
     * 删除文件夹
     * 
     * @param folderId 文件夹主键
     * @return 结果
     */
    public int deleteCmsFolderByFolderId(Long folderId);

    /**
     * 批量删除文件夹
     * 
     * @param folderIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCmsFolderByFolderIds(Long[] folderIds);

    /**
     * 校验文件夹名称是否唯一
     *
     * @param folderName 文件夹名称
     * @param parentId 父文件夹ID
     * @return 结果
     */
    public CmsFolder checkCmsFolderNameUnique(@Param("folderName") String folderName, @Param("parentId") Long parentId);

    /**
     * 根据ID查询所有子文件夹（正常状态）
     *
     * @param folderId 文件夹ID
     * @return 子文件夹数
     */
    public int selectNormalChildrenFolderById(Long folderId);

    /**
     * 根据ID查询所有子文件夹
     *
     * @param folderId 文件夹ID
     * @return 文件夹列表
     */
    public List<CmsFolder> selectChildrenFolderById(Long folderId);

    /**
     * 是否存在子节点
     *
     * @param folderId 文件夹ID
     * @return 结果
     */
    public int hasChildByFolderId(Long folderId);

    /**
     * 查询文件夹是否存在文件
     *
     * @param folderId 文件夹ID
     * @return 结果
     */
    public int checkFolderExistDocs(Long folderId);

    /**
     * 修改子元素关系
     *
     * @param folders 子元素
     * @return 结果
     */
    public int updateFolderChildren(@Param("folders") List<CmsFolder> folders);

    /**
     * 修改所在文件夹正常状态
     *
     * @param folderIds 文件夹ID组
     */
    public void updateFolderStatusNormal(Long[] folderIds);

    /**
     * 查询文件夹列表
     *
     * @param ancestorIds 文件夹
     * @return 文件夹集合
     */
    public List<CmsFolder> selectCmsFolderListByAncestor(@Param("ancestorIds") List<Long> ancestorIds);

}
