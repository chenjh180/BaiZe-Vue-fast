package com.ruoyi.cms.folder.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.cms.folder.domain.CmsFolder;
import com.ruoyi.cms.folder.domain.CmsTreeSelect;

/**
 * 文件夹Service接口
 * 
 * @author ning
 * @date 2023-06-25
 */
public interface ICmsFolderService extends IService<CmsFolder>
{
    /**
     * 查询文件夹
     * 
     * @param folderId 文件夹主键
     * @return 文件夹
     */
    public CmsFolder selectCmsFolderByFolderId(Long folderId);

    /**
     * 查询文件夹列表
     * 
     * @param cmsFolder 文件夹
     * @return 文件夹集合
     */
    public List<CmsFolder> selectCmsFolderList(CmsFolder cmsFolder);

    /**
     * 新增文件夹
     * 
     * @param cmsFolder 文件夹
     * @return 结果
     */
    public int insertCmsFolder(CmsFolder cmsFolder);

    /**
     * 修改文件夹
     * 
     * @param cmsFolder 文件夹
     * @return 结果
     */
    public int updateCmsFolder(CmsFolder cmsFolder);

    /**
     * 批量删除文件夹
     * 
     * @param folderIds 需要删除的文件夹主键集合
     * @return 结果
     */
    public int deleteCmsFolderByFolderIds(Long[] folderIds);

    /**
     * 删除文件夹信息
     * 
     * @param folderId 文件夹主键
     * @return 结果
     */
    public int deleteCmsFolderByFolderId(Long folderId);

    /**
     * 校验部门名称是否唯一
     *
     * @param cmsFolder 文件夹
     * @return 结果
     */
    public boolean checkCmsFolderNameUnique(CmsFolder cmsFolder);

    /**
     * 校验文件夹是否有数据权限
     *
     * @param folderId 文件夹id
     */
    public void checkCmsFolderDataScope(Long folderId);

    /**
     * 根据ID查询所有子文件夹（正常状态）
     *
     * @param folderId 文件夹ID
     * @return 子文件夹数
     */
    public int selectNormalChildrenFolderById(Long folderId);

    /**
     * 是否存在文件夹子节点
     *
     * @param folderId 文件夹ID
     * @return 结果
     */
    public boolean hasChildByFolderId(Long folderId);

    /**
     * 查询文件夹是否存在文件
     *
     * @param folderId 文件夹ID
     * @return 结果 true 存在 false 不存在
     */
    public boolean checkFolderExistDocs(Long folderId);

    /**
     * 查询文件夹树结构信息
     *
     * @param cmsFolder 文件夹信息
     * @return 文件夹树信息集合
     */
    public List<CmsTreeSelect> selectFolderTreeList(CmsFolder cmsFolder);

    /**
     * 查询文件夹列表
     *
     * @param ancestorIds 文件夹
     * @return 文件夹集合
     */
    public List<CmsFolder> selectCmsFolderListByAncestor(List<Long> ancestorIds);

}
