package com.ruoyi.cms.folder.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.cms.folder.domain.CmsFolder;
import com.ruoyi.cms.folder.domain.CmsTreeSelect;
import com.ruoyi.cms.folder.mapper.CmsFolderMapper;
import com.ruoyi.cms.folder.service.ICmsFolderService;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.project.system.domain.SysUser;

/**
 * 文件夹Service业务层处理
 * 
 * @author ning
 * @date 2023-06-25
 */
@Service
public class CmsFolderServiceImpl extends ServiceImpl<CmsFolderMapper, CmsFolder> implements ICmsFolderService 
{
    /**
     * 查询文件夹
     * 
     * @param folderId 文件夹主键
     * @return 文件夹
     */
    @Override
    public CmsFolder selectCmsFolderByFolderId(Long folderId)
    {
        return baseMapper.selectCmsFolderByFolderId(folderId);
    }

    /**
     * 查询文件夹列表
     * 
     * @param cmsFolder 文件夹
     * @return 文件夹
     */
    @Override
    public List<CmsFolder> selectCmsFolderList(CmsFolder cmsFolder)
    {
        return baseMapper.selectCmsFolderList(cmsFolder);
    }

    /**
     * 新增文件夹
     * 
     * @param cmsFolder 文件夹
     * @return 结果
     */
    @Override
    public int insertCmsFolder(CmsFolder cmsFolder)
    {
        if (cmsFolder.getParentId()!=0){
            CmsFolder info = baseMapper.selectCmsFolderByFolderId(cmsFolder.getParentId());
            // 如果父节点不为正常状态,则不允许新增子节点
            if (!UserConstants.DEPT_NORMAL.equals(info.getStatus()))
            {
                throw new ServiceException("文件夹停用，不允许新增");
            }
            cmsFolder.setAncestors(info.getAncestors() + "," + cmsFolder.getParentId());
        } else {
            cmsFolder.setAncestors(cmsFolder.getParentId().toString());
        }
//        cmsFolder.setCreateTime(DateUtils.getNowDate());
        return baseMapper.insertCmsFolder(cmsFolder);
    }

    /**
     * 修改文件夹
     * 
     * @param cmsFolder 文件夹
     * @return 结果
     */
    @Override
    public int updateCmsFolder(CmsFolder cmsFolder)
    {
        CmsFolder newParentFolder = baseMapper.selectCmsFolderByFolderId(cmsFolder.getParentId());
        CmsFolder oldFolder = baseMapper.selectCmsFolderByFolderId(cmsFolder.getFolderId());
        if (StringUtils.isNotNull(newParentFolder) && StringUtils.isNotNull(oldFolder))
        {
            String newAncestors = newParentFolder.getAncestors() + "," + newParentFolder.getFolderId();
            String oldAncestors = oldFolder.getAncestors();
            cmsFolder.setAncestors(newAncestors);
            updateFolderChildren(cmsFolder.getFolderId(), newAncestors, oldAncestors);
        } else {
            cmsFolder.setAncestors("0");
        }
        int result = baseMapper.updateCmsFolder(cmsFolder);
        if (UserConstants.DEPT_NORMAL.equals(cmsFolder.getStatus()) && StringUtils.isNotEmpty(cmsFolder.getAncestors())
                && !StringUtils.equals("0", cmsFolder.getAncestors()))
        {
            // 如果该部门是启用状态，则启用该部门的所有上级部门
            updateParentFolderStatusNormal(cmsFolder);
        }
        return result;
    }

    /**
     * 批量删除文件夹
     * 
     * @param folderIds 需要删除的文件夹主键
     * @return 结果
     */
    @Override
    public int deleteCmsFolderByFolderIds(Long[] folderIds)
    {
        return baseMapper.deleteCmsFolderByFolderIds(folderIds);
    }

    /**
     * 删除文件夹信息
     * 
     * @param folderId 文件夹主键
     * @return 结果
     */
    @Override
    public int deleteCmsFolderByFolderId(Long folderId)
    {
        return baseMapper.deleteCmsFolderByFolderId(folderId);
    }

    @Override
    public boolean checkCmsFolderNameUnique(CmsFolder cmsFolder) {
        Long folderId = StringUtils.isNull(cmsFolder.getFolderId()) ? -1L : cmsFolder.getFolderId();
        CmsFolder info = baseMapper.checkCmsFolderNameUnique(cmsFolder.getFolderName(), cmsFolder.getParentId());
        if (StringUtils.isNotNull(info) && info.getFolderId().longValue() != folderId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    @Override
    public void checkCmsFolderDataScope(Long folderId) {
        if (!SysUser.isAdmin(SecurityUtils.getUserId()))
        {
            CmsFolder folder = new CmsFolder();
            folder.setFolderId(folderId);
            List<CmsFolder> folders = this.selectCmsFolderList(folder);
            if (StringUtils.isEmpty(folders))
            {
                throw new ServiceException("没有权限访问文件夹数据！");
            }
        }
    }

    @Override
    public int selectNormalChildrenFolderById(Long folderId) {
        return baseMapper.selectNormalChildrenFolderById(folderId);
    }

    @Override
    public boolean hasChildByFolderId(Long folderId) {
        int result = baseMapper.hasChildByFolderId(folderId);
        return result > 0;
    }

    @Override
    public boolean checkFolderExistDocs(Long folderId) {
        int result = baseMapper.checkFolderExistDocs(folderId);
        return result > 0;
    }

    @Override
    public List<CmsTreeSelect> selectFolderTreeList(CmsFolder cmsFolder) {
        List<CmsFolder> folders = this.selectCmsFolderList(cmsFolder);
        return buildDeptTreeSelect(folders);
    }

    @Override
    public List<CmsFolder> selectCmsFolderListByAncestor(List<Long> ancestorIds) {
        return baseMapper.selectCmsFolderListByAncestor(ancestorIds);
    }

    public List<CmsFolder> buildFolderTree(List<CmsFolder> folders) {
        List<CmsFolder> returnList = new ArrayList<>();
        List<Long> tempList = folders.stream().map(CmsFolder::getFolderId).collect(Collectors.toList());
        for (CmsFolder folder : folders)
        {
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(folder.getParentId()))
            {
                recursionFn(folders, folder);
                returnList.add(folder);
            }
        }
        if (returnList.isEmpty())
        {
            returnList = folders;
        }
        return returnList;
    }

    public List<CmsTreeSelect> buildDeptTreeSelect(List<CmsFolder> folders) {
        List<CmsFolder> deptTrees = buildFolderTree(folders);
        return deptTrees.stream().map(CmsTreeSelect::new).collect(Collectors.toList());
    }

    /**
     * 修改子元素关系
     *
     * @param folderId 被修改的文件夹ID
     * @param newAncestors 新的父ID集合
     * @param oldAncestors 旧的父ID集合
     */
    public void updateFolderChildren(Long folderId, String newAncestors, String oldAncestors)
    {
        List<CmsFolder> children = baseMapper.selectChildrenFolderById(folderId);
        for (CmsFolder child : children)
        {
            child.setAncestors(child.getAncestors().replaceFirst(oldAncestors, newAncestors));
        }
        if (children.size() > 0)
        {
            baseMapper.updateFolderChildren(children);
        }
    }

    /**
     * 修改该文件夹的父级文件夹状态
     *
     * @param folder 当前文件夹
     */
    private void updateParentFolderStatusNormal(CmsFolder folder)
    {
        String ancestors = folder.getAncestors();
        Long[] deptIds = Convert.toLongArray(ancestors);
        baseMapper.updateFolderStatusNormal(deptIds);
    }

    /**
     * 递归列表
     */
    private void recursionFn(List<CmsFolder> list, CmsFolder t)
    {
        // 得到子节点列表
        List<CmsFolder> childList = getChildList(list, t);
        t.setChildren(childList);
        for (CmsFolder tChild : childList)
        {
            if (hasChild(list, tChild))
            {
                recursionFn(list, tChild);
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<CmsFolder> getChildList(List<CmsFolder> list, CmsFolder t)
    {
        List<CmsFolder> tlist = new ArrayList<>();
        Iterator<CmsFolder> it = list.iterator();
        while (it.hasNext())
        {
            CmsFolder n = (CmsFolder) it.next();
            if (StringUtils.isNotNull(n.getParentId()) && n.getParentId().longValue() == t.getFolderId().longValue())
            {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<CmsFolder> list, CmsFolder t)
    {
        return getChildList(list, t).size() > 0;
    }
}
