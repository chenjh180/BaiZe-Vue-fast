package com.ruoyi.cms.folder.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ruoyi.cms.folder.domain.CmsFolder;
import com.ruoyi.cms.folder.domain.CmsTreeSelect;
import com.ruoyi.cms.folder.service.ICmsFolderService;
import com.ruoyi.common.constant.ApiImplicitParamType;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.R;

import cn.dev33.satoken.annotation.SaCheckPermission;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

/**
 * 文件夹Controller
 * 
 * @author ning
 * @date 2023-06-25
 */
@Api(tags="文件夹管理")
@RestController
@RequestMapping("/cms/folder")
public class CmsFolderController extends BaseController
{
    @Autowired
    private ICmsFolderService cmsFolderService;

    /**
     * 查询文件夹列表
     */
    @ApiOperation("查询文件夹列表")
    @SaCheckPermission("cms:folder:list")
    @GetMapping("/list")
    public R<List<CmsFolder>> list(CmsFolder cmsFolder)
    {
        List<CmsFolder> list = cmsFolderService.selectCmsFolderList(cmsFolder);
        return success(list);
    }

    /**
     * 查询文件夹列表（排除节点）
     */
    @ApiOperation("查询文件夹列表（排除节点）")
    @ApiImplicitParam(name = "folderId", value = "文件夹ID", required = true, dataType = "Long", paramType = ApiImplicitParamType.PATH, dataTypeClass = Long.class)
    @SaCheckPermission("cms:folder:list")
    @GetMapping("/list/exclude/{folderId}")
    public R<List<CmsFolder>> excludeChild(@PathVariable(value = "folderId", required = false) Long folderId)
    {
        List<CmsFolder> folderIds = cmsFolderService.selectCmsFolderList(new CmsFolder());
        folderIds.removeIf(d -> d.getFolderId().intValue() == folderId || ArrayUtils.contains(StringUtils.split(d.getAncestors(), ","), folderId + ""));
        return success(folderIds);
    }

    /**
     * 导出文件夹列表
     */
    @ApiOperation("导出文件夹列表")
    @SaCheckPermission("cms:folder:export")
    @Log(title = "文件夹", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CmsFolder cmsFolder)
    {
        List<CmsFolder> list = cmsFolderService.selectCmsFolderList(cmsFolder);
        ExcelUtil<CmsFolder> util = new ExcelUtil<CmsFolder>(CmsFolder.class);
        util.exportExcel(response, list, "文件夹数据");
    }

    /**
     * 获取文件夹详细信息
     */
    @ApiOperation("获取文件夹详细信息")
    @SaCheckPermission("cms:folder:query")
    @GetMapping(value = "/{folderId}")
    public R<CmsFolder> getInfo(@PathVariable("folderId") Long folderId)
    {
        cmsFolderService.checkCmsFolderDataScope(folderId);
        return success(cmsFolderService.selectCmsFolderByFolderId(folderId));
    }

    /**
     * 新增文件夹
     */
    @ApiOperation("新增文件夹")
    @SaCheckPermission("cms:folder:add")
    @Log(title = "文件夹", businessType = BusinessType.INSERT)
    @PostMapping
    public R<Integer> add(@RequestBody CmsFolder cmsFolder)
    {
        if (!cmsFolderService.checkCmsFolderNameUnique(cmsFolder))
        {
            return error("新增文件夹'" + cmsFolder.getFolderName() + "'失败，文件夹名称已存在");
        }
//        cmsFolder.setCreateBy(getUsername());
        return success(cmsFolderService.insertCmsFolder(cmsFolder));
    }

    /**
     * 修改文件夹
     */
    @ApiOperation("修改文件夹")
    @SaCheckPermission("cms:folder:edit")
    @Log(title = "文件夹", businessType = BusinessType.UPDATE)
    @PutMapping
    public R<Integer> edit(@RequestBody CmsFolder cmsFolder)
    {
        Long folderId = cmsFolder.getFolderId();
        cmsFolderService.checkCmsFolderDataScope(folderId);
        if (!cmsFolderService.checkCmsFolderNameUnique(cmsFolder))
        {
            return error("修改文件夹'" + cmsFolder.getFolderName() + "'失败，文件夹名称已存在");
        }
        else if (cmsFolder.getParentId().equals(folderId))
        {
            return error("修改文件夹'" + cmsFolder.getFolderName() + "'失败，上级文件夹不能是自己");
        }
        else if (StringUtils.equals(UserConstants.DEPT_DISABLE, cmsFolder.getStatus()) && cmsFolderService.selectNormalChildrenFolderById(folderId) > 0)
        {
            return error("该文件夹包含未停用的子文件夹！");
        }
        else if (cmsFolder.getParentId()!=0)
        {
            CmsFolder parentFolder = cmsFolderService.selectCmsFolderByFolderId(cmsFolder.getParentId());
            if (parentFolder.getAncestors().contains(String.valueOf(cmsFolder.getFolderId())))
            {
                return error("修改文件夹'" + cmsFolder.getFolderName() + "'失败，上级文件夹不能是自己的子文件夹");
            }
        }
//        cmsFolder.setUpdateBy(getUsername());
        return success(cmsFolderService.updateCmsFolder(cmsFolder));
    }

    /**
     * 删除文件夹
     */
    @ApiOperation("删除文件夹")
    @ApiImplicitParam(name = "folderIds", value = "文件夹ID数组", required = true, dataType = "Long[]", paramType = ApiImplicitParamType.PATH, dataTypeClass = Long[].class)
    @SaCheckPermission("cms:folder:remove")
    @Log(title = "文件夹", businessType = BusinessType.DELETE)
	@DeleteMapping("/{folderIds}")
    public R<Integer> remove(@PathVariable Long[] folderIds)
    {
        for (Long folderId : folderIds) {
            if (cmsFolderService.hasChildByFolderId(folderId))
            {
                return warn("存在下级文件夹,不允许删除");
            }
            if (cmsFolderService.checkFolderExistDocs(folderId))
            {
                return warn("文件夹存在文件,不允许删除");
            }
            cmsFolderService.checkCmsFolderDataScope(folderId);
        }
        return success(cmsFolderService.deleteCmsFolderByFolderIds(folderIds));
    }

    /**
     * 获取文件夹树列表
     */
    @ApiOperation("获取文件夹树列表")
    @SaCheckPermission("cms:folder:list")
    @RequestMapping("/folderTreeList")
    public R<List<CmsTreeSelect>> folderTreeList(CmsFolder cmsFolder)
    {
        return success(cmsFolderService.selectFolderTreeList(cmsFolder));
    }
}
