# 项目相关配置
ruoyi:
  # 名称
  name: RuoYi
  # 版本
  version: 3.8.8
  # 版权年份
  copyrightYear: 2024
  # 实例演示开关
  demoEnabled: true
  # 文件路径 示例（ Windows配置D:/ruoyi/uploadPath，Linux配置 /home/ruoyi/uploadPath）
  profile: D:/ruoyi/uploadPath
  # 获取ip地址开关
  addressEnabled: true
  # 验证码类型 math 数字计算 char 字符验证
  #用滑块验证码，此配置作废
  #captchaType: math

# 开发环境配置
server:
  # 服务器的HTTP端口，默认为8080
  port: 8080
  servlet:
    # 应用的访问路径
    context-path: /
  #tomcat:
  #  # tomcat的URI编码
  #  uri-encoding: UTF-8
  #  # 连接数满后的排队数，默认为100
  #  accept-count: 1000
  #  threads:
  #    # tomcat最大线程数，默认为200
  #    max: 800
  #    # Tomcat启动初始化的线程数，默认值10
  #    min-spare: 100
# undertow 配置
  undertow:
    # HTTP post内容的最大大小。当值为-1时，默认值为大小是无限的
    max-http-post-size: -1
    # 以下的配置会影响buffer,这些buffer会用于服务器连接的IO操作,有点类似netty的池化内存管理
    # 每块buffer的空间大小,越小的空间被利用越充分
    buffer-size: 512
    # 是否分配的直接内存
    direct-buffers: true
    threads:
      # 设置IO线程数, 它主要执行非阻塞的任务,它们会负责多个连接, 默认设置每个CPU核心一个线程
      io: 8
      # 阻塞任务线程池, 当执行类似servlet请求阻塞操作, undertow会从这个线程池中取得线程,它的值设置取决于系统的负载
      worker: 256
      
# 日志配置
logging:
  level:
    com.ruoyi: debug
    org.springframework: warn

# 用户配置
user:
  password:
    # 密码最大错误次数
    maxRetryCount: 5
    # 密码锁定时间（默认10分钟）
    lockTime: 10

# Spring配置
spring:
  # 资源信息
  messages:
    # 国际化资源文件路径
    basename: i18n/messages
  profiles:
    active: druid
  # 文件上传
  servlet:
    multipart:
      # 单个文件大小
      max-file-size: 10MB
      # 设置总上传的文件大小
      max-request-size: 20MB
  # 服务模块
  devtools:
    restart:
      # 热部署开关
      enabled: true
  # redis 配置
  redis:
    # 地址
    host: localhost
    # 端口，默认为6379
    port: 6379
    # 数据库索引
    database: 0
    # 密码
    password: 123456
    # 连接超时时间
    timeout: 10s
    lettuce:
      pool:
        # 连接池中的最小空闲连接
        min-idle: 0
        # 连接池中的最大空闲连接
        max-idle: 8
        # 连接池的最大数据库连接数
        max-active: 200
        # #连接池最大阻塞等待时间（使用负值表示没有限制）
        max-wait: -1ms
  flyway:
    # 是否启用flyway
    enabled: false
    # 编码格式，默认UTF-8
    encoding: UTF-8
    # 迁移sql脚本文件存放路径，默认db/migration
    locations: classpath:db/migration
    # 迁移sql脚本文件名称的前缀，默认V
    sql-migration-prefix: V
    # 迁移sql脚本文件名称的分隔符，默认2个下划线__
    sql-migration-separator: __
    # 迁移sql脚本文件名称的后缀
    sql-migration-suffixes: .sql
    # 迁移时是否进行校验，默认true
    validate-on-migrate: true
    # flyway 的 clean 命令会删除指定 schema 下的所有 table, 生产务必禁掉。这个默认值是false 理论上作为默认配置是不科学的。
    clean-disabled: true
    # 如果数据库不是空表，需要设置成 true，否则启动报错
    baseline-on-migrate: true
    baseline-version: 0

# token配置
sa-token:
  # token 名称（同时也是 cookie 名称）
  token-name: Admin-Token
  # token 有效期（单位：秒） 默认30天，-1 代表永久有效
  timeout: 2592000
  # token 最低活跃频率（单位：秒），如果 token 超过此时间没有访问系统就会被冻结，默认-1 代表不限制，永不冻结
  active-timeout: -1
  # 是否允许同一账号多地同时登录 （为 true 时允许一起登录, 为 false 时新登录挤掉旧登录）
  is-concurrent: true
  # 在多人登录同一账号时，是否共用一个 token （为 true 时所有登录共用一个 token, 为 false 时每次登录新建一个 token）
  is-share: true
  # token 风格（默认可取值：uuid、simple-uuid、random-32、random-64、random-128、tik）
  token-style: random-32
  # 是否输出操作日志 
  is-log: true
  # 日志等级（trace、debug、info、warn、error、fatal），此值与 logLevelInt 联动
  log-level: trace
  # 日志等级 int 值（1=trace、2=debug、3=info、4=warn、5=error、6=fatal），此值与 logLevel 联动
  log-level-int: 1
  # 同一账号最大登录数量，-1代表不限 （只有在 isConcurrent=true，isShare=false 时此配置才有效）
  max-login-count: -1
  # 是否在登录后将 Token 写入到响应头
  is-write-header: true
  # 是否打开自动续签 （如果此值为true，框架会在每次直接或间接调用 getLoginId() 时进行一次过期检查与续签操作）
  auto-renew: true
  # token前缀，例如填写 Bearer 实际传参 satoken: Bearer xxxx-xxxx-xxxx-xxxx
  #token-prefix: "Bearer "
  # 是否尝试从 请求体 里读取 Token
  is-read-body: true
  # 是否尝试从 header 里读取 Token
  is-read-header: true
  # 是否尝试从 cookie 里读取 Token，此值为 false 后，StpUtil.login(id) 登录时也不会再往前端注入Cookie
  is-read-cookie: true
   
# MyBatis Plus配置
mybatis-plus:
  # 搜索指定包别名
  typeAliasesPackage: com.ruoyi.project.**.domain,com.ruoyi.cms.**.domain
  # 配置mapper的扫描，找到所有的mapper.xml映射文件
  mapperLocations: classpath*:mybatis/**/*Mapper.xml
  # 加载全局的配置文件
  configLocation: classpath:mybatis/mybatis-config.xml

# PageHelper分页插件
pagehelper: 
  helperDialect: postgresql
  reasonable: true
  supportMethodsArguments: true
  params: count=countSql 

# Swagger配置
swagger:
  # 是否开启swagger
  enabled: true
  # 请求前缀
  pathMapping: /dev-api

# 防止XSS攻击
xss:
  # 过滤开关
  enabled: true
  # 排除链接（多个用逗号分隔）
  excludes: /system/notice
  # 匹配链接
  urlPatterns: /system/*,/monitor/*,/tool/*
  
# 代码生成
gen:
  # 作者
  author: ruoyi
  # 默认生成包路径 system 需改成自己的模块名称 如 system monitor tool
  packageName: com.ruoyi.project.system
  # 自动去除表前缀，默认是true
  autoRemovePre: false
  # 表前缀（生成类名不会包含表前缀，多个用逗号分隔）
  tablePrefix: sys_

# 雪花id 
snowflake:
  workerId: 0
  datacenterId: 0

# 动态脚本
magic-api:
  web: /magic/web
  editor-config: classpath:./magic-editor-config.js
  task:
    thread-name-prefix: magic-task- #线程池名字前缀
    pool:
      size: 8 #线程池大小，默认值为CPU核心数
    shutdown:
      awaitTermination: false #关闭时是否等待任务执行完毕，默认为false
      awaitTerminationPeriod: 10s # 关闭时最多等待任务执行完毕的时间
  resource:
    type: database              # 配置接口存储方式，这里选择存在数据库中
    table-name: magic_api_file  # 数据库中的表名
    prefix: /magic-api          # 前缀
  prefix: /s8a/                 # 接口前缀，可以不配置
  banner: true # 打印banner
  auto-import-module: db,log     # 自动导入的模块
  auto-import-package: cn.hutool.core.*,java.lang.*,java.util.*   # 自动导包
  sql-column-case: camel        #启用驼峰命名转换
  support-cross-domain: true # 跨域支持，默认开启
  show-sql: true #配置打印SQL
  allow-override: true #禁止覆盖应用接口
  #secret-key: 123456789 # 远程推送时的秘钥，未配置则不开启推送
  throw-exception: true         # 执行出错时，异常将抛出处理
  compile-cache-size: 500 #配置编译缓存容量
  #persistence-response-body: true #是否持久化保存ResponseBody
  thread-pool-executor-size: 8 # async语句的线程池大小
  backup: #备份相关配置
    enable: true #是否启用
    max-history: 30 #备份保留天数，-1为永久保留
    #datasource: magic  #指定数据源（单数据源时无需配置，多数据源时默认使用主数据源，如果存在其他数据源中需要指定。）
    table-name: magic_api_backup #使用数据库存储备份时的表名
  response: |- #配置JSON格式，格式为magic-script中的表达式
    {
      code: code,
      message: message,
      data,
      timestamp,
      requestTime,
      executeTime,
    }
  response-code:
    success: 200                #执行成功的code值
    invalid: 400                #参数验证未通过的code值
    exception: 500              #执行出现异常的code值
  crud: # CRUD相关配置
    logic-delete-column: deleted #逻辑删除列
    logic-delete-value: 2       #逻辑删除值
  page:
    size: pageSize              # 页大小的请求参数名称     与若依保持一致
    page: pageNum               # 页码的请求参数名称       与若依保持一致
    default-page: 1             # 未传页码时的默认首页
    default-size: 10            # 未传页大小时的默认页大小
  date-pattern: # 配置请求参数支持的日期格式
    - yyyy-MM-dd
    - yyyy-MM-dd HH:mm:ss
    - yyyyMMddHHmmss
    - yyyyMMdd
  cache: # 缓存相关配置
    capacity: 10000 #缓存容量
    ttl: -1 # 永不过期
    enable: true # 启用缓存
  security:  # 安全配置
    username: ruoyi # 登录用的用户名
    password: 123456 # 登录用的密码
  #swagger:
  #  version: 1.0
  #  description: MagicAPI 接口信息
  #  title: MagicAPI Swagger Docs
  #  name: MagicAPI 接口
  #  location: /v2/api-docs/magic-api/swagger2.json
  debug:
    timeout: 60 # 断点超时时间，默认60s
    
    
# 滑块验证码
aj:
  captcha:
    # 缓存类型
    cache-type: redis
    # blockPuzzle 滑块 clickWord 文字点选  default默认两者都实例化
    type: blockPuzzle
    # 右下角显示字
    water-mark: ruoyi.vip
    # 校验滑动拼图允许误差偏移量(默认5像素)
    slip-offset: 5
    # aes加密坐标开启或者禁用(true|false)
    aes-status: true
    # 滑动干扰项(0/1/2)
    interference-options: 2

# 配置文件敏感数据加密 
jasypt:
  encryptor:
    # -Djasypt.encryptor.password=qSgwk6kgMA7
    password: qSgwk6kgMA7
    algorithm: PBEWithMD5AndDES
    iv-generator-classname: org.jasypt.iv.NoIvGenerator
    
# 微信小程序配置
wechat-mini:
  appid: wx90a74703c0ac0ffb
  secret: fd81e9d1ee91a3bbb9065480a2f913d4

# 创建用户的默认信息
user-default: 
  # 默认部门
  dept: 100
  # 默认角色
  roleIds:
    - 2
  # 默认岗位 
  postIds:
    - 4
  # 默认性别：2 未知
  sex: 2
  # 默认头像
  avatar: 
  # 默认昵称前缀
  nickname-prefix: "微信用户"
  # 默认昵称后缀类型
  # str 随机字符串：大小写英文
  # num 随机数字
  nickname-suffix-type: "str" 
  # 默认昵称后缀长度
  nickname-suffix-length: 9
  
# 极简 HTTP 调用 API 框架
forest:
  bean-id: config0 # 在spring上下文中bean的id，默认值为forestConfiguration
  forest:
  backend: okhttp3             # 后端HTTP框架（默认为 okhttp3）
  max-connections: 1000        # 连接池最大连接数（默认为 500）
  max-route-connections: 500   # 每个路由的最大连接数（默认为 500）
  max-request-queue-size: 100  # [自v1.5.22版本起可用] 最大请求等待队列大小
  max-async-thread-size: 300   # [自v1.5.21版本起可用] 最大异步线程数
  max-async-queue-size: 16     # [自v1.5.22版本起可用] 最大异步线程池队列大小
  timeout: 3000                # [已不推荐使用] 请求超时时间，单位为毫秒（默认为 3000）
  connect-timeout: 3000        # 连接超时时间，单位为毫秒（默认为 timeout）
  read-timeout: 3000           # 数据读取超时时间，单位为毫秒（默认为 timeout）
  max-retry-count: 0           # 请求失败后重试次数（默认为 0 次不重试）
  ssl-protocol: TLS            # 单向验证的HTTPS的默认TLS协议（默认为 TLS）
  log-enabled: true            # 打开或关闭日志（默认为 true）
  log-request: true            # 打开/关闭Forest请求日志（默认为 true）
  log-response-status: true    # 打开/关闭Forest响应状态日志（默认为 true）
  log-response-content: true   # 打开/关闭Forest响应内容日志（默认为 false）
  async-mode: platform         # [自v1.5.27版本起可用] 异步模式（默认为 platform）
  variables:
    usernameDemo: foo      # 声明全局变量，变量名: username，变量值: foo
    userpwdDemo: bar       # 声明全局变量，变量名: userpwd，变量值: bar


# 不校验token
sa-router:
  not-match:
    # 验证码
    - /captcha/get     # 滑动验证码
    - /captcha/check   # 滑动验证码
#    - /captchaImage     # 图片验证码，已废弃
    # 登录
    - /login           
    - /weChatMini/login # 微信小程序登录
    # 静态资源
    - /**/*.html
    - /**/*.css
    - /**/*.js
    - /swagger-ui/**
    - /swagger-resources/**
    - /swagger-ui.html
    - /*/api-docs
    #
    - /profile/**
    - /common/download**
    - /common/download/resource**
    # 监控
    - /webjars/**
    - /druid/**
    # magic-api
    - /magic/web/**
#    - /s8a/**          # magicapi 接口
    - /favicon.ico
    # monitoring 监控
    - /monitoring/**
    # 切换语言
    - /changeLanguage
    
# Minio配置
minio:
  url: http://localhost:9000
  accessKey: minioadmin
  secretKey: minioadmin
  bucketName: ruoyi
  

javamelody:
  # 启用JavaMelody自动配置（可选，默认值：true）
  enabled: true
  # 要从监视中排除的数据源名称（可选，以逗号分隔）
  excluded-datasources: secretSource,topSecretSource
  # 启用对Spring服务和控制器的监视（可选，默认值：true）
  spring-monitoring-enabled: true
  # JavaMelody的初始化参数（可选）
  # See: https://github.com/javamelody/javamelody/wiki/UserGuide#6-optional-parameters
  init-parameters:
    # 记录http请求
    log: true
    # 从监视中排除图像，css，字体和js网址
    #url-exclude-pattern: (/webjars/.*|/css/.*|/images/.*|/fonts/.*|/js/.*)
    # 汇总http请求中的数字
    #http-transform-pattern: \d+
    # 添加基本身份验证
    authorized-users: ruoyi:123456
    # 更改默认存储目录：
    #storage-directory: /tmp/javamelody
    # 更改默认的“ / monitoring”路径：
    #monitoring-path: /admin/performance

knife4j:
  enable: true
  setting:
    # Knife4j默认显示中文(zh-CN),如果开发者想直接显示英文(en-US)，在通过该配置进行设置即可
    language: zh-CN
    enable-swagger-models: true
    enable-document-manage: true
    swagger-model-name: 实体类列表
    enable-version: false
    enable-reload-cache-parameter: false
    enable-after-script: true
    enable-filter-multipart-api-method-type: POST
    enable-filter-multipart-apis: false
    enable-request-cache: true
    enable-host: false
    enable-host-text: 192.168.0.193:8000
    enable-home-custom: true
    home-custom-path: classpath:markdown/home.md
    enable-search: false
    enable-footer: false
    enable-footer-custom: true
    footer-custom-content: Apache License 2.0 | Copyright  2019-[浙江八一菜刀股份有限公司](https://gitee.com/xiaoym/knife4j)
    enable-dynamic-parameter: false
    enable-debug: true
    enable-open-api: false
    enable-group: true
  cors: false
  production: false
  basic:
    enable: true
    username: ruoyi
    password: 123456
