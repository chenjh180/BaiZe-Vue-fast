--
-- PostgreSQL database dump
--

-- Dumped from database version 12.10
-- Dumped by pg_dump version 12.10

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: find_in_set(bigint, character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.find_in_set(bigint, character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
DECLARE
  STR ALIAS FOR $1;
  STRS ALIAS FOR $2;
  POS INTEGER;
  STATUS BOOLEAN;
BEGIN
	SELECT POSITION( ','||STR||',' IN ','||STRS||',') INTO POS;
	IF POS > 0 THEN
	  STATUS = TRUE;
	ELSE
	  STATUS = FALSE;
	END IF;
	RETURN STATUS; 
END;
$_$;


--
-- Name: substring_index(character varying, character varying, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.substring_index(character varying, character varying, integer) RETURNS character varying
    LANGUAGE plpgsql IMMUTABLE STRICT
    AS $_$
DECLARE
tokens varchar[];
length integer ;
indexnum integer;
BEGIN
tokens := pg_catalog.string_to_array($1, $2);
length := pg_catalog.array_upper(tokens, 1);
indexnum := length - ($3 * -1) + 1;
IF $3 >= 0 THEN
RETURN pg_catalog.array_to_string(tokens[1:$3], $2);
ELSE
RETURN pg_catalog.array_to_string(tokens[indexnum:length], $2);
END IF;
END;
$_$;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: cms_docs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cms_docs (
    docs_id bigint NOT NULL,
    folder_id bigint,
    content_id bigint,
    docs_name character varying(30) DEFAULT ''::character varying,
    order_num integer DEFAULT 0,
    docs_type character varying(30),
    del_flag character(1) DEFAULT '0'::bpchar,
    create_by character varying(64) DEFAULT ''::character varying,
    create_time timestamp(6) without time zone,
    update_by character varying(64) DEFAULT ''::character varying,
    update_time timestamp(6) without time zone,
    content text DEFAULT ''::text,
    content_markdown text DEFAULT ''::text
);


--
-- Name: TABLE cms_docs; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.cms_docs IS '文件表';


--
-- Name: COLUMN cms_docs.docs_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.cms_docs.docs_id IS '文件id';


--
-- Name: COLUMN cms_docs.folder_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.cms_docs.folder_id IS '文件夹id';


--
-- Name: COLUMN cms_docs.content_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.cms_docs.content_id IS '文件内容id';


--
-- Name: COLUMN cms_docs.docs_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.cms_docs.docs_name IS '文件名称';


--
-- Name: COLUMN cms_docs.order_num; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.cms_docs.order_num IS '显示顺序';


--
-- Name: COLUMN cms_docs.docs_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.cms_docs.docs_type IS '文件类型';


--
-- Name: COLUMN cms_docs.del_flag; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.cms_docs.del_flag IS '删除标志（0代表存在 2代表删除）';


--
-- Name: COLUMN cms_docs.create_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.cms_docs.create_by IS '创建者';


--
-- Name: COLUMN cms_docs.create_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.cms_docs.create_time IS '创建时间';


--
-- Name: COLUMN cms_docs.update_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.cms_docs.update_by IS '更新者';


--
-- Name: COLUMN cms_docs.update_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.cms_docs.update_time IS '更新时间';


--
-- Name: COLUMN cms_docs.content; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.cms_docs.content IS '内容';


--
-- Name: COLUMN cms_docs.content_markdown; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.cms_docs.content_markdown IS 'Markdown格式内容';


--
-- Name: cms_folder; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cms_folder (
    folder_id bigint NOT NULL,
    parent_id bigint DEFAULT 0,
    ancestors character varying(50) DEFAULT ''::character varying,
    folder_name character varying(30) DEFAULT ''::character varying,
    order_num integer DEFAULT 0,
    status character(1) DEFAULT '0'::bpchar,
    del_flag character(1) DEFAULT '0'::bpchar,
    create_by character varying(64) DEFAULT ''::character varying,
    create_time timestamp(6) without time zone,
    update_by character varying(64) DEFAULT ''::character varying,
    update_time timestamp(6) without time zone
);


--
-- Name: TABLE cms_folder; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.cms_folder IS '文件夹表';


--
-- Name: COLUMN cms_folder.folder_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.cms_folder.folder_id IS '文件夹id';


--
-- Name: COLUMN cms_folder.parent_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.cms_folder.parent_id IS '父文件夹id';


--
-- Name: COLUMN cms_folder.ancestors; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.cms_folder.ancestors IS '祖级列表';


--
-- Name: COLUMN cms_folder.folder_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.cms_folder.folder_name IS '文件夹名称';


--
-- Name: COLUMN cms_folder.order_num; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.cms_folder.order_num IS '显示顺序';


--
-- Name: COLUMN cms_folder.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.cms_folder.status IS '文件夹状态（0正常 1停用）';


--
-- Name: COLUMN cms_folder.del_flag; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.cms_folder.del_flag IS '删除标志（0代表存在 2代表删除）';


--
-- Name: COLUMN cms_folder.create_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.cms_folder.create_by IS '创建者';


--
-- Name: COLUMN cms_folder.create_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.cms_folder.create_time IS '创建时间';


--
-- Name: COLUMN cms_folder.update_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.cms_folder.update_by IS '更新者';


--
-- Name: COLUMN cms_folder.update_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.cms_folder.update_time IS '更新时间';


--
-- Name: flyway_schema_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.flyway_schema_history (
    installed_rank integer NOT NULL,
    version character varying(50),
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp without time zone DEFAULT now() NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);


--
-- Name: gen_table; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.gen_table (
    table_id bigint NOT NULL,
    table_name character varying(200) DEFAULT ''::character varying,
    table_comment character varying(500) DEFAULT ''::character varying,
    sub_table_name character varying(64),
    sub_table_fk_name character varying(64),
    class_name character varying(100) DEFAULT ''::character varying,
    tpl_category character varying(200) DEFAULT 'crud'::character varying,
    tpl_web_type character varying(30) DEFAULT 'element-ui'::character varying,
    package_name character varying(100),
    module_name character varying(30),
    business_name character varying(30),
    function_name character varying(50),
    function_author character varying(50),
    gen_type character(1) DEFAULT '0'::bpchar,
    gen_path character varying(200) DEFAULT '/'::character varying,
    options character varying(1000),
    create_by character varying(64) DEFAULT ''::character varying,
    create_time timestamp(6) without time zone,
    update_by character varying(64) DEFAULT ''::character varying,
    update_time timestamp(6) without time zone,
    remark character varying(500)
);


--
-- Name: TABLE gen_table; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.gen_table IS '代码生成业务表';


--
-- Name: COLUMN gen_table.table_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table.table_id IS '编号';


--
-- Name: COLUMN gen_table.table_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table.table_name IS '表名称';


--
-- Name: COLUMN gen_table.table_comment; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table.table_comment IS '表描述';


--
-- Name: COLUMN gen_table.sub_table_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table.sub_table_name IS '关联子表的表名';


--
-- Name: COLUMN gen_table.sub_table_fk_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table.sub_table_fk_name IS '子表关联的外键名';


--
-- Name: COLUMN gen_table.class_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table.class_name IS '实体类名称';


--
-- Name: COLUMN gen_table.tpl_category; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table.tpl_category IS '使用的模板（crud单表操作 tree树表操作）';


--
-- Name: COLUMN gen_table.tpl_web_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table.tpl_web_type IS '前端模板类型（element-ui模版 element-plus模版）';


--
-- Name: COLUMN gen_table.package_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table.package_name IS '生成包路径';


--
-- Name: COLUMN gen_table.module_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table.module_name IS '生成模块名';


--
-- Name: COLUMN gen_table.business_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table.business_name IS '生成业务名';


--
-- Name: COLUMN gen_table.function_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table.function_name IS '生成功能名';


--
-- Name: COLUMN gen_table.function_author; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table.function_author IS '生成功能作者';


--
-- Name: COLUMN gen_table.gen_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table.gen_type IS '生成代码方式（0zip压缩包 1自定义路径）';


--
-- Name: COLUMN gen_table.gen_path; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table.gen_path IS '生成路径（不填默认项目路径）';


--
-- Name: COLUMN gen_table.options; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table.options IS '其它生成选项';


--
-- Name: COLUMN gen_table.create_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table.create_by IS '创建者';


--
-- Name: COLUMN gen_table.create_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table.create_time IS '创建时间';


--
-- Name: COLUMN gen_table.update_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table.update_by IS '更新者';


--
-- Name: COLUMN gen_table.update_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table.update_time IS '更新时间';


--
-- Name: COLUMN gen_table.remark; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table.remark IS '备注';


--
-- Name: gen_table_column; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.gen_table_column (
    column_id bigint NOT NULL,
    table_id bigint NOT NULL,
    column_name character varying(200),
    column_comment character varying(500),
    column_type character varying(100),
    java_type character varying(500),
    java_field character varying(200),
    is_pk character(1),
    is_increment character(1),
    is_required character(1),
    is_insert character(1),
    is_edit character(1),
    is_list character(1),
    is_query character(1),
    query_type character varying(200) DEFAULT 'EQ'::character varying,
    html_type character varying(200),
    dict_type character varying(200) DEFAULT ''::character varying,
    sort integer,
    create_by character varying(64) DEFAULT ''::character varying,
    create_time timestamp(6) without time zone,
    update_by character varying(64) DEFAULT ''::character varying,
    update_time timestamp(6) without time zone
);


--
-- Name: TABLE gen_table_column; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.gen_table_column IS '代码生成业务表字段';


--
-- Name: COLUMN gen_table_column.column_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table_column.column_id IS '编号';


--
-- Name: COLUMN gen_table_column.table_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table_column.table_id IS '归属表编号';


--
-- Name: COLUMN gen_table_column.column_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table_column.column_name IS '列名称';


--
-- Name: COLUMN gen_table_column.column_comment; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table_column.column_comment IS '列描述';


--
-- Name: COLUMN gen_table_column.column_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table_column.column_type IS '列类型';


--
-- Name: COLUMN gen_table_column.java_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table_column.java_type IS 'JAVA类型';


--
-- Name: COLUMN gen_table_column.java_field; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table_column.java_field IS 'JAVA字段名';


--
-- Name: COLUMN gen_table_column.is_pk; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table_column.is_pk IS '是否主键（1是）';


--
-- Name: COLUMN gen_table_column.is_increment; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table_column.is_increment IS '是否自增（1是）';


--
-- Name: COLUMN gen_table_column.is_required; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table_column.is_required IS '是否必填（1是）';


--
-- Name: COLUMN gen_table_column.is_insert; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table_column.is_insert IS '是否为插入字段（1是）';


--
-- Name: COLUMN gen_table_column.is_edit; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table_column.is_edit IS '是否编辑字段（1是）';


--
-- Name: COLUMN gen_table_column.is_list; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table_column.is_list IS '是否列表字段（1是）';


--
-- Name: COLUMN gen_table_column.is_query; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table_column.is_query IS '是否查询字段（1是）';


--
-- Name: COLUMN gen_table_column.query_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table_column.query_type IS '查询方式（等于、不等于、大于、小于、范围）';


--
-- Name: COLUMN gen_table_column.html_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table_column.html_type IS '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）';


--
-- Name: COLUMN gen_table_column.dict_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table_column.dict_type IS '字典类型';


--
-- Name: COLUMN gen_table_column.sort; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table_column.sort IS '排序';


--
-- Name: COLUMN gen_table_column.create_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table_column.create_by IS '创建者';


--
-- Name: COLUMN gen_table_column.create_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table_column.create_time IS '创建时间';


--
-- Name: COLUMN gen_table_column.update_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table_column.update_by IS '更新者';


--
-- Name: COLUMN gen_table_column.update_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gen_table_column.update_time IS '更新时间';


--
-- Name: list_column; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.list_column AS
 SELECT c.relname AS table_name,
    a.attname AS column_name,
    d.description AS column_comment,
        CASE
            WHEN (a.attnotnull AND (con.conname IS NULL)) THEN 1
            ELSE 0
        END AS is_required,
        CASE
            WHEN (con.conname IS NOT NULL) THEN 1
            ELSE 0
        END AS is_pk,
    a.attnum AS sort,
        CASE
            WHEN ("position"(pg_get_expr(ad.adbin, ad.adrelid), ((((c.relname)::text || '_'::text) || (a.attname)::text) || '_seq'::text)) > 0) THEN 1
            ELSE 0
        END AS is_increment,
    btrim(
        CASE
            WHEN ((t.typelem <> (0)::oid) AND (t.typlen = '-1'::integer)) THEN 'ARRAY'::text
            ELSE
            CASE
                WHEN (t.typtype = 'd'::"char") THEN format_type(t.typbasetype, NULL::integer)
                ELSE format_type(a.atttypid, NULL::integer)
            END
        END, '"'::text) AS column_type
   FROM (((((pg_attribute a
     JOIN (pg_class c
     JOIN pg_namespace n ON ((c.relnamespace = n.oid))) ON ((a.attrelid = c.oid)))
     LEFT JOIN pg_description d ON (((d.objoid = c.oid) AND (a.attnum = d.objsubid))))
     LEFT JOIN pg_constraint con ON (((con.conrelid = c.oid) AND (a.attnum = ANY (con.conkey)))))
     LEFT JOIN pg_attrdef ad ON (((a.attrelid = ad.adrelid) AND (a.attnum = ad.adnum))))
     LEFT JOIN pg_type t ON ((a.atttypid = t.oid)))
  WHERE ((c.relkind = ANY (ARRAY['r'::"char", 'p'::"char"])) AND (a.attnum > 0) AND (n.nspname = 'public'::name) AND (NOT a.attisdropped))
  ORDER BY c.relname, a.attnum;


--
-- Name: list_table; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.list_table AS
 SELECT c.relname AS table_name,
    obj_description(c.oid) AS table_comment,
    CURRENT_TIMESTAMP AS create_time,
    CURRENT_TIMESTAMP AS update_time
   FROM (pg_class c
     LEFT JOIN pg_namespace n ON ((n.oid = c.relnamespace)))
  WHERE ((c.relkind = ANY (ARRAY['r'::"char", 'p'::"char"])) AND (c.relname !~~ 'spatial_%'::text) AND (n.nspname = 'public'::name) AND (n.nspname <> ''::name));


--
-- Name: magic_api_backup; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.magic_api_backup (
    id character varying(32) NOT NULL,
    create_date bigint NOT NULL,
    tag character varying(32),
    type character varying(32),
    name character varying(64),
    content text,
    create_by character varying(64)
);


--
-- Name: COLUMN magic_api_backup.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.magic_api_backup.id IS '原对象ID';


--
-- Name: COLUMN magic_api_backup.create_date; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.magic_api_backup.create_date IS '备份时间';


--
-- Name: COLUMN magic_api_backup.tag; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.magic_api_backup.tag IS '标签';


--
-- Name: COLUMN magic_api_backup.type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.magic_api_backup.type IS '类型';


--
-- Name: COLUMN magic_api_backup.name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.magic_api_backup.name IS '原名称';


--
-- Name: COLUMN magic_api_backup.content; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.magic_api_backup.content IS '备份内容';


--
-- Name: COLUMN magic_api_backup.create_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.magic_api_backup.create_by IS '操作人';


--
-- Name: magic_api_file; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.magic_api_file (
    file_path character varying(512) NOT NULL,
    file_content text
);


--
-- Name: qrtz_blob_triggers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qrtz_blob_triggers (
    sched_name character varying(120) NOT NULL,
    trigger_name character varying(200) NOT NULL,
    trigger_group character varying(200) NOT NULL,
    blob_data bytea
);


--
-- Name: TABLE qrtz_blob_triggers; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.qrtz_blob_triggers IS 'Blob类型的触发器表';


--
-- Name: COLUMN qrtz_blob_triggers.sched_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_blob_triggers.sched_name IS '调度名称';


--
-- Name: COLUMN qrtz_blob_triggers.trigger_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_blob_triggers.trigger_name IS 'qrtz_triggers表trigger_name的外键';


--
-- Name: COLUMN qrtz_blob_triggers.trigger_group; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_blob_triggers.trigger_group IS 'qrtz_triggers表trigger_group的外键';


--
-- Name: COLUMN qrtz_blob_triggers.blob_data; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_blob_triggers.blob_data IS '存放持久化Trigger对象';


--
-- Name: qrtz_calendars; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qrtz_calendars (
    sched_name character varying(120) NOT NULL,
    calendar_name character varying(200) NOT NULL,
    calendar bytea NOT NULL
);


--
-- Name: TABLE qrtz_calendars; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.qrtz_calendars IS '日历信息表';


--
-- Name: COLUMN qrtz_calendars.sched_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_calendars.sched_name IS '调度名称';


--
-- Name: COLUMN qrtz_calendars.calendar_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_calendars.calendar_name IS '日历名称';


--
-- Name: COLUMN qrtz_calendars.calendar; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_calendars.calendar IS '存放持久化calendar对象';


--
-- Name: qrtz_cron_triggers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qrtz_cron_triggers (
    sched_name character varying(120) NOT NULL,
    trigger_name character varying(200) NOT NULL,
    trigger_group character varying(200) NOT NULL,
    cron_expression character varying(200) NOT NULL,
    time_zone_id character varying(80)
);


--
-- Name: TABLE qrtz_cron_triggers; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.qrtz_cron_triggers IS 'Cron类型的触发器表';


--
-- Name: COLUMN qrtz_cron_triggers.sched_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_cron_triggers.sched_name IS '调度名称';


--
-- Name: COLUMN qrtz_cron_triggers.trigger_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_cron_triggers.trigger_name IS 'qrtz_triggers表trigger_name的外键';


--
-- Name: COLUMN qrtz_cron_triggers.trigger_group; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_cron_triggers.trigger_group IS 'qrtz_triggers表trigger_group的外键';


--
-- Name: COLUMN qrtz_cron_triggers.cron_expression; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_cron_triggers.cron_expression IS 'cron表达式';


--
-- Name: COLUMN qrtz_cron_triggers.time_zone_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_cron_triggers.time_zone_id IS '时区';


--
-- Name: qrtz_fired_triggers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qrtz_fired_triggers (
    sched_name character varying(120) NOT NULL,
    entry_id character varying(95) NOT NULL,
    trigger_name character varying(200) NOT NULL,
    trigger_group character varying(200) NOT NULL,
    instance_name character varying(200) NOT NULL,
    fired_time bigint NOT NULL,
    sched_time bigint NOT NULL,
    priority integer NOT NULL,
    state character varying(16) NOT NULL,
    job_name character varying(200),
    job_group character varying(200),
    is_nonconcurrent character varying(1),
    requests_recovery character varying(1)
);


--
-- Name: TABLE qrtz_fired_triggers; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.qrtz_fired_triggers IS '已触发的触发器表';


--
-- Name: COLUMN qrtz_fired_triggers.sched_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_fired_triggers.sched_name IS '调度名称';


--
-- Name: COLUMN qrtz_fired_triggers.entry_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_fired_triggers.entry_id IS '调度器实例id';


--
-- Name: COLUMN qrtz_fired_triggers.trigger_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_fired_triggers.trigger_name IS 'qrtz_triggers表trigger_name的外键';


--
-- Name: COLUMN qrtz_fired_triggers.trigger_group; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_fired_triggers.trigger_group IS 'qrtz_triggers表trigger_group的外键';


--
-- Name: COLUMN qrtz_fired_triggers.instance_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_fired_triggers.instance_name IS '调度器实例名';


--
-- Name: COLUMN qrtz_fired_triggers.fired_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_fired_triggers.fired_time IS '触发的时间';


--
-- Name: COLUMN qrtz_fired_triggers.sched_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_fired_triggers.sched_time IS '定时器制定的时间';


--
-- Name: COLUMN qrtz_fired_triggers.priority; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_fired_triggers.priority IS '优先级';


--
-- Name: COLUMN qrtz_fired_triggers.state; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_fired_triggers.state IS '状态';


--
-- Name: COLUMN qrtz_fired_triggers.job_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_fired_triggers.job_name IS '任务名称';


--
-- Name: COLUMN qrtz_fired_triggers.job_group; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_fired_triggers.job_group IS '任务组名';


--
-- Name: COLUMN qrtz_fired_triggers.is_nonconcurrent; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_fired_triggers.is_nonconcurrent IS '是否并发';


--
-- Name: COLUMN qrtz_fired_triggers.requests_recovery; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_fired_triggers.requests_recovery IS '是否接受恢复执行';


--
-- Name: qrtz_job_details; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qrtz_job_details (
    sched_name character varying(120) NOT NULL,
    job_name character varying(200) NOT NULL,
    job_group character varying(200) NOT NULL,
    description character varying(250),
    job_class_name character varying(250) NOT NULL,
    is_durable character varying(50) NOT NULL,
    is_nonconcurrent character varying(50) NOT NULL,
    is_update_data character varying(50) NOT NULL,
    requests_recovery character varying(50) NOT NULL,
    job_data bytea
);


--
-- Name: TABLE qrtz_job_details; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.qrtz_job_details IS '任务详细信息表';


--
-- Name: COLUMN qrtz_job_details.sched_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_job_details.sched_name IS '调度名称';


--
-- Name: COLUMN qrtz_job_details.job_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_job_details.job_name IS '任务名称';


--
-- Name: COLUMN qrtz_job_details.job_group; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_job_details.job_group IS '任务组名';


--
-- Name: COLUMN qrtz_job_details.description; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_job_details.description IS '相关介绍';


--
-- Name: COLUMN qrtz_job_details.job_class_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_job_details.job_class_name IS '执行任务类名称';


--
-- Name: COLUMN qrtz_job_details.is_durable; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_job_details.is_durable IS '是否持久化';


--
-- Name: COLUMN qrtz_job_details.is_nonconcurrent; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_job_details.is_nonconcurrent IS '是否并发';


--
-- Name: COLUMN qrtz_job_details.is_update_data; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_job_details.is_update_data IS '是否更新数据';


--
-- Name: COLUMN qrtz_job_details.requests_recovery; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_job_details.requests_recovery IS '是否接受恢复执行';


--
-- Name: COLUMN qrtz_job_details.job_data; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_job_details.job_data IS '存放持久化job对象';


--
-- Name: qrtz_locks; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qrtz_locks (
    sched_name character varying(120) NOT NULL,
    lock_name character varying(40) NOT NULL
);


--
-- Name: TABLE qrtz_locks; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.qrtz_locks IS '存储的悲观锁信息表';


--
-- Name: COLUMN qrtz_locks.sched_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_locks.sched_name IS '调度名称';


--
-- Name: COLUMN qrtz_locks.lock_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_locks.lock_name IS '悲观锁名称';


--
-- Name: qrtz_paused_trigger_grps; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qrtz_paused_trigger_grps (
    sched_name character varying(120) NOT NULL,
    trigger_group character varying(200) NOT NULL
);


--
-- Name: TABLE qrtz_paused_trigger_grps; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.qrtz_paused_trigger_grps IS '暂停的触发器表';


--
-- Name: COLUMN qrtz_paused_trigger_grps.sched_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_paused_trigger_grps.sched_name IS '调度名称';


--
-- Name: COLUMN qrtz_paused_trigger_grps.trigger_group; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_paused_trigger_grps.trigger_group IS 'qrtz_triggers表trigger_group的外键';


--
-- Name: qrtz_scheduler_state; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qrtz_scheduler_state (
    sched_name character varying(120) NOT NULL,
    instance_name character varying(200) NOT NULL,
    last_checkin_time bigint NOT NULL,
    checkin_interval bigint NOT NULL
);


--
-- Name: TABLE qrtz_scheduler_state; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.qrtz_scheduler_state IS '调度器状态表';


--
-- Name: COLUMN qrtz_scheduler_state.sched_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_scheduler_state.sched_name IS '调度名称';


--
-- Name: COLUMN qrtz_scheduler_state.instance_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_scheduler_state.instance_name IS '实例名称';


--
-- Name: COLUMN qrtz_scheduler_state.last_checkin_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_scheduler_state.last_checkin_time IS '上次检查时间';


--
-- Name: COLUMN qrtz_scheduler_state.checkin_interval; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_scheduler_state.checkin_interval IS '检查间隔时间';


--
-- Name: qrtz_simple_triggers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qrtz_simple_triggers (
    sched_name character varying(120) NOT NULL,
    trigger_name character varying(200) NOT NULL,
    trigger_group character varying(200) NOT NULL,
    repeat_count bigint NOT NULL,
    repeat_interval bigint NOT NULL,
    times_triggered bigint NOT NULL
);


--
-- Name: TABLE qrtz_simple_triggers; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.qrtz_simple_triggers IS '简单触发器的信息表';


--
-- Name: COLUMN qrtz_simple_triggers.sched_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_simple_triggers.sched_name IS '调度名称';


--
-- Name: COLUMN qrtz_simple_triggers.trigger_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_simple_triggers.trigger_name IS 'qrtz_triggers表trigger_name的外键';


--
-- Name: COLUMN qrtz_simple_triggers.trigger_group; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_simple_triggers.trigger_group IS 'qrtz_triggers表trigger_group的外键';


--
-- Name: COLUMN qrtz_simple_triggers.repeat_count; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_simple_triggers.repeat_count IS '重复的次数统计';


--
-- Name: COLUMN qrtz_simple_triggers.repeat_interval; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_simple_triggers.repeat_interval IS '重复的间隔时间';


--
-- Name: COLUMN qrtz_simple_triggers.times_triggered; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_simple_triggers.times_triggered IS '已经触发的次数';


--
-- Name: qrtz_simprop_triggers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qrtz_simprop_triggers (
    sched_name character varying(120) NOT NULL,
    trigger_name character varying(200) NOT NULL,
    trigger_group character varying(200) NOT NULL,
    str_prop_1 character varying(512),
    str_prop_2 character varying(512),
    str_prop_3 character varying(512),
    int_prop_1 integer,
    int_prop_2 integer,
    long_prop_1 bigint,
    long_prop_2 bigint,
    dec_prop_1 numeric(13,4),
    dec_prop_2 numeric(13,4),
    bool_prop_1 character varying(1),
    bool_prop_2 character varying(1)
);


--
-- Name: TABLE qrtz_simprop_triggers; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.qrtz_simprop_triggers IS '同步机制的行锁表';


--
-- Name: COLUMN qrtz_simprop_triggers.sched_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_simprop_triggers.sched_name IS '调度名称';


--
-- Name: COLUMN qrtz_simprop_triggers.trigger_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_simprop_triggers.trigger_name IS 'qrtz_triggers表trigger_name的外键';


--
-- Name: COLUMN qrtz_simprop_triggers.trigger_group; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_simprop_triggers.trigger_group IS 'qrtz_triggers表trigger_group的外键';


--
-- Name: COLUMN qrtz_simprop_triggers.str_prop_1; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_simprop_triggers.str_prop_1 IS 'String类型的trigger的第一个参数';


--
-- Name: COLUMN qrtz_simprop_triggers.str_prop_2; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_simprop_triggers.str_prop_2 IS 'String类型的trigger的第二个参数';


--
-- Name: COLUMN qrtz_simprop_triggers.str_prop_3; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_simprop_triggers.str_prop_3 IS 'String类型的trigger的第三个参数';


--
-- Name: COLUMN qrtz_simprop_triggers.int_prop_1; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_simprop_triggers.int_prop_1 IS 'int类型的trigger的第一个参数';


--
-- Name: COLUMN qrtz_simprop_triggers.int_prop_2; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_simprop_triggers.int_prop_2 IS 'int类型的trigger的第二个参数';


--
-- Name: COLUMN qrtz_simprop_triggers.long_prop_1; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_simprop_triggers.long_prop_1 IS 'long类型的trigger的第一个参数';


--
-- Name: COLUMN qrtz_simprop_triggers.long_prop_2; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_simprop_triggers.long_prop_2 IS 'long类型的trigger的第二个参数';


--
-- Name: COLUMN qrtz_simprop_triggers.dec_prop_1; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_simprop_triggers.dec_prop_1 IS 'decimal类型的trigger的第一个参数';


--
-- Name: COLUMN qrtz_simprop_triggers.dec_prop_2; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_simprop_triggers.dec_prop_2 IS 'decimal类型的trigger的第二个参数';


--
-- Name: COLUMN qrtz_simprop_triggers.bool_prop_1; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_simprop_triggers.bool_prop_1 IS 'Boolean类型的trigger的第一个参数';


--
-- Name: COLUMN qrtz_simprop_triggers.bool_prop_2; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_simprop_triggers.bool_prop_2 IS 'Boolean类型的trigger的第二个参数';


--
-- Name: qrtz_triggers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qrtz_triggers (
    sched_name character varying(120) NOT NULL,
    trigger_name character varying(200) NOT NULL,
    trigger_group character varying(200) NOT NULL,
    job_name character varying(200) NOT NULL,
    job_group character varying(200) NOT NULL,
    description character varying(250),
    next_fire_time bigint,
    prev_fire_time bigint,
    priority integer,
    trigger_state character varying(16) NOT NULL,
    trigger_type character varying(8) NOT NULL,
    start_time bigint NOT NULL,
    end_time bigint,
    calendar_name character varying(200),
    misfire_instr smallint,
    job_data bytea
);


--
-- Name: TABLE qrtz_triggers; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.qrtz_triggers IS '触发器详细信息表';


--
-- Name: COLUMN qrtz_triggers.sched_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_triggers.sched_name IS '调度名称';


--
-- Name: COLUMN qrtz_triggers.trigger_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_triggers.trigger_name IS '触发器的名字';


--
-- Name: COLUMN qrtz_triggers.trigger_group; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_triggers.trigger_group IS '触发器所属组的名字';


--
-- Name: COLUMN qrtz_triggers.job_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_triggers.job_name IS 'qrtz_job_details表job_name的外键';


--
-- Name: COLUMN qrtz_triggers.job_group; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_triggers.job_group IS 'qrtz_job_details表job_group的外键';


--
-- Name: COLUMN qrtz_triggers.description; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_triggers.description IS '相关介绍';


--
-- Name: COLUMN qrtz_triggers.next_fire_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_triggers.next_fire_time IS '上一次触发时间（毫秒）';


--
-- Name: COLUMN qrtz_triggers.prev_fire_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_triggers.prev_fire_time IS '下一次触发时间（默认为-1表示不触发）';


--
-- Name: COLUMN qrtz_triggers.priority; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_triggers.priority IS '优先级';


--
-- Name: COLUMN qrtz_triggers.trigger_state; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_triggers.trigger_state IS '触发器状态';


--
-- Name: COLUMN qrtz_triggers.trigger_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_triggers.trigger_type IS '触发器的类型';


--
-- Name: COLUMN qrtz_triggers.start_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_triggers.start_time IS '开始时间';


--
-- Name: COLUMN qrtz_triggers.end_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_triggers.end_time IS '结束时间';


--
-- Name: COLUMN qrtz_triggers.calendar_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_triggers.calendar_name IS '日程表名称';


--
-- Name: COLUMN qrtz_triggers.misfire_instr; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_triggers.misfire_instr IS '补偿执行的策略';


--
-- Name: COLUMN qrtz_triggers.job_data; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qrtz_triggers.job_data IS '存放持久化job对象';


--
-- Name: sys_config; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_config (
    config_id bigint NOT NULL,
    config_name character varying(100) DEFAULT ''::character varying,
    config_key character varying(100) DEFAULT ''::character varying,
    config_value character varying(500) DEFAULT ''::character varying,
    config_type character(1) DEFAULT 'N'::bpchar,
    create_by character varying(64) DEFAULT ''::character varying,
    create_time timestamp(6) without time zone,
    update_by character varying(64) DEFAULT ''::character varying,
    update_time timestamp(6) without time zone,
    remark character varying(500)
);


--
-- Name: TABLE sys_config; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.sys_config IS '参数配置表';


--
-- Name: COLUMN sys_config.config_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_config.config_id IS '参数主键';


--
-- Name: COLUMN sys_config.config_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_config.config_name IS '参数名称';


--
-- Name: COLUMN sys_config.config_key; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_config.config_key IS '参数键名';


--
-- Name: COLUMN sys_config.config_value; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_config.config_value IS '参数键值';


--
-- Name: COLUMN sys_config.config_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_config.config_type IS '系统内置（Y是 N否）';


--
-- Name: COLUMN sys_config.create_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_config.create_by IS '创建者';


--
-- Name: COLUMN sys_config.create_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_config.create_time IS '创建时间';


--
-- Name: COLUMN sys_config.update_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_config.update_by IS '更新者';


--
-- Name: COLUMN sys_config.update_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_config.update_time IS '更新时间';


--
-- Name: COLUMN sys_config.remark; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_config.remark IS '备注';


--
-- Name: sys_dept; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_dept (
    dept_id bigint NOT NULL,
    parent_id bigint DEFAULT 0,
    ancestors character varying(50) DEFAULT ''::character varying,
    dept_name character varying(30) DEFAULT ''::character varying,
    order_num integer DEFAULT 0,
    leader character varying(20),
    phone character varying(11),
    email character varying(50),
    status character(1) DEFAULT '0'::bpchar,
    del_flag character(1) DEFAULT '0'::bpchar,
    create_by character varying(64) DEFAULT ''::character varying,
    create_time timestamp(6) without time zone,
    update_by character varying(64) DEFAULT ''::character varying,
    update_time timestamp(6) without time zone
);


--
-- Name: TABLE sys_dept; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.sys_dept IS '部门表';


--
-- Name: COLUMN sys_dept.dept_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dept.dept_id IS '部门id';


--
-- Name: COLUMN sys_dept.parent_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dept.parent_id IS '父部门id';


--
-- Name: COLUMN sys_dept.ancestors; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dept.ancestors IS '祖级列表';


--
-- Name: COLUMN sys_dept.dept_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dept.dept_name IS '部门名称';


--
-- Name: COLUMN sys_dept.order_num; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dept.order_num IS '显示顺序';


--
-- Name: COLUMN sys_dept.leader; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dept.leader IS '负责人';


--
-- Name: COLUMN sys_dept.phone; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dept.phone IS '联系电话';


--
-- Name: COLUMN sys_dept.email; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dept.email IS '邮箱';


--
-- Name: COLUMN sys_dept.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dept.status IS '部门状态（0正常 1停用）';


--
-- Name: COLUMN sys_dept.del_flag; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dept.del_flag IS '删除标志（0代表存在 2代表删除）';


--
-- Name: COLUMN sys_dept.create_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dept.create_by IS '创建者';


--
-- Name: COLUMN sys_dept.create_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dept.create_time IS '创建时间';


--
-- Name: COLUMN sys_dept.update_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dept.update_by IS '更新者';


--
-- Name: COLUMN sys_dept.update_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dept.update_time IS '更新时间';


--
-- Name: sys_dict_data; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_dict_data (
    dict_code bigint NOT NULL,
    dict_sort integer DEFAULT 0,
    dict_label character varying(100) DEFAULT ''::character varying,
    dict_value character varying(100) DEFAULT ''::character varying,
    dict_type character varying(100) DEFAULT ''::character varying,
    css_class character varying(100),
    list_class character varying(100),
    is_default character(1) DEFAULT 'N'::bpchar,
    status character(1) DEFAULT '0'::bpchar,
    create_by character varying(64) DEFAULT ''::character varying,
    create_time timestamp(6) without time zone,
    update_by character varying(64) DEFAULT ''::character varying,
    update_time timestamp(6) without time zone,
    remark character varying(500)
);


--
-- Name: TABLE sys_dict_data; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.sys_dict_data IS '字典数据表';


--
-- Name: COLUMN sys_dict_data.dict_code; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict_data.dict_code IS '字典编码';


--
-- Name: COLUMN sys_dict_data.dict_sort; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict_data.dict_sort IS '字典排序';


--
-- Name: COLUMN sys_dict_data.dict_label; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict_data.dict_label IS '字典标签';


--
-- Name: COLUMN sys_dict_data.dict_value; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict_data.dict_value IS '字典键值';


--
-- Name: COLUMN sys_dict_data.dict_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict_data.dict_type IS '字典类型';


--
-- Name: COLUMN sys_dict_data.css_class; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict_data.css_class IS '样式属性（其他样式扩展）';


--
-- Name: COLUMN sys_dict_data.list_class; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict_data.list_class IS '表格回显样式';


--
-- Name: COLUMN sys_dict_data.is_default; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict_data.is_default IS '是否默认（Y是 N否）';


--
-- Name: COLUMN sys_dict_data.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict_data.status IS '状态（0正常 1停用）';


--
-- Name: COLUMN sys_dict_data.create_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict_data.create_by IS '创建者';


--
-- Name: COLUMN sys_dict_data.create_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict_data.create_time IS '创建时间';


--
-- Name: COLUMN sys_dict_data.update_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict_data.update_by IS '更新者';


--
-- Name: COLUMN sys_dict_data.update_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict_data.update_time IS '更新时间';


--
-- Name: COLUMN sys_dict_data.remark; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict_data.remark IS '备注';


--
-- Name: sys_dict_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_dict_type (
    dict_id bigint NOT NULL,
    dict_name character varying(100) DEFAULT ''::character varying,
    dict_type character varying(100) DEFAULT ''::character varying,
    status character(1) DEFAULT '0'::bpchar,
    create_by character varying(64) DEFAULT ''::character varying,
    create_time timestamp(6) without time zone,
    update_by character varying(64) DEFAULT ''::character varying,
    update_time timestamp(6) without time zone,
    remark character varying(500)
);


--
-- Name: TABLE sys_dict_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.sys_dict_type IS '字典类型表';


--
-- Name: COLUMN sys_dict_type.dict_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict_type.dict_id IS '字典主键';


--
-- Name: COLUMN sys_dict_type.dict_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict_type.dict_name IS '字典名称';


--
-- Name: COLUMN sys_dict_type.dict_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict_type.dict_type IS '字典类型';


--
-- Name: COLUMN sys_dict_type.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict_type.status IS '状态（0正常 1停用）';


--
-- Name: COLUMN sys_dict_type.create_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict_type.create_by IS '创建者';


--
-- Name: COLUMN sys_dict_type.create_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict_type.create_time IS '创建时间';


--
-- Name: COLUMN sys_dict_type.update_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict_type.update_by IS '更新者';


--
-- Name: COLUMN sys_dict_type.update_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict_type.update_time IS '更新时间';


--
-- Name: COLUMN sys_dict_type.remark; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict_type.remark IS '备注';


--
-- Name: sys_job; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_job (
    job_id bigint NOT NULL,
    job_name character varying(64) DEFAULT ''::character varying NOT NULL,
    job_group character varying(64) DEFAULT 'DEFAULT'::character varying NOT NULL,
    invoke_target character varying(500) NOT NULL,
    cron_expression character varying(255) DEFAULT ''::character varying,
    misfire_policy character varying(20) DEFAULT '3'::character varying,
    concurrent character(1) DEFAULT '1'::bpchar,
    status character(1) DEFAULT '0'::bpchar,
    create_by character varying(64) DEFAULT ''::character varying,
    create_time timestamp(6) without time zone,
    update_by character varying(64) DEFAULT ''::character varying,
    update_time timestamp(6) without time zone,
    remark character varying(500)
);


--
-- Name: TABLE sys_job; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.sys_job IS '定时任务调度表';


--
-- Name: COLUMN sys_job.job_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_job.job_id IS '任务ID';


--
-- Name: COLUMN sys_job.job_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_job.job_name IS '任务名称';


--
-- Name: COLUMN sys_job.job_group; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_job.job_group IS '任务组名';


--
-- Name: COLUMN sys_job.invoke_target; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_job.invoke_target IS '调用目标字符串';


--
-- Name: COLUMN sys_job.cron_expression; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_job.cron_expression IS 'cron执行表达式';


--
-- Name: COLUMN sys_job.misfire_policy; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_job.misfire_policy IS '计划执行错误策略（1立即执行 2执行一次 3放弃执行）';


--
-- Name: COLUMN sys_job.concurrent; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_job.concurrent IS '是否并发执行（0允许 1禁止）';


--
-- Name: COLUMN sys_job.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_job.status IS '状态（0正常 1暂停）';


--
-- Name: COLUMN sys_job.create_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_job.create_by IS '创建者';


--
-- Name: COLUMN sys_job.create_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_job.create_time IS '创建时间';


--
-- Name: COLUMN sys_job.update_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_job.update_by IS '更新者';


--
-- Name: COLUMN sys_job.update_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_job.update_time IS '更新时间';


--
-- Name: COLUMN sys_job.remark; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_job.remark IS '备注信息';


--
-- Name: sys_job_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_job_log (
    job_log_id bigint NOT NULL,
    job_name character varying(64) NOT NULL,
    job_group character varying(64) NOT NULL,
    invoke_target character varying(500) NOT NULL,
    job_message character varying(500),
    status character(1) DEFAULT '0'::bpchar,
    exception_info character varying(2000) DEFAULT ''::character varying,
    create_time timestamp(6) without time zone
);


--
-- Name: TABLE sys_job_log; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.sys_job_log IS '定时任务调度日志表';


--
-- Name: COLUMN sys_job_log.job_log_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_job_log.job_log_id IS '任务日志ID';


--
-- Name: COLUMN sys_job_log.job_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_job_log.job_name IS '任务名称';


--
-- Name: COLUMN sys_job_log.job_group; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_job_log.job_group IS '任务组名';


--
-- Name: COLUMN sys_job_log.invoke_target; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_job_log.invoke_target IS '调用目标字符串';


--
-- Name: COLUMN sys_job_log.job_message; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_job_log.job_message IS '日志信息';


--
-- Name: COLUMN sys_job_log.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_job_log.status IS '执行状态（0正常 1失败）';


--
-- Name: COLUMN sys_job_log.exception_info; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_job_log.exception_info IS '异常信息';


--
-- Name: COLUMN sys_job_log.create_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_job_log.create_time IS '创建时间';


--
-- Name: sys_logininfor; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_logininfor (
    info_id bigint NOT NULL,
    user_name character varying(50) DEFAULT ''::character varying,
    ipaddr character varying(128) DEFAULT ''::character varying,
    login_location character varying(255) DEFAULT ''::character varying,
    browser character varying(50) DEFAULT ''::character varying,
    os character varying(50) DEFAULT ''::character varying,
    status character(1) DEFAULT '0'::bpchar,
    msg character varying(255) DEFAULT ''::character varying,
    login_time timestamp(6) without time zone,
    user_agent character varying(2000)
);


--
-- Name: TABLE sys_logininfor; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.sys_logininfor IS '系统访问记录';


--
-- Name: COLUMN sys_logininfor.info_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_logininfor.info_id IS '访问ID';


--
-- Name: COLUMN sys_logininfor.user_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_logininfor.user_name IS '用户账号';


--
-- Name: COLUMN sys_logininfor.ipaddr; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_logininfor.ipaddr IS '登录IP地址';


--
-- Name: COLUMN sys_logininfor.login_location; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_logininfor.login_location IS '登录地点';


--
-- Name: COLUMN sys_logininfor.browser; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_logininfor.browser IS '浏览器类型';


--
-- Name: COLUMN sys_logininfor.os; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_logininfor.os IS '操作系统';


--
-- Name: COLUMN sys_logininfor.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_logininfor.status IS '登录状态（0成功 1失败）';


--
-- Name: COLUMN sys_logininfor.msg; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_logininfor.msg IS '提示消息';


--
-- Name: COLUMN sys_logininfor.login_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_logininfor.login_time IS '访问时间';


--
-- Name: COLUMN sys_logininfor.user_agent; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_logininfor.user_agent IS '原始的userAgent';


--
-- Name: sys_menu; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_menu (
    menu_id bigint NOT NULL,
    menu_name character varying(50) NOT NULL,
    parent_id bigint DEFAULT 0,
    order_num integer DEFAULT 0,
    path character varying(200) DEFAULT ''::character varying,
    component character varying(255),
    query character varying(255),
    is_frame integer DEFAULT 1,
    is_cache integer DEFAULT 0,
    menu_type character(1) NOT NULL,
    visible character(1) DEFAULT '0'::bpchar,
    status character(1) DEFAULT '0'::bpchar,
    perms character varying(100),
    icon character varying(100) DEFAULT '#'::character varying,
    create_by character varying(64) DEFAULT ''::character varying,
    create_time timestamp(6) without time zone,
    update_by character varying(64) DEFAULT ''::character varying,
    update_time timestamp(6) without time zone,
    remark character varying(500) DEFAULT ''::character varying,
    route_name character varying(50) DEFAULT ''::character varying
);


--
-- Name: TABLE sys_menu; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.sys_menu IS '菜单权限表';


--
-- Name: COLUMN sys_menu.menu_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.menu_id IS '菜单ID';


--
-- Name: COLUMN sys_menu.menu_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.menu_name IS '菜单名称';


--
-- Name: COLUMN sys_menu.parent_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.parent_id IS '父菜单ID';


--
-- Name: COLUMN sys_menu.order_num; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.order_num IS '显示顺序';


--
-- Name: COLUMN sys_menu.path; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.path IS '路由地址';


--
-- Name: COLUMN sys_menu.component; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.component IS '组件路径';


--
-- Name: COLUMN sys_menu.query; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.query IS '路由参数';


--
-- Name: COLUMN sys_menu.is_frame; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.is_frame IS '是否为外链（0是 1否）';


--
-- Name: COLUMN sys_menu.is_cache; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.is_cache IS '是否缓存（0缓存 1不缓存）';


--
-- Name: COLUMN sys_menu.menu_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.menu_type IS '菜单类型（M目录 C菜单 F按钮）';


--
-- Name: COLUMN sys_menu.visible; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.visible IS '菜单状态（0显示 1隐藏）';


--
-- Name: COLUMN sys_menu.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.status IS '菜单状态（0正常 1停用）';


--
-- Name: COLUMN sys_menu.perms; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.perms IS '权限标识';


--
-- Name: COLUMN sys_menu.icon; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.icon IS '菜单图标';


--
-- Name: COLUMN sys_menu.create_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.create_by IS '创建者';


--
-- Name: COLUMN sys_menu.create_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.create_time IS '创建时间';


--
-- Name: COLUMN sys_menu.update_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.update_by IS '更新者';


--
-- Name: COLUMN sys_menu.update_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.update_time IS '更新时间';


--
-- Name: COLUMN sys_menu.remark; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.remark IS '备注';


--
-- Name: COLUMN sys_menu.route_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.route_name IS '路由名称';


--
-- Name: sys_notice; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_notice (
    notice_id bigint NOT NULL,
    notice_title character varying(50) NOT NULL,
    notice_type character(1) NOT NULL,
    notice_content text,
    status character(1) DEFAULT '0'::bpchar,
    create_by character varying(64) DEFAULT ''::character varying,
    create_time timestamp(6) without time zone,
    update_by character varying(64) DEFAULT ''::character varying,
    update_time timestamp(6) without time zone,
    remark character varying(255),
    content_type character(1) DEFAULT '1'::bpchar,
    notice_content_markdown text,
    platform character(1) DEFAULT '1'::bpchar
);


--
-- Name: TABLE sys_notice; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.sys_notice IS '通知公告表
2024年5月14日：notice_content 字段改为 text 类型';


--
-- Name: COLUMN sys_notice.notice_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_notice.notice_id IS '公告ID';


--
-- Name: COLUMN sys_notice.notice_title; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_notice.notice_title IS '公告标题';


--
-- Name: COLUMN sys_notice.notice_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_notice.notice_type IS '公告类型（1通知 2公告）';


--
-- Name: COLUMN sys_notice.notice_content; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_notice.notice_content IS '公告内容';


--
-- Name: COLUMN sys_notice.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_notice.status IS '公告状态（0正常 1关闭）';


--
-- Name: COLUMN sys_notice.create_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_notice.create_by IS '创建者';


--
-- Name: COLUMN sys_notice.create_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_notice.create_time IS '创建时间';


--
-- Name: COLUMN sys_notice.update_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_notice.update_by IS '更新者';


--
-- Name: COLUMN sys_notice.update_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_notice.update_time IS '更新时间';


--
-- Name: COLUMN sys_notice.remark; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_notice.remark IS '备注';


--
-- Name: COLUMN sys_notice.content_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_notice.content_type IS '文本编辑器类型 1 其他编辑器 2 markdown';


--
-- Name: COLUMN sys_notice.notice_content_markdown; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_notice.notice_content_markdown IS 'Markdown格式内容';


--
-- Name: COLUMN sys_notice.platform; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_notice.platform IS '1 应用用 2 管理后台用';


--
-- Name: sys_oper_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_oper_log (
    oper_id bigint NOT NULL,
    title character varying(50) DEFAULT ''::character varying,
    business_type integer DEFAULT 0,
    method character varying(500) DEFAULT ''::character varying,
    request_method character varying(30) DEFAULT ''::character varying,
    operator_type integer DEFAULT 0,
    oper_name character varying(50) DEFAULT ''::character varying,
    dept_name character varying(50) DEFAULT ''::character varying,
    oper_url character varying(255) DEFAULT ''::character varying,
    oper_ip character varying(128) DEFAULT ''::character varying,
    oper_location character varying(255) DEFAULT ''::character varying,
    oper_param character varying(5000) DEFAULT ''::character varying,
    json_result character varying(5000) DEFAULT ''::character varying,
    status integer DEFAULT 0,
    error_msg character varying(5000) DEFAULT ''::character varying,
    oper_time timestamp(6) without time zone,
    cost_time bigint DEFAULT 0
);


--
-- Name: TABLE sys_oper_log; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.sys_oper_log IS '操作日志记录';


--
-- Name: COLUMN sys_oper_log.oper_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_oper_log.oper_id IS '日志主键';


--
-- Name: COLUMN sys_oper_log.title; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_oper_log.title IS '模块标题';


--
-- Name: COLUMN sys_oper_log.business_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_oper_log.business_type IS '业务类型（0其它 1新增 2修改 3删除）';


--
-- Name: COLUMN sys_oper_log.method; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_oper_log.method IS '方法名称';


--
-- Name: COLUMN sys_oper_log.request_method; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_oper_log.request_method IS '请求方式';


--
-- Name: COLUMN sys_oper_log.operator_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_oper_log.operator_type IS '操作类别（0其它 1后台用户 2手机端用户）';


--
-- Name: COLUMN sys_oper_log.oper_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_oper_log.oper_name IS '操作人员';


--
-- Name: COLUMN sys_oper_log.dept_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_oper_log.dept_name IS '部门名称';


--
-- Name: COLUMN sys_oper_log.oper_url; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_oper_log.oper_url IS '请求URL';


--
-- Name: COLUMN sys_oper_log.oper_ip; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_oper_log.oper_ip IS '主机地址';


--
-- Name: COLUMN sys_oper_log.oper_location; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_oper_log.oper_location IS '操作地点';


--
-- Name: COLUMN sys_oper_log.oper_param; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_oper_log.oper_param IS '请求参数';


--
-- Name: COLUMN sys_oper_log.json_result; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_oper_log.json_result IS '返回参数';


--
-- Name: COLUMN sys_oper_log.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_oper_log.status IS '操作状态（0正常 1异常）';


--
-- Name: COLUMN sys_oper_log.error_msg; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_oper_log.error_msg IS '错误消息';


--
-- Name: COLUMN sys_oper_log.oper_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_oper_log.oper_time IS '操作时间';


--
-- Name: COLUMN sys_oper_log.cost_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_oper_log.cost_time IS '消耗时间';


--
-- Name: sys_post; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_post (
    post_id bigint NOT NULL,
    post_code character varying(64) NOT NULL,
    post_name character varying(50) NOT NULL,
    post_sort integer NOT NULL,
    status character(1) NOT NULL,
    create_by character varying(64) DEFAULT ''::character varying,
    create_time timestamp(6) without time zone,
    update_by character varying(64) DEFAULT ''::character varying,
    update_time timestamp(6) without time zone,
    remark character varying(500)
);


--
-- Name: TABLE sys_post; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.sys_post IS '岗位信息表';


--
-- Name: COLUMN sys_post.post_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_post.post_id IS '岗位ID';


--
-- Name: COLUMN sys_post.post_code; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_post.post_code IS '岗位编码';


--
-- Name: COLUMN sys_post.post_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_post.post_name IS '岗位名称';


--
-- Name: COLUMN sys_post.post_sort; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_post.post_sort IS '显示顺序';


--
-- Name: COLUMN sys_post.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_post.status IS '状态（0正常 1停用）';


--
-- Name: COLUMN sys_post.create_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_post.create_by IS '创建者';


--
-- Name: COLUMN sys_post.create_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_post.create_time IS '创建时间';


--
-- Name: COLUMN sys_post.update_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_post.update_by IS '更新者';


--
-- Name: COLUMN sys_post.update_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_post.update_time IS '更新时间';


--
-- Name: COLUMN sys_post.remark; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_post.remark IS '备注';


--
-- Name: sys_role; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_role (
    role_id bigint NOT NULL,
    role_name character varying(30) NOT NULL,
    role_key character varying(100) NOT NULL,
    role_sort integer NOT NULL,
    data_scope character(1) DEFAULT '1'::bpchar,
    menu_check_strictly boolean DEFAULT false,
    dept_check_strictly boolean DEFAULT false,
    status character(1) NOT NULL,
    del_flag character(1) DEFAULT '0'::bpchar,
    create_by character varying(64) DEFAULT ''::character varying,
    create_time timestamp(6) without time zone,
    update_by character varying(64) DEFAULT ''::character varying,
    update_time timestamp(6) without time zone,
    remark character varying(500)
);


--
-- Name: TABLE sys_role; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.sys_role IS '角色信息表';


--
-- Name: COLUMN sys_role.role_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role.role_id IS '角色ID';


--
-- Name: COLUMN sys_role.role_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role.role_name IS '角色名称';


--
-- Name: COLUMN sys_role.role_key; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role.role_key IS '角色权限字符串';


--
-- Name: COLUMN sys_role.role_sort; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role.role_sort IS '显示顺序';


--
-- Name: COLUMN sys_role.data_scope; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role.data_scope IS '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）';


--
-- Name: COLUMN sys_role.menu_check_strictly; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role.menu_check_strictly IS '菜单树选择项是否关联显示';


--
-- Name: COLUMN sys_role.dept_check_strictly; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role.dept_check_strictly IS '部门树选择项是否关联显示';


--
-- Name: COLUMN sys_role.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role.status IS '角色状态（0正常 1停用）';


--
-- Name: COLUMN sys_role.del_flag; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role.del_flag IS '删除标志（0代表存在 2代表删除）';


--
-- Name: COLUMN sys_role.create_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role.create_by IS '创建者';


--
-- Name: COLUMN sys_role.create_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role.create_time IS '创建时间';


--
-- Name: COLUMN sys_role.update_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role.update_by IS '更新者';


--
-- Name: COLUMN sys_role.update_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role.update_time IS '更新时间';


--
-- Name: COLUMN sys_role.remark; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role.remark IS '备注';


--
-- Name: sys_role_dept; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_role_dept (
    role_id bigint NOT NULL,
    dept_id bigint NOT NULL
);


--
-- Name: TABLE sys_role_dept; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.sys_role_dept IS '角色和部门关联表';


--
-- Name: COLUMN sys_role_dept.role_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role_dept.role_id IS '角色ID';


--
-- Name: COLUMN sys_role_dept.dept_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role_dept.dept_id IS '部门ID';


--
-- Name: sys_role_menu; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_role_menu (
    role_id bigint NOT NULL,
    menu_id bigint NOT NULL
);


--
-- Name: TABLE sys_role_menu; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.sys_role_menu IS '角色和菜单关联表';


--
-- Name: COLUMN sys_role_menu.role_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role_menu.role_id IS '角色ID';


--
-- Name: COLUMN sys_role_menu.menu_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role_menu.menu_id IS '菜单ID';


--
-- Name: sys_user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_user (
    user_id bigint NOT NULL,
    dept_id bigint NOT NULL,
    user_name character varying(30) NOT NULL,
    nick_name character varying(30) NOT NULL,
    user_type character varying(2) DEFAULT '00'::character varying,
    email character varying(50) DEFAULT ''::character varying,
    phonenumber character varying(11) DEFAULT ''::character varying,
    sex character(1) DEFAULT '2'::bpchar,
    avatar character varying(1000) DEFAULT ''::character varying,
    password character varying(100) DEFAULT ''::character varying,
    status character(1) DEFAULT '0'::bpchar,
    del_flag character(1) DEFAULT '0'::bpchar,
    login_ip character varying(128) DEFAULT ''::character varying,
    login_date timestamp(6) without time zone,
    create_by character varying(64) DEFAULT ''::character varying,
    create_time timestamp(6) without time zone,
    update_by character varying(64) DEFAULT ''::character varying,
    update_time timestamp(6) without time zone,
    remark character varying(500),
    openid character varying(50) DEFAULT ''::character varying,
    init_nick_name smallint DEFAULT 0,
    init_avatar smallint DEFAULT 0
);


--
-- Name: TABLE sys_user; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.sys_user IS '用户信息表';


--
-- Name: COLUMN sys_user.user_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.user_id IS '用户ID';


--
-- Name: COLUMN sys_user.dept_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.dept_id IS '部门ID';


--
-- Name: COLUMN sys_user.user_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.user_name IS '用户账号';


--
-- Name: COLUMN sys_user.nick_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.nick_name IS '用户昵称';


--
-- Name: COLUMN sys_user.user_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.user_type IS '用户类型（00系统用户，11微信小程序用户）';


--
-- Name: COLUMN sys_user.email; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.email IS '用户邮箱';


--
-- Name: COLUMN sys_user.phonenumber; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.phonenumber IS '手机号码';


--
-- Name: COLUMN sys_user.sex; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.sex IS '用户性别（0男 1女 2未知）';


--
-- Name: COLUMN sys_user.avatar; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.avatar IS '头像地址';


--
-- Name: COLUMN sys_user.password; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.password IS '密码';


--
-- Name: COLUMN sys_user.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.status IS '帐号状态（0正常 1停用）';


--
-- Name: COLUMN sys_user.del_flag; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.del_flag IS '删除标志（0代表存在 2代表删除）';


--
-- Name: COLUMN sys_user.login_ip; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.login_ip IS '最后登录IP';


--
-- Name: COLUMN sys_user.login_date; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.login_date IS '最后登录时间';


--
-- Name: COLUMN sys_user.create_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.create_by IS '创建者';


--
-- Name: COLUMN sys_user.create_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.create_time IS '创建时间';


--
-- Name: COLUMN sys_user.update_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.update_by IS '更新者';


--
-- Name: COLUMN sys_user.update_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.update_time IS '更新时间';


--
-- Name: COLUMN sys_user.remark; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.remark IS '备注';


--
-- Name: COLUMN sys_user.openid; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.openid IS '微信openid';


--
-- Name: COLUMN sys_user.init_nick_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.init_nick_name IS '是否更新过微信昵称 0 否 1 是';


--
-- Name: COLUMN sys_user.init_avatar; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.init_avatar IS '是否更新过微信头像 0 否 1 是';


--
-- Name: sys_user_post; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_user_post (
    user_id bigint NOT NULL,
    post_id bigint NOT NULL
);


--
-- Name: TABLE sys_user_post; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.sys_user_post IS '用户与岗位关联表';


--
-- Name: COLUMN sys_user_post.user_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user_post.user_id IS '用户ID';


--
-- Name: COLUMN sys_user_post.post_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user_post.post_id IS '岗位ID';


--
-- Name: sys_user_role; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_user_role (
    user_id bigint NOT NULL,
    role_id bigint NOT NULL
);


--
-- Name: TABLE sys_user_role; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.sys_user_role IS '用户和角色关联表';


--
-- Name: COLUMN sys_user_role.user_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user_role.user_id IS '用户ID';


--
-- Name: COLUMN sys_user_role.role_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user_role.role_id IS '角色ID';


--
-- Data for Name: cms_docs; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.cms_docs (docs_id, folder_id, content_id, docs_name, order_num, docs_type, del_flag, create_by, create_time, update_by, update_time, content, content_markdown) VALUES (232, 219, NULL, 'txt文件', 0, 'txt', '0', 'admin', '2023-06-27 22:56:25', 'admin', '2023-07-14 11:30:13', '1、朱自清《荷塘月色》片段

    路上只我一个人，背着手踱着。这一片天地好像是我的;我也像超出了平常旳自己，到了另一世界里。我爱热闹，也爱冷静;爱群居，也爱独处。像今晚上，一个人在这苍茫旳月下，什么都可以想，什么都可以不想，便觉是个自由的人。白天里一定要做的事，一定要说的话，现在都可不理。这是独处的妙处，我且受用这无边的荷香月色好了。

    曲曲折折的荷塘上面，弥望旳是田田的叶子。叶子出水很高，像亭亭旳舞女旳裙。层层的叶子中间，零星地点缀着些白花，有袅娜(niǎo,nuó)地开着旳，有羞涩地打着朵儿旳;正如一粒粒的明珠，又如碧天里的星星，又如刚出浴的美人。微风过处，送来缕缕清香，仿佛远处高楼上渺茫的歌声似的。这时候叶子与花也有一丝的颤动，像闪电般，霎时传过荷塘的那边去了。叶子本是肩并肩密密地挨着，这便宛然有了一道凝碧的波痕。叶子底下是脉脉(mò)的流水，遮住了，不能见一些颜色;而叶子却更见风致了。

    月光如流水一般，静静地泻在这一片叶子和花上。薄薄的青雾浮起在荷塘里。叶子和花仿佛在牛乳中洗过一样;又像笼着轻纱的梦。虽然是满月，天上却有一层淡淡的云，所以不能朗照;但我以为这恰是到了好处——酣眠固不可少，小睡也别有风味的。月光是隔了树照过来的，高处丛生的灌木，落下参差的斑驳的黑影，峭楞楞如鬼一般;弯弯的杨柳的稀疏的倩影，却又像是画在荷叶上。塘中的月色并不均匀;但光与影有着和谐的旋律，如梵婀(ē)玲(英语violin小提琴的译音)上奏着的名曲。

    荷塘的四面，远远近近，高高低低都是树，而杨柳最多。这些树将一片荷塘重重围住;只在小路一旁，漏着几段空隙，像是特为月光留下的。树色一例是阴阴的，乍看像一团烟雾;但杨柳的丰姿，便在烟雾里也辨得出。树梢上隐隐约约的是一带远山，只有些大意罢了。树缝里也漏着一两点路灯光，没精打采的，是渴睡人的眼。这时候最热闹的，要数树上的蝉声与水里的蛙声;但热闹是它们的，我什么也没有。

2、鲁迅《从百草园到三味书屋》片段

    不必说碧绿的菜畦，光滑的石井栏，高大的皂荚树，紫红的桑椹;也不必说鸣蝉在树叶里长吟，肥胖的黄蜂伏在菜花上，轻捷的叫天子(云雀)忽然从草间直窜向云霄里去了。单是周围的短短的泥墙根一带，就有无限趣味。油蛉在这里低唱，蟋蟀们在这里弹琴。翻开断砖来，有时会遇见蜈蚣;还有斑蝥，倘若用手指按住它的脊梁，便会拍的一声，从后窍喷出一阵烟雾。何首乌藤和木莲藤缠络着，木莲有莲房一般的果实，何首乌有拥肿的根。有人说，何首乌根是有象人形的，吃了便可以成仙，我于是常常拔它起来，牵连不断地拔起来，也曾因此弄坏了泥墙，却从来没有见过有一块根象人样。如果不怕刺，还可以摘到覆盆子，象小珊瑚珠攒成的小球，又酸又甜，色味都比桑椹要好得远。

3、陈从周《说园》片段

    园有静观、动观之分，这一点我们在造园之先，首要考虑。何谓静观，就是园中予游者多驻足的观赏点;动观就是要有较长的游览线。二者说来，小园应以静观为主，动观为辅，庭院专主静观。大园则以动观为主，静观为辅。前者如苏州网师园，后者则苏州拙政园差可似之。人们进入网师园宜坐宜留之建筑多，绕池一周，有槛前细数游鱼，有亭中待月迎风，而轩外花影移墙，峰峦当窗，宛然如画，静中生趣。至于拙政园径缘池转，廊引人随，与“日午画船桥下过，衣香人影太匆匆”的瘦西湖相仿佛，妙在移步换影，这是动观。立意在先，文循意出。动静之分，有关园林性质与园林面积大小。象上海正在建造的盆景园，则宜以静观为主，即为一例。

    中国园林是由建筑、山水、花木等组合而成的一个综合艺术品，富有诗情画意。叠山理水要造成“虽由人作，宛自天开”的境界。山与水的关系究竟如何呢?简言之，模山范水，用局部之景而非缩小(网师园水池仿虎丘白莲池，极妙)，处理原则悉符画本。山贵有脉，水贵有源，脉源贯通，全园生动。我曾经用“水随山转，山因水活”与“溪水因山成曲折，山蹊随地作低平”来说明山水之间的关系，也就是从真山真水中所得到的启示。明末清初叠山家张南垣主张用平冈小陂、陵阜陂阪，也就是要使园林山水接近自然。如果我们能初步理解这个道理，就不至于离自然太远，多少能呈现水石交融的美妙境界。

4、梁实秋《雅舍》片段

    “雅舍”最宜月夜——地势较高，得月较先。看山头吐月，红盘乍涌，一霎间，清光四射，天空皎洁，四野无声，微闻犬吠，坐客无不悄然!舍前有两株梨树，等到月升中天，清光从树间筛洒而下，地下阴影斑斓，此时尤为幽绝。直到兴阑人散，归房就寝，月光仍然逼进窗来，助我凄凉。细雨蒙蒙之际，“雅舍”亦复有趣。推窗展望，俨然米氏章法，若云若雾，一片弥漫。但若大雨滂沱，我就又惶悚不安了，屋顶浓印到处都有，起初如碗大，俄而扩大如盆，继则滴水乃不绝，终乃屋顶灰泥突然崩裂，如奇葩初绽，砉然一声而泥水下注，此刻满室狼藉，抢救无及。此种经验，已数见不鲜。

5、冰心《图画》

    信步走下山门去，何曾想寻幽访胜?

    转过山坳来，一片青草地，参天的树影无际。树后弯弯的石桥，桥后两个俯蹲在残照里的狮子。回过头来，只一道的断瓦颓垣，剥落的红门，却深深掩闭。原来是故家陵阙!何用来感慨兴亡，且印下一幅图画。

    半山里，凭高下视，千百的燕子，绕着殿儿飞。城垛般的围墙，白石的甬道，黄绿琉璃瓦的门楼，玲珑剔透。楼前是山上的晚霞鲜红，楼后是天边的平原村树，深蓝浓紫。暮霭里，融合在一起。难道是玉宇琼楼?难道是瑶宫贝阙?何用来搜索诗肠，且印下一幅图画。

    低头走着，—首诗的断句，忽然浮上脑海来。“四月江南无矮树，人家都在绿阴中。”何用苦忆是谁的著作，何用苦忆这诗的全文。只此已描画尽了山下的人家!

6、徐志摩《我所知道的康桥》片段

    康桥的灵性全在一条河上;康河，我敢说是全世界最秀丽的一条水。河的名字是葛兰大(Granta)，也有叫康河(Kiver Cam)的，许有上下流的区别，我不甚清楚。河身多的是曲折，上游是有名的拜伦潭——“Byron’s Pool”——当年拜伦常在那里玩的;有一个老村子叫格兰骞斯德，有一个果子园，你可以躺在累累的桃李树荫下吃茶，花果会掉入你的茶杯，小雀子会到你桌上来啄食，那真是别有一番天地。这是上游;下游是从骞斯德顿下去，河面展开，那是春夏间竞舟的场所。上下河分界处有一个坝筑，水流急得很，在星光下听水声，听近村晚钟声，听河畔倦牛刍草声，是我康桥经验中最神秘的一种：大自然的优美、宁静，调谐在这星光与波光的默契中不期然的淹入了你的性灵。

7、周作人《乌篷船》片段

    倘若出城，走三四十里路(我们那里的里程是很短，一里才及英里三分之一)，来回总要预备一天。你坐在船上，应该是游山的态度，看看四周物色，随处可见的山，岸旁的乌柏，河边的红寥和白殇，渔舍，各式各样的桥，困倦的时候睡在舱中拿出随笔来看，或者冲一碗清茶喝喝。偏门外的鉴湖一带，贺家池，壶筋左近，我都是喜欢的，或者往娄公埠骑驴去游兰亭(但我劝你还是步行，骑驴或者于你不很相宜)，到得暮色苍然的时候进城上都挂着薛荔的东门来，倒是颇有趣味的事。倘若路上不平静，你往杭州去时可于下午开船，黄昏时候的景色正最好看，只可惜这一带地方的名字我都忘记了。夜间睡在舱中，听水声橹声，来往船只的招呼声，以及乡间的犬吠鸡鸣，也都很有意思。雇一只船到乡下去看庙戏，可以了解中国旧戏的真趣味，而且在船上行动自如，要看就看，要睡就睡，要喝酒就喝酒，我觉得也可以算是理想的行乐法。只可惜讲维新以来这些演剧与迎会都已禁止，中产阶级的低能人别在“布业会馆”等处建起“海式”的戏场来，请大家买票看上海的猫儿戏。这些地方你千万不要去。--你到我那故乡，恐怕没有一个人认得，我又因为在教书不能陪你去玩，坐夜船，谈闲天，实在抱歉而且惆怅。川岛君夫妇现在偁山下，本来可以给你绍介，但是你到那里的时候他们恐怕已经离开故乡了。初寒，善自珍重，不尽。', NULL);
INSERT INTO public.cms_docs (docs_id, folder_id, content_id, docs_name, order_num, docs_type, del_flag, create_by, create_time, update_by, update_time, content, content_markdown) VALUES (233, 219, NULL, 'world文件', 0, 'world', '0', 'admin', '2023-06-27 22:56:42', 'admin', '2023-07-14 11:42:46', '<p style="text-align: center;"><strong>硅步至千里，溪流聚江河</strong></p>
<p style="text-align: center;">王宁</p>
<p>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;聚沙成塔，集腋成袭；涓涓细流，汇城大河，学会小处着眼，才能登高望远。</p>
<p>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;小处着眼，能在细微处磨砺心智。山高路远,只有经验未必能坚定地保持前行，而强大的心智会给我们不竭的驱动力。日复一日的柴草和苦胆是勾践卧薪尝胆的坚韧不拔，一字一句推敲斟酌是李雪芹增删红楼的精益求精，一尺一丈砸磨敲炸是毛相林&ldquo;愚公移山&rdquo;的持之以恒，他们如怀揣着崇高的信念的朝圣者，越过荆棘遍布的路途为自己的理想献上饱满艳丽的果实。或许这些信念心智在起点只是一粒小小沙石，在不断磨砺中，它照亮了我们的前路，我们拂去了它的蒙尘，相辅相成，相携共赴。</p>
<p>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;不积跬步无以至千里，不积小流无以成江海，没有一处高山不能被攀登，没有一处湍流不能被渡过，小处着眼，脚踏实地，彼岸从来都不遥远。</p>', NULL);
INSERT INTO public.cms_docs (docs_id, folder_id, content_id, docs_name, order_num, docs_type, del_flag, create_by, create_time, update_by, update_time, content, content_markdown) VALUES (234, 219, NULL, 'markdown文件', 0, 'markdown', '0', 'admin', '2023-06-27 22:57:00', 'admin', '2023-07-13 22:37:18', '<div data-inline-code-theme="red" data-code-block-theme="tomorrow-night"><h1 data-lines="1" data-sign="31607bd7da4aa9e0b38fb690412ed821" id="%E7%94%A8-markdown-%E5%88%B6%E4%BD%9C%E5%B9%BB%E7%81%AF%E7%89%87" class=""><a href="#%E7%94%A8-markdown-%E5%88%B6%E4%BD%9C%E5%B9%BB%E7%81%AF%E7%89%87" class="anchor"></a>用 Markdown 制作幻灯片</h1><h3 data-lines="1" data-sign="a840f557727342275fe0e8c893e1b92d" id="%E5%9F%BA%E6%9C%AC%E8%AF%AD%E6%B3%95"><a href="#%E5%9F%BA%E6%9C%AC%E8%AF%AD%E6%B3%95" class="anchor"></a>基本语法</h3><p data-lines="1" data-type="p" data-sign="53e1d69c580440229c59a0b09cf10eb51">  首先，我们在 markdown 语法的基础上做了一个扩展：通过 <code>----</code> 分割线把一整篇 markdown 文档划分成为若干张幻灯片。</p><h4 data-lines="1" data-sign="dc159b71e4f945c33c349524c6773302" id="%E8%87%AA%E5%AE%9A%E4%B9%89%E6%A0%B7%E5%BC%8F"><a href="#%E8%87%AA%E5%AE%9A%E4%B9%89%E6%A0%B7%E5%BC%8F" class="anchor"></a>自定义样式</h4><p data-lines="2" data-type="p" data-sign="62b49d2703d24f9dc5e31f803ed8d13a2">  我们对 markdown 格式做了一点点进一步的扩展 —— 通过在每一页幻灯片开头撰写 html 注释来设置这页幻灯片的特殊样式。<br>  比如我们为第二页幻灯片换一个不一样的背景，同时正文文字颜色变成白色：</p><div data-sign="1b9d834507176f215647db1afd4febe7" data-type="codeBlock" data-lines="21">
      <div class="cherry-copy-code-block" style="display:none;"><i class="ch-icon ch-icon-copy" title="copy"></i></div>
      <pre class="language-javascript"><code class="language-javascript wrap"><span class="code-line"># 这里是第一页</span>
<span class="code-line"></span>
<span class="code-line">以及一些基本的自我介绍</span>
<span class="code-line"></span>
<span class="code-line"><span class="token operator">--</span><span class="token operator">--</span></span>
<span class="code-line"></span>
<span class="code-line"><span class="token operator">&lt;</span><span class="token operator">!</span><span class="token operator">--</span> style<span class="token operator">:</span> background<span class="token operator">:</span> #4fc08d<span class="token punctuation">;</span> color<span class="token operator">:</span> white<span class="token punctuation">;</span> <span class="token operator">--</span><span class="token operator">&gt;</span></span>
<span class="code-line"></span>
<span class="code-line">### 这里是第二页</span>
<span class="code-line"></span>
<span class="code-line"><span class="token operator">-</span> 这里有内容</span>
<span class="code-line"><span class="token operator">-</span> 这里有内容</span>
<span class="code-line"><span class="token operator">-</span> 这里有内容</span>
<span class="code-line"></span>
<span class="code-line"><span class="token operator">--</span><span class="token operator">--</span></span>
<span class="code-line"></span>
<span class="code-line">没了，讲完了</span>
<span class="code-line"></span>
<span class="code-line">__谢谢__</span></code></pre>
    </div><p data-lines="1" data-type="p" data-sign="06054e022f9f0d771422b612b63da7c31">  当然在 markdown 中你也可以撰写任意 HTML5 代码，比如嵌入一段 HTML 甚至全局有效的 <code>&lt;style&gt;</code> 标签，都可以被解析：</p><div data-sign="2fdaee99b91e22b3c8414632a7d758f6" data-type="codeBlock" data-lines="43">
      <div class="cherry-copy-code-block" style="display:none;"><i class="ch-icon ch-icon-copy" title="copy"></i></div>
      <pre class="language-javascript"><code class="language-javascript wrap"><span class="code-line"># 这里是第一页</span>
<span class="code-line"></span>
<span class="code-line">以及一些基本的自我介绍</span>
<span class="code-line"></span>
<span class="code-line"><span class="token operator">&lt;</span>div <span class="token keyword">class</span><span class="token operator">=</span><span class="token string">"notification"</span><span class="token operator">&gt;</span>Welcome<span class="token operator">!</span><span class="token operator">&lt;</span><span class="token operator">/</span>div<span class="token operator">&gt;</span></span>
<span class="code-line"></span>
<span class="code-line"><span class="token operator">--</span><span class="token operator">--</span></span>
<span class="code-line"></span>
<span class="code-line"><span class="token operator">&lt;</span><span class="token operator">!</span><span class="token operator">--</span> style<span class="token operator">:</span> background<span class="token operator">:</span> #4fc08d<span class="token punctuation">;</span> color<span class="token operator">:</span> white<span class="token punctuation">;</span> <span class="token operator">--</span><span class="token operator">&gt;</span></span>
<span class="code-line"></span>
<span class="code-line">## 这里是第二页</span>
<span class="code-line"></span>
<span class="code-line"><span class="token operator">-</span> 我会自动调节字号</span>
<span class="code-line"><span class="token operator">-</span> 我支持键盘和手势翻页</span>
<span class="code-line"><span class="token operator">-</span> 我支持 <span class="token constant">URL</span> hash 同步</span>
<span class="code-line"></span>
<span class="code-line"><span class="token operator">&lt;</span>small<span class="token operator">&gt;</span>现在页面网址的结尾应该是 \`<span class="token operator">...</span><span class="token punctuation">.</span>#<span class="token number">2</span>\`<span class="token operator">&lt;</span><span class="token operator">/</span>small<span class="token operator">&gt;</span></span>
<span class="code-line"></span>
<span class="code-line"><span class="token operator">--</span><span class="token operator">--</span></span>
<span class="code-line"></span>
<span class="code-line"><span class="token punctuation">[</span>空白链接<span class="token punctuation">]</span><span class="token punctuation">(</span>about<span class="token operator">:</span>blank<span class="token punctuation">)</span></span>
<span class="code-line"></span>
<span class="code-line">试一试按下 <span class="token operator">&lt;</span>kbd<span class="token operator">&gt;</span>Alt<span class="token operator">&lt;</span><span class="token operator">/</span>kbd<span class="token operator">&gt;</span> 的同时点击这个链接</span>
<span class="code-line"></span>
<span class="code-line"><span class="token operator">--</span><span class="token operator">--</span></span>
<span class="code-line"></span>
<span class="code-line">没了，讲完了</span>
<span class="code-line"></span>
<span class="code-line"><span class="token operator">&lt;</span>div <span class="token keyword">class</span><span class="token operator">=</span><span class="token string">"notification"</span><span class="token operator">&gt;</span>Thanks<span class="token operator">!</span><span class="token operator">&lt;</span><span class="token operator">/</span>div<span class="token operator">&gt;</span></span>
<span class="code-line"></span>
<span class="code-line"><span class="token operator">&lt;</span>style<span class="token operator">&gt;</span></span>
<span class="code-line"><span class="token punctuation">.</span>notification <span class="token punctuation">{</span></span>
<span class="code-line">  <span class="token literal-property property">position</span><span class="token operator">:</span> absolute<span class="token punctuation">;</span></span>
<span class="code-line">  <span class="token literal-property property">top</span><span class="token operator">:</span> 20px<span class="token punctuation">;</span></span>
<span class="code-line">  <span class="token literal-property property">right</span><span class="token operator">:</span> 20px<span class="token punctuation">;</span></span>
<span class="code-line">  border<span class="token operator">-</span>radius<span class="token operator">:</span> 3px<span class="token punctuation">;</span></span>
<span class="code-line">  <span class="token literal-property property">padding</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">.</span>25em 1em<span class="token punctuation">;</span></span>
<span class="code-line">  background<span class="token operator">-</span>color<span class="token operator">:</span> yellow<span class="token punctuation">;</span></span>
<span class="code-line">  <span class="token literal-property property">color</span><span class="token operator">:</span> #<span class="token number">666</span><span class="token punctuation">;</span></span>
<span class="code-line"><span class="token punctuation">}</span></span>
<span class="code-line"><span class="token operator">&lt;</span><span class="token operator">/</span>style<span class="token operator">&gt;</span></span></code></pre>
    </div><h3 data-lines="1" data-sign="e6f76688e6eea0994e79ced08f8013d0" id="one-more-thing"><a href="#one-more-thing" class="anchor"></a>One More Thing</h3><p data-lines="1" data-type="p" data-sign="36641485ab3fdf9c2a5d47f1517dadcd1">  支持直接导出成为 PDF 格式文件。你只需要简单的打开浏览器的打印对话框，然后选择导出成为 PDF，就可以轻松的把自己的幻灯片导出成为 PDF 格式文件。</p><h3 data-lines="1" data-sign="cf2f6473e8998887bf761b593a19d171" id="markdown%E8%AF%AD%E6%B3%95%E7%9A%84%E6%89%A9%E5%B1%95"><a href="#markdown%E8%AF%AD%E6%B3%95%E7%9A%84%E6%89%A9%E5%B1%95" class="anchor"></a>Markdown语法的扩展</h3><ol class="cherry-list__default cherry-highlight-line" start="1" data-sign="e8482e5f432c5ff3527cee8a29c0c595" data-lines="10"><li><p>它基于 <a target="_blank" rel="nofollow" href="https://markdown.com.cn/">标记</a> 。</p></li><li><p>您可以按水平线(<code>----</code>)分隔页面。</p></li><li><p>您可以将HTML注释用于每张幻灯片的元信息。 格式类似于<code>&lt;!-- key: value --&gt;</code>。 以下是所有有用的元键：</p><ul class="cherry-list__default"><li><p><code>background</code>：幻灯片的背景样式</p></li><li><p><code>backgroundColor</code>：幻灯片的背景色</p></li><li><p><code>backgroundImage</code>：幻灯片背景图片的网址</p></li><li><p><code>color</code>：幻灯片的默认字体颜色</p></li><li><p><code>style</code>：附在幻灯片上的嵌入式CSS文字</p></li><li><p><code>stageBackground</code>：显示当前幻灯片时附加到舞台的背景样式</p></li></ul></li></ol><p data-lines="2" data-type="p" data-sign="173a41f426d3ba6ad6f1518b82d94fd12">例：</p><div data-sign="ef74291c9749c6e2379358499625b54f" data-type="codeBlock" data-lines="12">
      <div class="cherry-copy-code-block" style="display:none;"><i class="ch-icon ch-icon-copy" title="copy"></i></div>
      <pre class="language-javascript"><code class="language-javascript wrap"><span class="code-line"><span class="token operator">&lt;</span><span class="token operator">!</span><span class="token operator">--</span> color<span class="token operator">:</span> red<span class="token punctuation">;</span> <span class="token operator">--</span><span class="token operator">&gt;</span></span>
<span class="code-line"><span class="token operator">&lt;</span><span class="token operator">!</span><span class="token operator">--</span> style<span class="token operator">:</span> font<span class="token operator">-</span>weight<span class="token operator">:</span> bold<span class="token punctuation">;</span> <span class="token operator">--</span><span class="token operator">&gt;</span></span>
<span class="code-line"> </span>
<span class="code-line"># Hello</span>
<span class="code-line"> </span>
<span class="code-line"><span class="token operator">--</span><span class="token operator">-</span></span>
<span class="code-line"> </span>
<span class="code-line"><span class="token operator">&lt;</span><span class="token operator">!</span><span class="token operator">--</span> stageBackground<span class="token operator">:</span> silver <span class="token operator">--</span><span class="token operator">&gt;</span></span>
<span class="code-line"> </span>
<span class="code-line"><span class="token operator">!</span><span class="token punctuation">[</span><span class="token punctuation">.</span><span class="token operator">/</span>faviconStore<span class="token punctuation">.</span>ico<span class="token punctuation">]</span><span class="token punctuation">(</span><span class="token punctuation">.</span><span class="token operator">/</span>faviconStore<span class="token punctuation">.</span>ico<span class="token punctuation">)</span> <span class="token keyword">this</span> is content</span></code></pre>
    </div></div>', '# 用 Markdown 制作幻灯片
### 基本语法
&emsp;&emsp;首先，我们在 markdown 语法的基础上做了一个扩展：通过 `----` 分割线把一整篇 markdown 文档划分成为若干张幻灯片。
#### 自定义样式
&emsp;&emsp;我们对 markdown 格式做了一点点进一步的扩展 —— 通过在每一页幻灯片开头撰写 html 注释来设置这页幻灯片的特殊样式。
&emsp;&emsp;比如我们为第二页幻灯片换一个不一样的背景，同时正文文字颜色变成白色：
```
# 这里是第一页

以及一些基本的自我介绍

----

<!-- style: background: #4fc08d; color: white; -->

### 这里是第二页

- 这里有内容
- 这里有内容
- 这里有内容

----

没了，讲完了

__谢谢__
```
&emsp;&emsp;当然在 markdown 中你也可以撰写任意 HTML5 代码，比如嵌入一段 HTML 甚至全局有效的 `<style>` 标签，都可以被解析：
```
# 这里是第一页

以及一些基本的自我介绍

<div class="notification">Welcome!</div>

----

<!-- style: background: #4fc08d; color: white; -->

## 这里是第二页

- 我会自动调节字号
- 我支持键盘和手势翻页
- 我支持 URL hash 同步

<small>现在页面网址的结尾应该是 \`....#2\`</small>

----

[空白链接](about:blank)

试一试按下 <kbd>Alt</kbd> 的同时点击这个链接

----

没了，讲完了

<div class="notification">Thanks!</div>

<style>
.notification {
  position: absolute;
  top: 20px;
  right: 20px;
  border-radius: 3px;
  padding: 0.25em 1em;
  background-color: yellow;
  color: #666;
}
</style>
```
### One More Thing
&emsp;&emsp;支持直接导出成为 PDF 格式文件。你只需要简单的打开浏览器的打印对话框，然后选择导出成为 PDF，就可以轻松的把自己的幻灯片导出成为 PDF 格式文件。
### Markdown语法的扩展

1. 它基于 [标记](https://markdown.com.cn/){target=_blank} 。
2. 您可以按水平线(`----`)分隔页面。
3. 您可以将HTML注释用于每张幻灯片的元信息。 格式类似于`<!-- key: value -->`。 以下是所有有用的元键：
	- `background`：幻灯片的背景样式
	- `backgroundColor`：幻灯片的背景色
	- `backgroundImage`：幻灯片背景图片的网址
	- `color`：幻灯片的默认字体颜色
	- `style`：附在幻灯片上的嵌入式CSS文字
	- `stageBackground`：显示当前幻灯片时附加到舞台的背景样式

例：
```
<!-- color: red; -->
<!-- style: font-weight: bold; -->
 
# Hello
 
---
 
<!-- stageBackground: silver -->
 
![./faviconStore.ico](./faviconStore.ico) this is content
```');
INSERT INTO public.cms_docs (docs_id, folder_id, content_id, docs_name, order_num, docs_type, del_flag, create_by, create_time, update_by, update_time, content, content_markdown) VALUES (235, 219, NULL, 'excel文件', 0, 'excel', '0', 'admin', '2023-06-27 22:57:17', 'admin', '2023-07-14 21:00:10', '[{"name":"sheet1","color":"","index":1,"status":1,"order":1,"celldata":[],"config":{"merge":{},"borderInfo":[{"rangeType":"cell","value":{"row_index":0,"col_index":0,"r":{"style":1,"color":"rgb(204, 204, 204)"},"t":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":0,"col_index":1,"r":{"style":1,"color":"rgb(204, 204, 204)"},"t":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":0,"col_index":2,"r":{"style":1,"color":"rgb(204, 204, 204)"},"t":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":0,"col_index":3,"r":{"style":1,"color":"rgb(204, 204, 204)"},"t":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":0,"col_index":4,"r":{"style":1,"color":"rgb(204, 204, 204)"},"t":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":0,"col_index":5,"r":{"style":1,"color":"rgb(204, 204, 204)"},"t":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":0,"col_index":6,"r":{"style":1,"color":"rgb(204, 204, 204)"},"t":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":0,"col_index":7,"r":{"style":1,"color":"rgb(204, 204, 204)"},"t":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":1,"col_index":0,"r":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":1,"col_index":1,"r":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":1,"col_index":2,"r":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":1,"col_index":3,"r":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":1,"col_index":4,"r":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":1,"col_index":5,"r":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":1,"col_index":6,"r":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":1,"col_index":7,"r":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":2,"col_index":0,"r":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":2,"col_index":1,"r":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":2,"col_index":2,"r":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":2,"col_index":3,"r":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":2,"col_index":4,"r":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":2,"col_index":5,"r":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":2,"col_index":6,"r":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":2,"col_index":7,"r":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":3,"col_index":0,"r":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":3,"col_index":1,"r":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":3,"col_index":2,"r":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":3,"col_index":3,"r":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":3,"col_index":4,"r":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":3,"col_index":5,"r":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":3,"col_index":6,"r":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":3,"col_index":7,"r":{"style":1,"color":"rgb(204, 204, 204)"},"b":{"style":1,"color":"rgb(204, 204, 204)"}}},{"rangeType":"cell","value":{"row_index":1,"col_index":4,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":2,"col_index":0,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":2,"col_index":1,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":2,"col_index":2,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":2,"col_index":3,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":2,"col_index":4,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":2,"col_index":5,"l":{"color":"rgb(204, 204, 204)","style":1},"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":2,"col_index":6,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":2,"col_index":7,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":3,"col_index":0,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":3,"col_index":1,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":3,"col_index":2,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":3,"col_index":3,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":3,"col_index":4,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":3,"col_index":5,"l":{"color":"rgb(204, 204, 204)","style":1},"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":3,"col_index":6,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":3,"col_index":7,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":4,"col_index":0,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":4,"col_index":1,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":4,"col_index":2,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":4,"col_index":3,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":4,"col_index":4,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":4,"col_index":5,"l":{"color":"rgb(204, 204, 204)","style":1},"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":4,"col_index":6,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":4,"col_index":7,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":2,"col_index":4,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":3,"col_index":4,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":4,"col_index":4,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}}],"rowlen":{},"columnlen":{"1":134,"2":86,"4":86},"colhidden":{},"rowhidden":{}},"jfgird_select_save":[],"luckysheet_select_save":[{"left":74,"width":134,"top":100,"height":19,"left_move":74,"width_move":134,"top_move":100,"height_move":19,"row":[5,5],"column":[1,1],"row_focus":5,"column_focus":1}],"data":[[{"v":"姓名","ct":{"fa":"General","t":"g"},"m":"姓名","bg":null,"bl":1,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0},{"v":"邮箱","ct":{"fa":"General","t":"g"},"m":"邮箱","bg":null,"bl":1,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0},{"v":"电话","ct":{"fa":"General","t":"g"},"m":"电话","bg":null,"bl":1,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0},{"v":"传真","ct":{"fa":"General","t":"g"},"m":"传真","bg":null,"bl":1,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0},{"v":"手机","ct":{"fa":"General","t":"g"},"m":"手机","bg":null,"bl":1,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0},{"v":"权重","ct":{"fa":"General","t":"g"},"m":"权重","bg":null,"bl":1,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0},{"v":"部门","ct":{"fa":"General","t":"g"},"m":"部门","bg":null,"bl":1,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0},{"v":"兼职部门","ct":{"fa":"General","t":"g"},"m":"兼职部门","bg":null,"bl":1,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[{"ct":{"fa":"General","t":"g"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"m":"张三","v":"张三"},{"ct":{"fa":"@","t":"s"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"m":"zhansan@mail.com","v":"zhansan@mail.com"},{"ct":{"fa":"@","t":"s"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"qp":1,"m":"1888888888","v":"1888888888"},{"ct":{"fa":"@","t":"s"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"qp":1,"m":"010000","v":"010000"},{"ct":{"fa":"@","t":"s"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"qp":1,"m":"1888888888","v":"1888888888"},{"ct":{"fa":"General","t":"n"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"v":100,"m":"100"},{"ct":{"fa":"General","t":"g"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"m":"运营中心","v":"运营中心"},{"ct":{"fa":"General","t":"g"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"m":"无","v":"无"},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[{"ct":{"fa":"General","t":"g"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"m":"李四","v":"李四"},{"ct":{"fa":"@","t":"s"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"m":"lisi@mail.com","v":"lisi@mail.com"},{"ct":{"fa":"@","t":"s"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"qp":1,"m":"1777777777","v":"1777777777"},{"ct":{"fa":"@","t":"s"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"qp":1,"m":"010000","v":"010000"},{"ct":{"fa":"@","t":"s"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"qp":1,"m":"1777777777","v":"1777777777"},{"ct":{"fa":"General","t":"n"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"v":100,"m":"100"},{"ct":{"fa":"General","t":"g"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"m":"运营中心","v":"运营中心"},{"ct":{"fa":"General","t":"g"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"m":"无","v":"无"},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[{"ct":{"fa":"General","t":"g"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"m":"王五","v":"王五"},{"ct":{"fa":"@","t":"s"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"m":"wangwu@mail.com","v":"wangwu@mail.com"},{"ct":{"fa":"@","t":"s"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"qp":1,"m":"1666666666","v":"1666666666"},{"ct":{"fa":"@","t":"s"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"qp":1,"m":"010000","v":"010000"},{"ct":{"fa":"@","t":"s"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"qp":1,"m":"1666666666","v":"1666666666"},{"ct":{"fa":"General","t":"n"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"v":100,"m":"100"},{"ct":{"fa":"General","t":"g"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"m":"运营中心","v":"运营中心"},{"ct":{"fa":"General","t":"g"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"m":"无","v":"无"},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[{"ct":{"fa":"General","t":"g"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"m":"老六","v":"老六"},{"ct":{"fa":"@","t":"s"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"m":"laoliu@mail.com","v":"laoliu@mail.com"},{"ct":{"fa":"@","t":"s"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"qp":1,"m":"1555555555","v":"1555555555"},{"ct":{"fa":"@","t":"s"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"qp":1,"m":"010000","v":"010000"},{"ct":{"fa":"@","t":"s"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"qp":1,"m":"1555555555","v":"1555555555"},{"ct":{"fa":"General","t":"n"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"v":100,"m":"100"},{"ct":{"fa":"General","t":"g"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"m":"运营中心","v":"运营中心"},{"ct":{"fa":"General","t":"g"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"m":"无","v":"无"},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[{"ct":{"fa":"General","t":"g"}},{"ct":{"fa":"General","t":"n"}},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null]],"load":"1","visibledatarow":[20,40,60,80,100,120,140,160,180,200,220,240,260,280,300,320,340,360,380,400,420,440,460,480,500,520,540,560,580,600,620,640,660,680,700,720,740,760,780,800,820,840,860,880,900,920,940,960,980,1000,1020,1040,1060,1080,1100,1120,1140,1160,1180,1200,1220,1240,1260,1280,1300,1320,1340,1360,1380,1400,1420,1440,1460,1480,1500,1520,1540,1560,1580,1600],"visibledatacolumn":[74,209,296,370,457,531,605,679,753,827,901,975,1049,1123,1197,1271,1345,1419,1493,1567,1641,1715,1789,1863,1937,2011,2085,2159,2233,2307,2381,2455,2529,2603,2677,2751,2825,2899,2973,3047,3121,3195,3269,3343,3417,3491,3565,3639,3713,3787,3861,3935,4009,4083,4157],"ch_width":4277,"rh_height":1680,"luckysheet_selection_range":[],"zoomRatio":1,"scrollLeft":0,"scrollTop":0,"hide":0,"calcChain":[],"luckysheet_conditionformat_save":[],"dataVerification":{},"filter_select":null,"filter":null,"luckysheet_alternateformat_save":[],"hyperlink":{},"frozen":{"type":"cancel"},"freezen":{"horizontal":null}},{"name":"Sheet2","color":"","index":"Sheet_7pw140iWkWK0_1688947876707","status":0,"order":1,"celldata":[],"config":{"merge":{},"borderInfo":[{"rangeType":"cell","value":{"row_index":0,"col_index":0,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":{"color":"rgb(204, 204, 204)","style":1},"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":0,"col_index":1,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":{"color":"rgb(204, 204, 204)","style":1},"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":0,"col_index":2,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":{"color":"rgb(204, 204, 204)","style":1},"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":0,"col_index":3,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":{"color":"rgb(204, 204, 204)","style":1},"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":0,"col_index":4,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":{"color":"rgb(204, 204, 204)","style":1},"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":0,"col_index":5,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":{"color":"rgb(204, 204, 204)","style":1},"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":0,"col_index":6,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":{"color":"rgb(204, 204, 204)","style":1},"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":0,"col_index":7,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":{"color":"rgb(204, 204, 204)","style":1},"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":1,"col_index":0,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":1,"col_index":1,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":1,"col_index":2,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":1,"col_index":3,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":1,"col_index":4,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":1,"col_index":5,"l":{"color":"rgb(204, 204, 204)","style":1},"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":1,"col_index":6,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}},{"rangeType":"cell","value":{"row_index":1,"col_index":7,"l":null,"r":{"color":"rgb(204, 204, 204)","style":1},"t":null,"b":{"color":"rgb(204, 204, 204)","style":1}}}],"columnlen":{"1":134,"2":86,"4":86}},"data":[[{"v":"姓名","ct":{"fa":"General","t":"g"},"m":"姓名","bg":null,"bl":1,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0},{"v":"邮箱","ct":{"fa":"General","t":"g"},"m":"邮箱","bg":null,"bl":1,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0},{"v":"电话","ct":{"fa":"General","t":"g"},"m":"电话","bg":null,"bl":1,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0},{"v":"传真","ct":{"fa":"General","t":"g"},"m":"传真","bg":null,"bl":1,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0},{"v":"手机","ct":{"fa":"General","t":"g"},"m":"手机","bg":null,"bl":1,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0},{"v":"权重","ct":{"fa":"General","t":"g"},"m":"权重","bg":null,"bl":1,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0},{"v":"部门","ct":{"fa":"General","t":"g"},"m":"部门","bg":null,"bl":1,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0},{"v":"兼职部门","ct":{"fa":"General","t":"g"},"m":"兼职部门","bg":null,"bl":1,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},null,null,null,null,null],[{"ct":{"fa":"General","t":"g"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"m":"王九","v":"王九"},{"ct":{"fa":"@","t":"s"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"m":"wangjiu@mail.com","v":"wangjiu@mail.com"},{"ct":{"fa":"@","t":"s"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"qp":1,"m":"1999999999","v":"1999999999"},{"ct":{"fa":"@","t":"s"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"qp":1,"m":"010000","v":"010000"},{"ct":{"fa":"@","t":"s"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"qp":1,"m":"1999999999","v":"1999999999"},{"ct":{"fa":"General","t":"n"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"v":100,"m":"100"},{"ct":{"fa":"General","t":"g"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"m":"开发","v":"开发"},{"ct":{"fa":"General","t":"g"},"bg":null,"bl":0,"it":0,"ff":5,"fs":11,"fc":"rgb(0, 0, 0)","ht":0,"vt":0,"m":"无","v":"无"},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null]],"load":"1","hide":0,"luckysheet_select_save":[{"left":679,"width":73,"top":60,"height":19,"left_move":679,"width_move":73,"top_move":60,"height_move":19,"row":[3,3],"column":[8,8],"row_focus":3,"column_focus":8}],"visibledatarow":[20,40,60,80,100,120,140,160,180,200,220,240,260,280,300,320,340,360,380,400,420,440,460,480,500,520,540,560,580,600,620,640,660,680,700,720,740,760,780,800,820,840,860,880,900,920,940,960,980,1000,1020,1040,1060,1080,1100,1120,1140,1160,1180,1200,1220,1240,1260,1280,1300,1320,1340,1360,1380,1400,1420,1440,1460,1480,1500,1520,1540,1560,1580,1600,1620,1640,1660,1680],"visibledatacolumn":[74,209,296,370,457,531,605,679,753,827,901,975,1049,1123,1197,1271,1345,1419,1493,1567,1641,1715,1789,1863,1937,2011,2085,2159,2233,2307,2381,2455,2529,2603,2677,2751,2825,2899,2973,3047,3121,3195,3269,3343,3417,3491,3565,3639,3713,3787,3861,3935,4009,4083,4157,4231,4305,4379,4453,4527],"ch_width":4647,"rh_height":1760,"luckysheet_selection_range":[],"scrollLeft":0,"scrollTop":0,"zoomRatio":1,"jfgird_select_save":[],"images":{}}]', NULL);
INSERT INTO public.cms_docs (docs_id, folder_id, content_id, docs_name, order_num, docs_type, del_flag, create_by, create_time, update_by, update_time, content, content_markdown) VALUES (236, 219, NULL, '幻灯片', 0, 'ppt', '0', 'admin', '2023-06-27 22:57:36', 'admin', '2023-07-11 14:27:31', '# 这里是第一页

以及一些基本的自我介绍

<div class="notification">Welcome!</div>

----

<!-- style: background: #4fc08d; color: white; -->

## 这里是第二页

- 我会自动调节字号
- 我支持键盘和手势翻页
- 我支持 URL hash 同步

<small>现在页面网址的结尾应该是 \`....#2\`</small>

----

[空白链接](about:blank)

试一试按下 <kbd>Alt</kbd> 的同时点击这个链接

----

没了，讲完了

<div class="notification">Thanks!</div>

<style>
.notification {
  position: absolute;
  top: 20px;
  right: 20px;
  border-radius: 3px;
  padding: 0.25em 1em;
  background-color: yellow;
  color: #666;
}
</style>', NULL);
INSERT INTO public.cms_docs (docs_id, folder_id, content_id, docs_name, order_num, docs_type, del_flag, create_by, create_time, update_by, update_time, content, content_markdown) VALUES (237, 219, NULL, '思维导图', 0, 'mindMapping', '0', 'admin', '2023-06-28 22:05:10', 'admin', '2023-07-14 11:52:35', '{"root":{"data":{"text":"根节点","expand":true,"isActive":false,"icon":["progress_8"]},"children":[{"data":{"text":"二级节点","generalization":{"text":"概要","expand":true,"isActive":false},"expand":true,"isActive":false,"note":"\n| 姓名 | 联系方式 |\n| --- | --- |\n| 张三 | 1888888888 |"},"children":[{"data":{"text":"分支主题","expand":true,"isActive":false},"children":[]},{"data":{"text":"分支主题","expand":true,"isActive":false},"children":[]}]},{"data":{"text":"二级节点","expand":true,"isActive":false},"children":[]}]},"theme":{"template":"classic4","config":{}},"layout":"logicalStructure","config":{},"view":{"transform":{"scaleX":1,"scaleY":1,"shear":0,"rotate":0,"translateX":-428.16666666666686,"translateY":-47.16666666666676,"originX":0,"originY":0,"a":1,"b":0,"c":0,"d":1,"e":-428.16666666666686,"f":-47.16666666666676},"state":{"scale":1,"x":-428.16666666666686,"y":-47.16666666666676,"sx":-66.16666666666688,"sy":-23.166666666666757}}}', NULL);
INSERT INTO public.cms_docs (docs_id, folder_id, content_id, docs_name, order_num, docs_type, del_flag, create_by, create_time, update_by, update_time, content, content_markdown) VALUES (238, 219, NULL, '画图', 0, 'img', '0', 'admin', '2023-07-05 14:39:01', 'admin', '2023-07-14 12:18:06', '{"state":{"scale":1,"scrollX":-22,"scrollY":-97,"scrollStep":50,"backgroundColor":null,"strokeStyle":"#000000","fillStyle":"transparent","fontFamily":"微软雅黑, Microsoft YaHei","fontSize":18,"dragStrokeStyle":"#666","showGrid":false,"readonly":false,"gridConfig":{"size":20,"strokeStyle":"#dfe0e1","lineWidth":1}},"elements":[{"groupId":"","type":"diamond","width":64,"height":88,"x":677,"y":155,"rotate":0,"style":{"strokeStyle":"#000000","fillStyle":"transparent","lineWidth":2,"lineDash":0,"globalAlpha":1}},{"groupId":"","type":"triangle","width":61,"height":72,"x":890,"y":-18,"rotate":0,"style":{"strokeStyle":"#000000","fillStyle":"transparent","lineWidth":2,"lineDash":0,"globalAlpha":1}},{"groupId":"","type":"text","width":120,"height":36,"x":449,"y":9,"rotate":0,"style":{"strokeStyle":"#000000","fillStyle":"#000000","lineWidth":2,"lineDash":0,"globalAlpha":1,"fontSize":24,"lineHeightRatio":1.5,"fontFamily":"楷体, 楷体_GB2312, SimKai, STKaiti"},"text":"你好！！！"},{"groupId":"","type":"freedraw","width":60,"height":59,"x":600,"y":214.00624084472656,"rotate":0,"style":{"strokeStyle":"#000000","fillStyle":"transparent","lineWidth":2,"lineDash":0,"globalAlpha":1},"pointArr":[[660,214.00624084472656,2.5],[655.7894736842105,215.92182526031098,2.75],[652.6315789473684,218.22052655901228,2.875],[648.421052631579,220.90234474083047,2.4370904095506676],[643.1578947368421,223.96727980576554,2.2127620398709587],[636.8421052631579,227.79844863693435,2.0868026682965284],[630.5263157894736,232.39585123433696,2.012959180185424],[624.2105263157895,237.37637071485642,1.964914977834315],[617.8947368421053,242.3568901953759,1.946433829082629],[612.6315789473684,246.18805902654475,1.9522936258077688],[609.4736842105264,249.6361109745967,1.9700363919719641],[606.3157894736842,252.70104603953178,1.981439223977314],[603.1578947368421,255.76598110446685,1.9904471429774029],[602.1052631578947,258.06468240316815,2.4952235714887014],[601.0526315789474,259.5971499356357,2.7476117857443505],[600,261.1296174681032,2.8738058928721752],[600,262.66208500057076,2.9369029464360876],[600,264.1945525330383,2.968451473218044],[600,266.11013694862265,2.984225736609022],[602.1052631578947,268.02572136420713,2.992112868304511],[603.1578947368421,269.9413057797915,2.9960564341522558],[605.2631578947369,271.473773312259,2.998028217076128],[606.3157894736842,271.8568901953759,2.9990141085380637],[607.3684210526316,272.6231239616097,2.999507054269032],[607.3684210526316,273.00624084472656,2.9997535271345157]]},{"groupId":"","type":"arrow","width":111,"height":0,"x":920,"y":99.00624084472656,"rotate":223,"style":{"strokeStyle":"#000000","fillStyle":"transparent","lineWidth":2,"lineDash":0,"globalAlpha":1},"pointArr":[[1031,99.00624084472656],[920,99.00624084472656]]},{"groupId":"","type":"line","width":48,"height":86,"x":776,"y":23.006240844726562,"rotate":29,"style":{"strokeStyle":"#22DE51","fillStyle":"transparent","lineWidth":2,"lineDash":0,"globalAlpha":1},"pointArr":[[824,23.006240844726562],[776,109.00624084472656]]},{"groupId":"","type":"circle","width":70.25667228100119,"height":70.25667228100119,"x":669,"y":-7.9937591552734375,"rotate":0,"style":{"strokeStyle":"#000000","fillStyle":"transparent","lineWidth":2,"lineDash":0,"globalAlpha":1}},{"groupId":"","type":"rectangle","width":37,"height":64,"x":691,"y":81.00624084472656,"rotate":0,"style":{"strokeStyle":null,"fillStyle":"#FFFFFF","lineWidth":2,"lineDash":0,"globalAlpha":1}},{"groupId":"","type":"image","width":194,"height":166,"x":1043,"y":111.00624084472656,"rotate":0,"style":{"strokeStyle":"#000000","fillStyle":"transparent","lineWidth":2,"lineDash":0,"globalAlpha":0.8},"url":"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAACmCAYAAACIhFB9AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAAAf5SURBVHhe7dzpbxRlHMDx/VN86Tt9YWJ8oTEmHonGxCMe8UrQBE0IETURIqJGQ1Q84hmDonKqIKX0AHoirbaFHlDswVGOFhBRKG13uy1F/Pn7PbjNbnm6LOzO7rD7ffFJ23lmmN3tfHeema5GrrvxXgFKHSEAihAARQiAIgRAEQKgCAFQhICMvbnsG+/yYkAIyBghAIoQAEUIgCIEQBFCAMob+6X/j39T9B6/IL/sPinPvfKRd5tCuPuxl6Sp+9Qlj3VjQ693/Xy67f4X5OsNzbLnyIR7TLsPxeXLdY1y891zvOtnK8gQHpyzWGrajsq8xV96x4NWsBAqduyXtz/dkLLshtufkE9W1kpL7xl5ct7SlLFCW/DWctk7eE5W/PybdzzfLAJ7Dc39Ty90y56e/647mCzSm+585pJtshVkCMuWV0rfiQvyc12PXH/LQ951ghSqEIy9CKsr20NzwCV88E219Bz/R17/YK13PN/e+PhHqdk56IJIXm4/PzP/vUAOpqBCsMdc335Mugcnpf3AmDw69y3vekEKXQjGltu4b6xQNtTulc6BmDvIfOP5Zm8U+X6zCCqEV95Z4aZ19mbTfjDqzg6+9YIUyhDsBbFrCN9YIdzxwDxp6Dzhrl/ueuRF7zr5tvynJllb1ekdC0oQIdiZy6ZDdkaw1znx/cwzXdBCF8I9j74sDR3HZcmH6y4ZK5THn39bOvSdKkxxzlmwTFr7hmXpF2Xu2sq3Tq4FEYJd1ySfBWzq2TUwLi8s+uySdYNU0BDsgN/U2DetfPs+N0dctXlXYHc+rsarS7+TnqEp+Xx1vXc8X+wCeNF7q+Tdr8qdlZvapPfYeRfp92Vt08uT2fq5unAOIgS7yzXzjpyxY8C3flAKGsIPW7pSfmnflrWEMgQLwEKwIHzj+TIzhJmv3c79oy4MCySxfPGyNTl7LXMdgp39m7v/csdC8nOxn9v6z8rDzy7xbhcEpkYZsCmRvevaFMk3HhY2317y0Q9uyvT8wk+962Qj1yHYnS/fNMh+tuU2nrw8SFwsX4ZdHNtFsl0s28Wcb52wsYtou5j2jWUjlyHY2a361wFn5tQt3VhQQhmCLbdx31i+2e1Su21qt09942EU1K3VXIZw631z3Wv62vtrvOO23MZtPd94roUuhLD9Qc3uYtgf0uws5RsvFHudLFLfH9S2tRwJZFoRxMVyWIQqBLuo+2xVXag+YvHdplb30Qr7iIVvvFBsymAfpbCPVNgtSFtmF5f2upog7sPnIgT7nfvuEl3ObLOHXClYCLN96M4+NvDU/7/YQrKDbObjsw/f2YfwfOsXgr1x2O1Hu5C3x2cfvrMP4QX1xyhCABRTI0ARAqAIAVCEABQ5QgAUIQCKEABFCIAiBEARAqAIAVCEAChCABQhAIoQABU5OxaTs6MxGR6JyvCosq9AiYmMRMfFxTB28etIinGgJERGY3ExFsRo9OL3iZ+BUhEZG5+Q6P/se6AURWITkxKNawjxSYkBJSoyPnFOxicvsihmMw4UsUh8ckpMIgagFEXi56ZkAihxEd9CoNREJqfOC1DqOCMAKuWM4FsBKAVMjQBFCIAiBEARAqAIAVCEAChCABQhAIoQUDROj8S8yzNBCCgahAAoQgAUIQCKEABFCIAiBEBlEkIsPuH09PbJ7z2908sJAUUjkxB6+/qlfPNmqampkeGRUTl56i/ZsmULIYTB6eGzsm3bNvc1sWxHU5McGRx035/6+7T7xQ0OHZsez4V0+z189KhUVFTK+vXr3dejQ0Mp22Yj3X4PHByQ8vJyt187QO25J2+bTiYh2H+NZvsZGRtLWU4IIfD3mWHZunWr+5pYljgw7ECorKySsrKy6TByZbb9Dhw+LL/s2CH7DxyU+OQ5ae/okNraWomOx1O2v1rp9tvS2iZDx467fTU0NrrlydumwzXCNW62A8MOfDsg7ExgB2K+Qpi5n4FDh9wZaXQsmrL8amW63z3d3dK4fXvKsnQI4RpnB0RVVZV07d7tLuBM8oFvB2BQIaTbr7H/HaIdjC0trSnbZiPdfu252lmhrr7ezeWv5DlnEoKddYydbSw0O+PZFI0QQiCsIdh8eueuXW4+P3NOnY10+7UzYP++/dLa1iabKyqks6vL+2/4ZBLCHyf/dNc89rzs+dljaW7+lRDCwH4Z6aYKQYYw234TEdi43V1J3i5bl3u+CVc6JWNqdI273IFRiBD2/t7j3rXt9mLyNrkw2377+vdJU1Ozu0FgIXZ0dkpdXV3GF+mEcI1LdzvRvh+LxtycOV+3T+2ALNf5ud3CTLAD1x5H8vZXa7b92i1bO/g3btw4ffv0SkK8khDs303+twkBRSOTECy0+oYGqa6udm80FreFTwgoGpmEYHfB7Non8QdCQkDR4RoBUIQAKEIAFCEAihAARQhAlggBUIQAKEIAFCEAihAARQiAIgRAEQKgCAFQhAAoQgAUIQCKEABFCIAiBEARAqAIAVCEAChCABQhAIoQAEUIgCIEQBECoAgBUIQAKEIAFCEAihAARQiAIgRAEQKgCAFQhAAoQgAUIQCKEABFCIAiBEARAqAIAVCEAChCABQhAIoQAEUIgCIEQBECoAgBUIQAKEIAFCEAihAARQiAIgRAEQKgCAFQhAAoQgAUIQCKEABFCIAiBEARAqAIAVCEAChCABQhAIoQAEUIgCIEQBECoAgBUIQAKEIAFCEAihCAqfPyH7Mc1SZ1bXg7AAAAAElFTkSuQmCC","ratio":1.1686746987951808},{"groupId":"","type":"circle","width":70.25667228100119,"height":70.25667228100119,"x":676.8716638594994,"y":-3.122095295774031,"rotate":5,"style":{"strokeStyle":"#000000","fillStyle":"#EF1D1D","lineWidth":2,"lineDash":5,"globalAlpha":1}},{"groupId":"","type":"freedraw","width":70,"height":66,"x":759,"y":216,"rotate":0,"style":{"strokeStyle":"#000000","fillStyle":"transparent","lineWidth":2,"lineDash":0,"globalAlpha":1},"pointArr":[[759,216,2.5],[760,216,2.75],[761,216,2.875],[762,216,2.9375],[764,216,2.96875],[769,216,2.984375],[773,216,2.9921875],[776,216,2.99609375],[778,217,2.998046875],[778,217,2.9990234375],[780,218,2.99951171875],[782,219,2.999755859375],[783,221,2.9998779296875],[785,223,2.99993896484375],[786,226,2.999969482421875],[789,231,2.9999847412109375],[790,235,2.9999923706054688],[791,238,2.9999961853027344],[793,241,2.999998092651367],[794,245,2.9999990463256836],[796,249,2.999999523162842],[797,252,2.999999761581421],[798,254,2.9999998807907104],[798,255,2.9999999403953552],[798,256,2.9999999701976776],[798,257,2.999999985098839],[798,258,2.9999999925494194],[798,262,2.9999999962747097],[799,266,2.999999998137355],[800,269,2.9999999990686774],[800,270,2.9999999995343387],[800,271,2.9999999997671694],[800,272,2.9999999998835847],[801,272,2.9999999999417923],[802,272,2.999999999970896],[802,273,2.999999999985448],[802,274,2.999999999992724],[802,274,2.999999999996362],[802,274,2.999999999998181],[803,274,2.9999999999990905],[804,275,2.9999999999995453],[805,276,2.9999999999997726],[806,277,2.9999999999998863],[807,278,2.999999999999943],[809,278,2.9999999999999716],[811,279,2.999999999999986],[814,280,2.999999999999993],[818,281,2.9999999999999964],[822,282,2.9999999999999982],[826,282,2.999999999999999],[828,282,2.9999999999999996],[829,282,3]]},{"groupId":"","type":"line","width":48,"height":86,"x":576,"y":32.00624084472656,"rotate":262,"style":{"strokeStyle":"#22DE51","fillStyle":"transparent","lineWidth":2,"lineDash":0,"globalAlpha":1},"pointArr":[[624,32.00624084472656],[576,118.00624084472656]]}]}', NULL);
INSERT INTO public.cms_docs (docs_id, folder_id, content_id, docs_name, order_num, docs_type, del_flag, create_by, create_time, update_by, update_time, content, content_markdown) VALUES (239, 219, NULL, '流程图', 0, 'bpmn', '0', 'admin', '2023-07-06 00:51:56', 'admin', '2023-07-14 12:36:11', '<?xml version="1.0" encoding="UTF-8"?>
<definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI" xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" id="sid-38422fae-e03e-43a3-bef4-bd33b32041b2" targetNamespace="http://bpmn.io/bpmn" exporter="bpmn-js (https://demo.bpmn.io)" exporterVersion="5.1.2">
  <process id="Process_1" isExecutable="false">
    <laneSet id="LaneSet_0bimkyb" />
    <startEvent id="StartEvent_1y45yut" name="开始">
      <outgoing>SequenceFlow_0h21x7r</outgoing>
    </startEvent>
    <sequenceFlow id="SequenceFlow_0h21x7r" sourceRef="StartEvent_1y45yut" targetRef="Task_1hcentk" />
    <task id="Task_1hcentk">
      <incoming>SequenceFlow_0h21x7r</incoming>
      <outgoing>Flow_05nx7t2</outgoing>
    </task>
    <subProcess id="Activity_0r2wz62">
      <incoming>Flow_05nx7t2</incoming>
      <startEvent id="Event_11oj9lf">
        <outgoing>Flow_0ysyuwl</outgoing>
      </startEvent>
      <exclusiveGateway id="Gateway_0ij4sax">
        <incoming>Flow_0ysyuwl</incoming>
        <outgoing>Flow_0c92zh7</outgoing>
      </exclusiveGateway>
      <sequenceFlow id="Flow_0ysyuwl" sourceRef="Event_11oj9lf" targetRef="Gateway_0ij4sax" />
      <endEvent id="Event_0ilg522">
        <incoming>Flow_0c92zh7</incoming>
      </endEvent>
      <sequenceFlow id="Flow_0c92zh7" sourceRef="Gateway_0ij4sax" targetRef="Event_0ilg522" />
    </subProcess>
    <sequenceFlow id="Flow_05nx7t2" sourceRef="Task_1hcentk" targetRef="Activity_0r2wz62" />
  </process>
  <bpmndi:BPMNDiagram id="BpmnDiagram_1">
    <bpmndi:BPMNPlane id="BpmnPlane_1" bpmnElement="Process_1">
      <bpmndi:BPMNEdge id="SequenceFlow_0h21x7r_di" bpmnElement="SequenceFlow_0h21x7r">
        <omgdi:waypoint x="188" y="120" />
        <omgdi:waypoint x="240" y="120" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_05nx7t2_di" bpmnElement="Flow_05nx7t2">
        <omgdi:waypoint x="340" y="120" />
        <omgdi:waypoint x="460" y="120" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="StartEvent_1y45yut_di" bpmnElement="StartEvent_1y45yut">
        <omgdc:Bounds x="152" y="102" width="36" height="36" />
        <bpmndi:BPMNLabel>
          <omgdc:Bounds x="160" y="145" width="23" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Task_1hcentk_di" bpmnElement="Task_1hcentk">
        <omgdc:Bounds x="240" y="80" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_0r2wz62_di" bpmnElement="Activity_0r2wz62" isExpanded="true">
        <omgdc:Bounds x="460" y="20" width="350" height="200" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="Flow_0ysyuwl_di" bpmnElement="Flow_0ysyuwl">
        <omgdi:waypoint x="536" y="120" />
        <omgdi:waypoint x="595" y="120" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_0c92zh7_di" bpmnElement="Flow_0c92zh7">
        <omgdi:waypoint x="645" y="120" />
        <omgdi:waypoint x="712" y="120" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="Event_11oj9lf_di" bpmnElement="Event_11oj9lf">
        <omgdc:Bounds x="500" y="102" width="36" height="36" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Gateway_0ij4sax_di" bpmnElement="Gateway_0ij4sax" isMarkerVisible="true">
        <omgdc:Bounds x="595" y="95" width="50" height="50" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Event_0ilg522_di" bpmnElement="Event_0ilg522">
        <omgdc:Bounds x="712" y="102" width="36" height="36" />
      </bpmndi:BPMNShape>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</definitions>
', NULL);
INSERT INTO public.cms_docs (docs_id, folder_id, content_id, docs_name, order_num, docs_type, del_flag, create_by, create_time, update_by, update_time, content, content_markdown) VALUES (243, 219, NULL, 'code', 0, 'code', '0', 'admin', '2023-07-14 10:23:12', 'admin', '2023-07-14 12:49:08', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<meta name="keywords" content="百度地图,百度地图API，百度地图自定义工具，百度地图所见即所得工具" />
<meta name="description" content="百度地图API自定义地图，帮助用户在可视化操作下生成百度地图" />
<title>百度地图API自定义地图</title>
<!--引用百度地图API-->
<style type="text/css">
    html,body{margin:0;padding:0;}
    .iw_poi_title {color:#CC5522;font-size:14px;font-weight:bold;overflow:hidden;padding-right:13px;white-space:nowrap}
    .iw_poi_content {font:12px arial,sans-serif;overflow:visible;padding-top:4px;white-space:-moz-pre-wrap;word-wrap:break-word}
</style>
<script type="text/javascript" src="http://api.map.baidu.com/api?key=&v=1.1&services=true"></script>
</head>

<body>
  <!--百度地图容器-->
  <div style="width:100%;height:100vh;border:#ccc solid 1px;" id="dituContent"></div>
</body>
<script type="text/javascript">
    //创建和初始化地图函数：
    function initMap(){
        createMap();//创建地图
        setMapEvent();//设置地图事件
        addMapControl();//向地图添加控件
        addMarker();//向地图中添加marker
        addRemark();//向地图中添加文字标注
    }
    
    //创建地图函数：
    function createMap(){
        var map = new BMap.Map("dituContent");//在百度地图容器中创建一个地图
        var point = new BMap.Point(114.518729,38.070685);//定义一个中心点坐标
        map.centerAndZoom(point,12);//设定地图的中心点和坐标并将地图显示在地图容器中
        window.map = map;//将map变量存储在全局
    }
    
    //地图事件设置函数：
    function setMapEvent(){
        map.enableDragging();//启用地图拖拽事件，默认启用(可不写)
        map.enableScrollWheelZoom();//启用地图滚轮放大缩小
        map.enableDoubleClickZoom();//启用鼠标双击放大，默认启用(可不写)
        map.enableKeyboard();//启用键盘上下左右键移动地图
    }
    
    //地图控件添加函数：
    function addMapControl(){
        //向地图中添加缩放控件
	var ctrl_nav = new BMap.NavigationControl({anchor:BMAP_ANCHOR_TOP_LEFT,type:BMAP_NAVIGATION_CONTROL_LARGE});
	map.addControl(ctrl_nav);
        //向地图中添加缩略图控件
	var ctrl_ove = new BMap.OverviewMapControl({anchor:BMAP_ANCHOR_BOTTOM_RIGHT,isOpen:1});
	map.addControl(ctrl_ove);
        //向地图中添加比例尺控件
	var ctrl_sca = new BMap.ScaleControl({anchor:BMAP_ANCHOR_BOTTOM_LEFT});
	map.addControl(ctrl_sca);
    }
    
    //标注点数组
    var markerArr = [{title:"国际庄",content:"摇滚之都",point:"114.49081|38.016692",isOpen:0,icon:{w:21,h:21,l:0,t:0,x:6,lb:5}}
		 ];
    //创建marker
    function addMarker(){
        for(var i=0;i<markerArr.length;i++){
            var json = markerArr[i];
            var p0 = json.point.split("|")[0];
            var p1 = json.point.split("|")[1];
            var point = new BMap.Point(p0,p1);
			var iconImg = createIcon(json.icon);
            var marker = new BMap.Marker(point,{icon:iconImg});
			var iw = createInfoWindow(i);
			var label = new BMap.Label(json.title,{"offset":new BMap.Size(json.icon.lb-json.icon.x+10,-20)});
			marker.setLabel(label);
            map.addOverlay(marker);
            label.setStyle({
                        borderColor:"#808080",
                        color:"#333",
                        cursor:"pointer"
            });
			
			(function(){
				var index = i;
				var _iw = createInfoWindow(i);
				var _marker = marker;
				_marker.addEventListener("click",function(){
				    this.openInfoWindow(_iw);
			    });
			    _iw.addEventListener("open",function(){
				    _marker.getLabel().hide();
			    })
			    _iw.addEventListener("close",function(){
				    _marker.getLabel().show();
			    })
				label.addEventListener("click",function(){
				    _marker.openInfoWindow(_iw);
			    })
				if(!!json.isOpen){
					label.hide();
					_marker.openInfoWindow(_iw);
				}
			})()
        }
    }
    //创建InfoWindow
    function createInfoWindow(i){
        var json = markerArr[i];
        var iw = new BMap.InfoWindow("<b class=''iw_poi_title'' title=''" + json.title + "''>" + json.title + "</b><div class=''iw_poi_content''>"+json.content+"</div>");
        return iw;
    }
    //创建一个Icon
    function createIcon(json){
        var icon = new BMap.Icon("http://app.baidu.com/map/images/us_mk_icon.png", new BMap.Size(json.w,json.h),{imageOffset: new BMap.Size(-json.l,-json.t),infoWindowOffset:new BMap.Size(json.lb+5,1),offset:new BMap.Size(json.x,json.h)})
        return icon;
    }
//文字标注数组
    var lbPoints = [{point:"114.621351|38.068867",content:"摇滚吧"}
		 ];
    //向地图中添加文字标注函数
    function addRemark(){
        for(var i=0;i<lbPoints.length;i++){
            var json = lbPoints[i];
            var p1 = json.point.split("|")[0];
            var p2 = json.point.split("|")[1];
            var label = new BMap.Label("<div style=''padding:2px;''>"+json.content+"</div>",{point:new BMap.Point(p1,p2),offset:new BMap.Size(3,-6)});
            map.addOverlay(label);
            label.setStyle({borderColor:"#999"});
        }
    }
    
    initMap();//创建和初始化地图
</script>
</html>', NULL);


--
-- Data for Name: cms_folder; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.cms_folder (folder_id, parent_id, ancestors, folder_name, order_num, status, del_flag, create_by, create_time, update_by, update_time) VALUES (219, 0, '0', '默认文件夹', 0, '0', '0', 'admin', '2023-06-26 13:04:28', 'admin', NULL);


--
-- Data for Name: flyway_schema_history; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.flyway_schema_history (installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (1, '1', '<< Flyway Baseline >>', 'BASELINE', '<< Flyway Baseline >>', NULL, 'null', '2024-05-27 00:04:51.407944', 0, true);
INSERT INTO public.flyway_schema_history (installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (2, '1.240527', 'INIT', 'SQL', 'V1.240527__INIT.sql', -690524359, 'postgres', '2024-05-27 00:04:51.989658', 242, true);


--
-- Data for Name: gen_table; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.gen_table (table_id, table_name, table_comment, sub_table_name, sub_table_fk_name, class_name, tpl_category, tpl_web_type, package_name, module_name, business_name, function_name, function_author, gen_type, gen_path, options, create_by, create_time, update_by, update_time, remark) VALUES (2, 'cms_folder', '文件夹表', '', '', 'CmsFolder', 'tree', 'element-ui', 'com.ruoyi.cms.folder', 'cms', 'folder', '文件夹', 'ning', '0', '/', '{"treeCode":"folder_id","treeName":"folder_name","treeParentCode":"parent_id","parentMenuId":"2000"}', 'admin', '2023-06-25 15:46:00', '', '2023-06-25 16:06:17', '文件夹表');
INSERT INTO public.gen_table (table_id, table_name, table_comment, sub_table_name, sub_table_fk_name, class_name, tpl_category, tpl_web_type, package_name, module_name, business_name, function_name, function_author, gen_type, gen_path, options, create_by, create_time, update_by, update_time, remark) VALUES (3, 'cms_docs', '文件表', '', '', 'CmsDocs', 'crud', 'element-ui', 'com.ruoyi.cms.docs', 'cms', 'docs', '文件', 'ning', '0', '/', '{"parentMenuId":2000}', 'admin', '2023-06-25 23:02:28', '', '2023-06-25 23:04:24', '文件表');


--
-- Data for Name: gen_table_column; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.gen_table_column (column_id, table_id, column_name, column_comment, column_type, java_type, java_field, is_pk, is_increment, is_required, is_insert, is_edit, is_list, is_query, query_type, html_type, dict_type, sort, create_by, create_time, update_by, update_time) VALUES (15, 2, 'folder_id', '文件夹id', 'bigint(20)', 'Long', 'folderId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-06-25 15:46:00', '', '2023-06-25 16:06:17');
INSERT INTO public.gen_table_column (column_id, table_id, column_name, column_comment, column_type, java_type, java_field, is_pk, is_increment, is_required, is_insert, is_edit, is_list, is_query, query_type, html_type, dict_type, sort, create_by, create_time, update_by, update_time) VALUES (16, 2, 'parent_id', '父文件夹id', 'bigint(20)', 'Long', 'parentId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2023-06-25 15:46:00', '', '2023-06-25 16:06:17');
INSERT INTO public.gen_table_column (column_id, table_id, column_name, column_comment, column_type, java_type, java_field, is_pk, is_increment, is_required, is_insert, is_edit, is_list, is_query, query_type, html_type, dict_type, sort, create_by, create_time, update_by, update_time) VALUES (17, 2, 'ancestors', '祖级列表', 'varchar(50)', 'String', 'ancestors', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-06-25 15:46:00', '', '2023-06-25 16:06:17');
INSERT INTO public.gen_table_column (column_id, table_id, column_name, column_comment, column_type, java_type, java_field, is_pk, is_increment, is_required, is_insert, is_edit, is_list, is_query, query_type, html_type, dict_type, sort, create_by, create_time, update_by, update_time) VALUES (18, 2, 'folder_name', '文件夹名称', 'varchar(30)', 'String', 'folderName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 4, 'admin', '2023-06-25 15:46:00', '', '2023-06-25 16:06:17');
INSERT INTO public.gen_table_column (column_id, table_id, column_name, column_comment, column_type, java_type, java_field, is_pk, is_increment, is_required, is_insert, is_edit, is_list, is_query, query_type, html_type, dict_type, sort, create_by, create_time, update_by, update_time) VALUES (19, 2, 'order_num', '显示顺序', 'int(4)', 'Integer', 'orderNum', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2023-06-25 15:46:00', '', '2023-06-25 16:06:17');
INSERT INTO public.gen_table_column (column_id, table_id, column_name, column_comment, column_type, java_type, java_field, is_pk, is_increment, is_required, is_insert, is_edit, is_list, is_query, query_type, html_type, dict_type, sort, create_by, create_time, update_by, update_time) VALUES (20, 2, 'status', '文件夹状态（0正常 1停用）', 'char(1)', 'String', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 6, 'admin', '2023-06-25 15:46:00', '', '2023-06-25 16:06:17');
INSERT INTO public.gen_table_column (column_id, table_id, column_name, column_comment, column_type, java_type, java_field, is_pk, is_increment, is_required, is_insert, is_edit, is_list, is_query, query_type, html_type, dict_type, sort, create_by, create_time, update_by, update_time) VALUES (21, 2, 'del_flag', '删除标志（0代表存在 2代表删除）', 'char(1)', 'String', 'delFlag', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 7, 'admin', '2023-06-25 15:46:00', '', '2023-06-25 16:06:17');
INSERT INTO public.gen_table_column (column_id, table_id, column_name, column_comment, column_type, java_type, java_field, is_pk, is_increment, is_required, is_insert, is_edit, is_list, is_query, query_type, html_type, dict_type, sort, create_by, create_time, update_by, update_time) VALUES (22, 2, 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 8, 'admin', '2023-06-25 15:46:00', '', '2023-06-25 16:06:17');
INSERT INTO public.gen_table_column (column_id, table_id, column_name, column_comment, column_type, java_type, java_field, is_pk, is_increment, is_required, is_insert, is_edit, is_list, is_query, query_type, html_type, dict_type, sort, create_by, create_time, update_by, update_time) VALUES (23, 2, 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2023-06-25 15:46:00', '', '2023-06-25 16:06:17');
INSERT INTO public.gen_table_column (column_id, table_id, column_name, column_comment, column_type, java_type, java_field, is_pk, is_increment, is_required, is_insert, is_edit, is_list, is_query, query_type, html_type, dict_type, sort, create_by, create_time, update_by, update_time) VALUES (24, 2, 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 10, 'admin', '2023-06-25 15:46:00', '', '2023-06-25 16:06:17');
INSERT INTO public.gen_table_column (column_id, table_id, column_name, column_comment, column_type, java_type, java_field, is_pk, is_increment, is_required, is_insert, is_edit, is_list, is_query, query_type, html_type, dict_type, sort, create_by, create_time, update_by, update_time) VALUES (25, 2, 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 11, 'admin', '2023-06-25 15:46:00', '', '2023-06-25 16:06:17');
INSERT INTO public.gen_table_column (column_id, table_id, column_name, column_comment, column_type, java_type, java_field, is_pk, is_increment, is_required, is_insert, is_edit, is_list, is_query, query_type, html_type, dict_type, sort, create_by, create_time, update_by, update_time) VALUES (26, 3, 'docs_id', '文件id', 'bigint(20)', 'Long', 'docsId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-06-25 23:02:28', '', '2023-06-25 23:04:24');
INSERT INTO public.gen_table_column (column_id, table_id, column_name, column_comment, column_type, java_type, java_field, is_pk, is_increment, is_required, is_insert, is_edit, is_list, is_query, query_type, html_type, dict_type, sort, create_by, create_time, update_by, update_time) VALUES (27, 3, 'folder_id', '文件夹id', 'bigint(20)', 'Long', 'folderId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2023-06-25 23:02:28', '', '2023-06-25 23:04:24');
INSERT INTO public.gen_table_column (column_id, table_id, column_name, column_comment, column_type, java_type, java_field, is_pk, is_increment, is_required, is_insert, is_edit, is_list, is_query, query_type, html_type, dict_type, sort, create_by, create_time, update_by, update_time) VALUES (28, 3, 'content_id', '文件内容id', 'bigint(20)', 'Long', 'contentId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-06-25 23:02:28', '', '2023-06-25 23:04:24');
INSERT INTO public.gen_table_column (column_id, table_id, column_name, column_comment, column_type, java_type, java_field, is_pk, is_increment, is_required, is_insert, is_edit, is_list, is_query, query_type, html_type, dict_type, sort, create_by, create_time, update_by, update_time) VALUES (29, 3, 'docs_name', '文件名称', 'varchar(30)', 'String', 'docsName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 4, 'admin', '2023-06-25 23:02:28', '', '2023-06-25 23:04:24');
INSERT INTO public.gen_table_column (column_id, table_id, column_name, column_comment, column_type, java_type, java_field, is_pk, is_increment, is_required, is_insert, is_edit, is_list, is_query, query_type, html_type, dict_type, sort, create_by, create_time, update_by, update_time) VALUES (30, 3, 'order_num', '显示顺序', 'int(4)', 'Integer', 'orderNum', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2023-06-25 23:02:28', '', '2023-06-25 23:04:24');
INSERT INTO public.gen_table_column (column_id, table_id, column_name, column_comment, column_type, java_type, java_field, is_pk, is_increment, is_required, is_insert, is_edit, is_list, is_query, query_type, html_type, dict_type, sort, create_by, create_time, update_by, update_time) VALUES (31, 3, 'docs_type', '文件类型', 'varchar(30)', 'String', 'docsType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 6, 'admin', '2023-06-25 23:02:28', '', '2023-06-25 23:04:24');
INSERT INTO public.gen_table_column (column_id, table_id, column_name, column_comment, column_type, java_type, java_field, is_pk, is_increment, is_required, is_insert, is_edit, is_list, is_query, query_type, html_type, dict_type, sort, create_by, create_time, update_by, update_time) VALUES (32, 3, 'del_flag', '删除标志（0代表存在 2代表删除）', 'char(1)', 'String', 'delFlag', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 7, 'admin', '2023-06-25 23:02:28', '', '2023-06-25 23:04:24');
INSERT INTO public.gen_table_column (column_id, table_id, column_name, column_comment, column_type, java_type, java_field, is_pk, is_increment, is_required, is_insert, is_edit, is_list, is_query, query_type, html_type, dict_type, sort, create_by, create_time, update_by, update_time) VALUES (33, 3, 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 8, 'admin', '2023-06-25 23:02:28', '', '2023-06-25 23:04:24');
INSERT INTO public.gen_table_column (column_id, table_id, column_name, column_comment, column_type, java_type, java_field, is_pk, is_increment, is_required, is_insert, is_edit, is_list, is_query, query_type, html_type, dict_type, sort, create_by, create_time, update_by, update_time) VALUES (34, 3, 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2023-06-25 23:02:28', '', '2023-06-25 23:04:24');
INSERT INTO public.gen_table_column (column_id, table_id, column_name, column_comment, column_type, java_type, java_field, is_pk, is_increment, is_required, is_insert, is_edit, is_list, is_query, query_type, html_type, dict_type, sort, create_by, create_time, update_by, update_time) VALUES (35, 3, 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 10, 'admin', '2023-06-25 23:02:28', '', '2023-06-25 23:04:24');
INSERT INTO public.gen_table_column (column_id, table_id, column_name, column_comment, column_type, java_type, java_field, is_pk, is_increment, is_required, is_insert, is_edit, is_list, is_query, query_type, html_type, dict_type, sort, create_by, create_time, update_by, update_time) VALUES (36, 3, 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 11, 'admin', '2023-06-25 23:02:28', '', '2023-06-25 23:04:24');


--
-- Data for Name: magic_api_backup; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.magic_api_backup (id, create_date, tag, type, name, content, create_by) VALUES ('2742c4d812c94f6eb1822f46adf85e71', 1716219575280, NULL, 'api', '未定义名称', '\x7b0d0a20202270726f7065727469657322203a207b207d2c0d0a202022696422203a20223237343263346438313263393466366562313832326634366164663835653731222c0d0a20202273637269707422203a202272657475726e202748656c6c6f206d616769632d61706927222c0d0a20202267726f7570496422203a20223631663937623862633963373437353138643235376262646434316537343134222c0d0a2020226e616d6522203a2022e69caae5ae9ae4b989e5908de7a7b0222c0d0a20202263726561746554696d6522203a206e756c6c2c0d0a20202275706461746554696d6522203a20313731363231393537353232372c0d0a2020226c6f636b22203a206e756c6c2c0d0a202022637265617465427922203a206e756c6c2c0d0a202022757064617465427922203a202272756f7969222c0d0a2020227061746822203a20222f61222c0d0a2020226d6574686f6422203a2022474554222c0d0a202022706172616d657465727322203a205b205d2c0d0a2020226f7074696f6e7322203a205b207b0d0a20202020226e616d6522203a2022726571756972655f6c6f67696e222c0d0a202020202276616c756522203a202274727565222c0d0a20202020226465736372697074696f6e22203a2022e8afa5e68ea5e58fa3e99c80e8a681e799bbe5bd95e6898de58581e8aeb8e8aebfe997ae222c0d0a2020202022726571756972656422203a2066616c73652c0d0a2020202022646174615479706522203a2022537472696e67222c0d0a20202020227479706522203a206e756c6c2c0d0a202020202264656661756c7456616c756522203a206e756c6c2c0d0a202020202276616c69646174655479706522203a206e756c6c2c0d0a20202020226572726f7222203a206e756c6c2c0d0a202020202265787072657373696f6e22203a206e756c6c2c0d0a20202020226368696c6472656e22203a206e756c6c0d0a20207d205d2c0d0a20202272657175657374426f647922203a206e756c6c2c0d0a2020226865616465727322203a205b205d2c0d0a202022706174687322203a205b205d2c0d0a202022726573706f6e7365426f647922203a20227b5c6e202020205c22636f64655c223a203230302c5c6e202020205c226d6573736167655c223a205c22737563636573735c222c5c6e202020205c22646174615c223a205c2248656c6c6f206d616769632d6170695c222c5c6e202020205c2274696d657374616d705c223a205c22313731363231393533393935335c222c5c6e202020205c227265717565737454696d655c223a205c22313731363231393533393838335c222c5c6e202020205c226578656375746554696d655c223a205c2237305c225c6e7d222c0d0a2020226465736372697074696f6e22203a206e756c6c2c0d0a20202272657175657374426f6479446566696e6974696f6e22203a206e756c6c2c0d0a202022726573706f6e7365426f6479446566696e6974696f6e22203a206e756c6c0d0a7d', 'ruoyi');
INSERT INTO public.magic_api_backup (id, create_date, tag, type, name, content, create_by) VALUES ('2742c4d812c94f6eb1822f46adf85e71', 1716219684097, NULL, 'api', '未定义名称', '\x7b0d0a20202270726f7065727469657322203a207b207d2c0d0a202022696422203a20223237343263346438313263393466366562313832326634366164663835653731222c0d0a20202273637269707422203a202272657475726e202748656c6c6f206d616769632d61706927222c0d0a20202267726f7570496422203a20223631663937623862633963373437353138643235376262646434316537343134222c0d0a2020226e616d6522203a2022e69caae5ae9ae4b989e5908de7a7b0222c0d0a20202263726561746554696d6522203a206e756c6c2c0d0a20202275706461746554696d6522203a20313731363231393638343032362c0d0a2020226c6f636b22203a206e756c6c2c0d0a202022637265617465427922203a206e756c6c2c0d0a202022757064617465427922203a202272756f7969222c0d0a2020227061746822203a20222f61222c0d0a2020226d6574686f6422203a2022474554222c0d0a202022706172616d657465727322203a205b205d2c0d0a2020226f7074696f6e7322203a205b207b0d0a20202020226e616d6522203a2022726571756972655f6c6f67696e222c0d0a202020202276616c756522203a202274727565222c0d0a20202020226465736372697074696f6e22203a2022e8afa5e68ea5e58fa3e99c80e8a681e799bbe5bd95e6898de58581e8aeb8e8aebfe997ae222c0d0a2020202022726571756972656422203a2066616c73652c0d0a2020202022646174615479706522203a2022537472696e67222c0d0a20202020227479706522203a206e756c6c2c0d0a202020202264656661756c7456616c756522203a206e756c6c2c0d0a202020202276616c69646174655479706522203a206e756c6c2c0d0a20202020226572726f7222203a206e756c6c2c0d0a202020202265787072657373696f6e22203a206e756c6c2c0d0a20202020226368696c6472656e22203a206e756c6c0d0a20207d205d2c0d0a20202272657175657374426f647922203a206e756c6c2c0d0a2020226865616465727322203a205b205d2c0d0a202022706174687322203a205b205d2c0d0a202022726573706f6e7365426f647922203a20227b5c6e202020205c226d73675c223a205c22746f6b656e20e5b7b2e5a4b1e695885c222c5c6e202020205c22636f64655c223a203430315c6e7d222c0d0a2020226465736372697074696f6e22203a206e756c6c2c0d0a20202272657175657374426f6479446566696e6974696f6e22203a206e756c6c2c0d0a202022726573706f6e7365426f6479446566696e6974696f6e22203a206e756c6c0d0a7d', 'ruoyi');
INSERT INTO public.magic_api_backup (id, create_date, tag, type, name, content, create_by) VALUES ('2742c4d812c94f6eb1822f46adf85e71', 1716219727795, NULL, 'api', '未定义名称', '\x7b0d0a20202270726f7065727469657322203a207b207d2c0d0a202022696422203a20223237343263346438313263393466366562313832326634366164663835653731222c0d0a20202273637269707422203a202272657475726e202748656c6c6f206d616769632d61706927222c0d0a20202267726f7570496422203a20223631663937623862633963373437353138643235376262646434316537343134222c0d0a2020226e616d6522203a2022e69caae5ae9ae4b989e5908de7a7b0222c0d0a20202263726561746554696d6522203a206e756c6c2c0d0a20202275706461746554696d6522203a20313731363231393732373731382c0d0a2020226c6f636b22203a206e756c6c2c0d0a202022637265617465427922203a206e756c6c2c0d0a202022757064617465427922203a202272756f7969222c0d0a2020227061746822203a20222f61222c0d0a2020226d6574686f6422203a2022474554222c0d0a202022706172616d657465727322203a205b205d2c0d0a2020226f7074696f6e7322203a205b207b0d0a20202020226e616d6522203a2022726571756972655f6c6f67696e222c0d0a202020202276616c756522203a202266616c7365222c0d0a20202020226465736372697074696f6e22203a2022e8afa5e68ea5e58fa3e99c80e8a681e799bbe5bd95e6898de58581e8aeb8e8aebfe997ae222c0d0a2020202022726571756972656422203a2066616c73652c0d0a2020202022646174615479706522203a2022537472696e67222c0d0a20202020227479706522203a206e756c6c2c0d0a202020202264656661756c7456616c756522203a206e756c6c2c0d0a202020202276616c69646174655479706522203a206e756c6c2c0d0a20202020226572726f7222203a206e756c6c2c0d0a202020202265787072657373696f6e22203a206e756c6c2c0d0a20202020226368696c6472656e22203a206e756c6c0d0a20207d205d2c0d0a20202272657175657374426f647922203a206e756c6c2c0d0a2020226865616465727322203a205b205d2c0d0a202022706174687322203a205b205d2c0d0a202022726573706f6e7365426f647922203a20227b5c6e202020205c226d73675c223a205c22746f6b656e20e5b7b2e5a4b1e695885c222c5c6e202020205c22636f64655c223a203430315c6e7d222c0d0a2020226465736372697074696f6e22203a206e756c6c2c0d0a20202272657175657374426f6479446566696e6974696f6e22203a206e756c6c2c0d0a202022726573706f6e7365426f6479446566696e6974696f6e22203a206e756c6c0d0a7d', 'ruoyi');
INSERT INTO public.magic_api_backup (id, create_date, tag, type, name, content, create_by) VALUES ('2742c4d812c94f6eb1822f46adf85e71', 1716219834293, NULL, 'api', '未定义名称', '\x7b0d0a20202270726f7065727469657322203a207b207d2c0d0a202022696422203a20223237343263346438313263393466366562313832326634366164663835653731222c0d0a20202273637269707422203a202272657475726e202748656c6c6f206d616769632d61706927222c0d0a20202267726f7570496422203a20223631663937623862633963373437353138643235376262646434316537343134222c0d0a2020226e616d6522203a2022e69caae5ae9ae4b989e5908de7a7b0222c0d0a20202263726561746554696d6522203a206e756c6c2c0d0a20202275706461746554696d6522203a20313731363231393833343235302c0d0a2020226c6f636b22203a206e756c6c2c0d0a202022637265617465427922203a206e756c6c2c0d0a202022757064617465427922203a202272756f7969222c0d0a2020227061746822203a20222f61222c0d0a2020226d6574686f6422203a2022474554222c0d0a202022706172616d657465727322203a205b205d2c0d0a2020226f7074696f6e7322203a205b207b0d0a20202020226e616d6522203a2022726571756972655f6c6f67696e222c0d0a202020202276616c756522203a202274727565222c0d0a20202020226465736372697074696f6e22203a2022e8afa5e68ea5e58fa3e99c80e8a681e799bbe5bd95e6898de58581e8aeb8e8aebfe997ae222c0d0a2020202022726571756972656422203a2066616c73652c0d0a2020202022646174615479706522203a2022537472696e67222c0d0a20202020227479706522203a206e756c6c2c0d0a202020202264656661756c7456616c756522203a206e756c6c2c0d0a202020202276616c69646174655479706522203a206e756c6c2c0d0a20202020226572726f7222203a206e756c6c2c0d0a202020202265787072657373696f6e22203a206e756c6c2c0d0a20202020226368696c6472656e22203a206e756c6c0d0a20207d205d2c0d0a20202272657175657374426f647922203a206e756c6c2c0d0a2020226865616465727322203a205b205d2c0d0a202022706174687322203a205b205d2c0d0a202022726573706f6e7365426f647922203a20227b5c6e202020205c22636f64655c223a203230302c5c6e202020205c226d6573736167655c223a205c22737563636573735c222c5c6e202020205c22646174615c223a205c2248656c6c6f206d616769632d6170695c222c5c6e202020205c2274696d657374616d705c223a205c22313731363231393735393335315c222c5c6e202020205c227265717565737454696d655c223a205c22313731363231393734353639355c222c5c6e202020205c226578656375746554696d655c223a205c2231333635365c225c6e7d222c0d0a2020226465736372697074696f6e22203a206e756c6c2c0d0a20202272657175657374426f6479446566696e6974696f6e22203a206e756c6c2c0d0a202022726573706f6e7365426f6479446566696e6974696f6e22203a206e756c6c0d0a7d', 'ruoyi');
INSERT INTO public.magic_api_backup (id, create_date, tag, type, name, content, create_by) VALUES ('2742c4d812c94f6eb1822f46adf85e71', 1716219958930, NULL, 'api', '未定义名称', '\x7b0d0a20202270726f7065727469657322203a207b207d2c0d0a202022696422203a20223237343263346438313263393466366562313832326634366164663835653731222c0d0a20202273637269707422203a202272657475726e202748656c6c6f206d616769632d61706927222c0d0a20202267726f7570496422203a20223631663937623862633963373437353138643235376262646434316537343134222c0d0a2020226e616d6522203a2022e69caae5ae9ae4b989e5908de7a7b0222c0d0a20202263726561746554696d6522203a206e756c6c2c0d0a20202275706461746554696d6522203a20313731363231393935383833332c0d0a2020226c6f636b22203a206e756c6c2c0d0a202022637265617465427922203a206e756c6c2c0d0a202022757064617465427922203a202272756f7969222c0d0a2020227061746822203a20222f61222c0d0a2020226d6574686f6422203a2022474554222c0d0a202022706172616d657465727322203a205b205d2c0d0a2020226f7074696f6e7322203a205b207b0d0a20202020226e616d6522203a2022726571756972655f6c6f67696e222c0d0a202020202276616c756522203a202274727565222c0d0a20202020226465736372697074696f6e22203a2022e8afa5e68ea5e58fa3e99c80e8a681e799bbe5bd95e6898de58581e8aeb8e8aebfe997ae222c0d0a2020202022726571756972656422203a2066616c73652c0d0a2020202022646174615479706522203a2022537472696e67222c0d0a20202020227479706522203a206e756c6c2c0d0a202020202264656661756c7456616c756522203a206e756c6c2c0d0a202020202276616c69646174655479706522203a206e756c6c2c0d0a20202020226572726f7222203a206e756c6c2c0d0a202020202265787072657373696f6e22203a206e756c6c2c0d0a20202020226368696c6472656e22203a206e756c6c0d0a20207d205d2c0d0a20202272657175657374426f647922203a206e756c6c2c0d0a2020226865616465727322203a205b205d2c0d0a202022706174687322203a205b205d2c0d0a202022726573706f6e7365426f647922203a20227b5c6e202020205c226d73675c223a205c22746f6b656e20e5b7b2e5a4b1e695885c222c5c6e202020205c22636f64655c223a203430315c6e7d222c0d0a2020226465736372697074696f6e22203a206e756c6c2c0d0a20202272657175657374426f6479446566696e6974696f6e22203a206e756c6c2c0d0a202022726573706f6e7365426f6479446566696e6974696f6e22203a207b0d0a20202020226e616d6522203a2022222c0d0a202020202276616c756522203a2022222c0d0a20202020226465736372697074696f6e22203a2022222c0d0a2020202022726571756972656422203a2066616c73652c0d0a2020202022646174615479706522203a20224f626a656374222c0d0a20202020227479706522203a206e756c6c2c0d0a202020202264656661756c7456616c756522203a206e756c6c2c0d0a202020202276616c69646174655479706522203a2022222c0d0a20202020226572726f7222203a2022222c0d0a202020202265787072657373696f6e22203a2022222c0d0a20202020226368696c6472656e22203a205b207b0d0a202020202020226e616d6522203a20226d7367222c0d0a2020202020202276616c756522203a2022746f6b656e20e5b7b2e5a4b1e69588222c0d0a202020202020226465736372697074696f6e22203a2022222c0d0a20202020202022726571756972656422203a2066616c73652c0d0a20202020202022646174615479706522203a2022537472696e67222c0d0a202020202020227479706522203a206e756c6c2c0d0a2020202020202264656661756c7456616c756522203a206e756c6c2c0d0a2020202020202276616c69646174655479706522203a2022222c0d0a202020202020226572726f7222203a2022222c0d0a2020202020202265787072657373696f6e22203a2022222c0d0a202020202020226368696c6472656e22203a205b205d0d0a202020207d2c207b0d0a202020202020226e616d6522203a2022636f6465222c0d0a2020202020202276616c756522203a2022343031222c0d0a202020202020226465736372697074696f6e22203a2022222c0d0a20202020202022726571756972656422203a2066616c73652c0d0a20202020202022646174615479706522203a2022496e7465676572222c0d0a202020202020227479706522203a206e756c6c2c0d0a2020202020202264656661756c7456616c756522203a206e756c6c2c0d0a2020202020202276616c69646174655479706522203a2022222c0d0a202020202020226572726f7222203a2022222c0d0a2020202020202265787072657373696f6e22203a2022222c0d0a202020202020226368696c6472656e22203a205b205d0d0a202020207d205d0d0a20207d0d0a7d', 'ruoyi');


--
-- Data for Name: magic_api_file; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.magic_api_file (file_path, file_content) VALUES ('/magic-api/api/', 'this is directory');
INSERT INTO public.magic_api_file (file_path, file_content) VALUES ('/magic-api/datasource/', 'this is directory');
INSERT INTO public.magic_api_file (file_path, file_content) VALUES ('/magic-api/function/', 'this is directory');
INSERT INTO public.magic_api_file (file_path, file_content) VALUES ('/magic-api/task/', 'this is directory');
INSERT INTO public.magic_api_file (file_path, file_content) VALUES ('/magic-api/api/测试/', 'this is directory');
INSERT INTO public.magic_api_file (file_path, file_content) VALUES ('/magic-api/api/测试/group.json', '{
  "properties" : { },
  "id" : "61f97b8bc9c747518d257bbdd41e7414",
  "name" : "测试",
  "type" : "api",
  "parentId" : "0",
  "path" : "/test",
  "createTime" : 1715099609278,
  "updateTime" : null,
  "createBy" : null,
  "updateBy" : null,
  "paths" : [ ],
  "options" : [ ]
}');
INSERT INTO public.magic_api_file (file_path, file_content) VALUES ('/magic-api/api/测试/未定义名称.ms', '{
  "properties" : { },
  "id" : "2742c4d812c94f6eb1822f46adf85e71",
  "script" : null,
  "groupId" : "61f97b8bc9c747518d257bbdd41e7414",
  "name" : "未定义名称",
  "createTime" : null,
  "updateTime" : 1716219958833,
  "lock" : null,
  "createBy" : null,
  "updateBy" : "ruoyi",
  "path" : "/a",
  "method" : "GET",
  "parameters" : [ ],
  "options" : [ {
    "name" : "require_login",
    "value" : "true",
    "description" : "该接口需要登录才允许访问",
    "required" : false,
    "dataType" : "String",
    "type" : null,
    "defaultValue" : null,
    "validateType" : null,
    "error" : null,
    "expression" : null,
    "children" : null
  } ],
  "requestBody" : null,
  "headers" : [ ],
  "paths" : [ ],
  "responseBody" : "{\n    \"msg\": \"token 已失效\",\n    \"code\": 401\n}",
  "description" : null,
  "requestBodyDefinition" : null,
  "responseBodyDefinition" : {
    "name" : "",
    "value" : "",
    "description" : "",
    "required" : false,
    "dataType" : "Object",
    "type" : null,
    "defaultValue" : null,
    "validateType" : "",
    "error" : "",
    "expression" : "",
    "children" : [ {
      "name" : "msg",
      "value" : "token 已失效",
      "description" : "",
      "required" : false,
      "dataType" : "String",
      "type" : null,
      "defaultValue" : null,
      "validateType" : "",
      "error" : "",
      "expression" : "",
      "children" : [ ]
    }, {
      "name" : "code",
      "value" : "401",
      "description" : "",
      "required" : false,
      "dataType" : "Integer",
      "type" : null,
      "defaultValue" : null,
      "validateType" : "",
      "error" : "",
      "expression" : "",
      "children" : [ ]
    } ]
  }
}
================================
return ''Hello magic-api''');


--
-- Data for Name: qrtz_blob_triggers; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: qrtz_calendars; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: qrtz_cron_triggers; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.qrtz_cron_triggers (sched_name, trigger_name, trigger_group, cron_expression, time_zone_id) VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', '0/10 * * * * ?', 'Asia/Shanghai');
INSERT INTO public.qrtz_cron_triggers (sched_name, trigger_name, trigger_group, cron_expression, time_zone_id) VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', '0/15 * * * * ?', 'Asia/Shanghai');
INSERT INTO public.qrtz_cron_triggers (sched_name, trigger_name, trigger_group, cron_expression, time_zone_id) VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', '0/20 * * * * ?', 'Asia/Shanghai');


--
-- Data for Name: qrtz_fired_triggers; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: qrtz_job_details; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.qrtz_job_details (sched_name, job_name, job_group, description, job_class_name, is_durable, is_nonconcurrent, is_update_data, requests_recovery, job_data) VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 'com.ruoyi.common.utils.job.QuartzDisallowConcurrentExecution', 'false', 'true', 'false', 'false', '\xaced0005737200156f72672e71756172747a2e4a6f62446174614d61709fb083e8bfa9b0cb020000787200266f72672e71756172747a2e7574696c732e537472696e674b65794469727479466c61674d61708208e8c3fbc55d280200015a0013616c6c6f77735472616e7369656e74446174617872001d6f72672e71756172747a2e7574696c732e4469727479466c61674d617013e62ead28760ace0200025a000564697274794c00036d617074000f4c6a6176612f7574696c2f4d61703b787001737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f4000000000000c7708000000100000000174000f5441534b5f50524f5045525449455373720027636f6d2e72756f79692e70726f6a6563742e6d6f6e69746f722e646f6d61696e2e5379734a6f6200000000000000010200084c000a636f6e63757272656e747400124c6a6176612f6c616e672f537472696e673b4c000e63726f6e45787072657373696f6e71007e00094c000c696e766f6b6554617267657471007e00094c00086a6f6247726f757071007e00094c00056a6f6249647400104c6a6176612f6c616e672f4c6f6e673b4c00076a6f624e616d6571007e00094c000d6d697366697265506f6c69637971007e00094c000673746174757371007e000978720029636f6d2e72756f79692e6672616d65776f726b2e7765622e646f6d61696e2e42617365456e7469747900000000000000010200074c0008637265617465427971007e00094c000a63726561746554696d657400104c6a6176612f7574696c2f446174653b4c0006706172616d7371007e00034c000672656d61726b71007e00094c000b73656172636856616c756571007e00094c0008757064617465427971007e00094c000a75706461746554696d6571007e000c787074000561646d696e7372000e6a6176612e7574696c2e44617465686a81014b5974190300007870770800000179a8519b1878707400007070707400013174000e302f3135202a202a202a202a203f74001572795461736b2e7279506172616d7328277279272974000744454641554c547372000e6a6176612e6c616e672e4c6f6e673b8be490cc8f23df0200014a000576616c7565787200106a6176612e6c616e672e4e756d62657286ac951d0b94e08b02000078700000000000000002740018e7b3bbe7bb9fe9bb98e8aea4efbc88e69c89e58f82efbc8974000133740001317800');
INSERT INTO public.qrtz_job_details (sched_name, job_name, job_group, description, job_class_name, is_durable, is_nonconcurrent, is_update_data, requests_recovery, job_data) VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 'com.ruoyi.common.utils.job.QuartzDisallowConcurrentExecution', 'false', 'true', 'false', 'false', '\xaced0005737200156f72672e71756172747a2e4a6f62446174614d61709fb083e8bfa9b0cb020000787200266f72672e71756172747a2e7574696c732e537472696e674b65794469727479466c61674d61708208e8c3fbc55d280200015a0013616c6c6f77735472616e7369656e74446174617872001d6f72672e71756172747a2e7574696c732e4469727479466c61674d617013e62ead28760ace0200025a000564697274794c00036d617074000f4c6a6176612f7574696c2f4d61703b787001737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f4000000000000c7708000000100000000174000f5441534b5f50524f5045525449455373720027636f6d2e72756f79692e70726f6a6563742e6d6f6e69746f722e646f6d61696e2e5379734a6f6200000000000000010200084c000a636f6e63757272656e747400124c6a6176612f6c616e672f537472696e673b4c000e63726f6e45787072657373696f6e71007e00094c000c696e766f6b6554617267657471007e00094c00086a6f6247726f757071007e00094c00056a6f6249647400104c6a6176612f6c616e672f4c6f6e673b4c00076a6f624e616d6571007e00094c000d6d697366697265506f6c69637971007e00094c000673746174757371007e000978720029636f6d2e72756f79692e6672616d65776f726b2e7765622e646f6d61696e2e42617365456e7469747900000000000000010200074c0008637265617465427971007e00094c000a63726561746554696d657400104c6a6176612f7574696c2f446174653b4c0006706172616d7371007e00034c000672656d61726b71007e00094c000b73656172636856616c756571007e00094c0008757064617465427971007e00094c000a75706461746554696d6571007e000c787074000561646d696e7372000e6a6176612e7574696c2e44617465686a81014b5974190300007870770800000179a8519b1878707400007070707400013174000e302f3230202a202a202a202a203f74003872795461736b2e72794d756c7469706c65506172616d7328277279272c20747275652c20323030304c2c203331362e3530442c203130302974000744454641554c547372000e6a6176612e6c616e672e4c6f6e673b8be490cc8f23df0200014a000576616c7565787200106a6176612e6c616e672e4e756d62657286ac951d0b94e08b02000078700000000000000003740018e7b3bbe7bb9fe9bb98e8aea4efbc88e5a49ae58f82efbc8974000133740001317800');
INSERT INTO public.qrtz_job_details (sched_name, job_name, job_group, description, job_class_name, is_durable, is_nonconcurrent, is_update_data, requests_recovery, job_data) VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 'com.ruoyi.common.utils.job.QuartzDisallowConcurrentExecution', 'false', 'true', 'false', 'false', '\xaced0005737200156f72672e71756172747a2e4a6f62446174614d61709fb083e8bfa9b0cb020000787200266f72672e71756172747a2e7574696c732e537472696e674b65794469727479466c61674d61708208e8c3fbc55d280200015a0013616c6c6f77735472616e7369656e74446174617872001d6f72672e71756172747a2e7574696c732e4469727479466c61674d617013e62ead28760ace0200025a000564697274794c00036d617074000f4c6a6176612f7574696c2f4d61703b787001737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f4000000000000c7708000000100000000174000f5441534b5f50524f5045525449455373720027636f6d2e72756f79692e70726f6a6563742e6d6f6e69746f722e646f6d61696e2e5379734a6f6200000000000000010200084c000a636f6e63757272656e747400124c6a6176612f6c616e672f537472696e673b4c000e63726f6e45787072657373696f6e71007e00094c000c696e766f6b6554617267657471007e00094c00086a6f6247726f757071007e00094c00056a6f6249647400104c6a6176612f6c616e672f4c6f6e673b4c00076a6f624e616d6571007e00094c000d6d697366697265506f6c69637971007e00094c000673746174757371007e000978720029636f6d2e72756f79692e6672616d65776f726b2e7765622e646f6d61696e2e42617365456e7469747900000000000000010200074c0008637265617465427971007e00094c000a63726561746554696d657400104c6a6176612f7574696c2f446174653b4c0006706172616d7371007e00034c000672656d61726b71007e00094c000b73656172636856616c756571007e00094c0008757064617465427971007e00094c000a75706461746554696d6571007e000c787074000561646d696e7372000e6a6176612e7574696c2e44617465686a81014b5974190300007870770800000179a8519b1878707400007070707400013174000e302f3130202a202a202a202a203f74001172795461736b2e72794e6f506172616d7374000744454641554c547372000e6a6176612e6c616e672e4c6f6e673b8be490cc8f23df0200014a000576616c7565787200106a6176612e6c616e672e4e756d62657286ac951d0b94e08b02000078700000000000000001740018e7b3bbe7bb9fe9bb98e8aea4efbc88e697a0e58f82efbc8974000132740001307800');


--
-- Data for Name: qrtz_locks; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.qrtz_locks (sched_name, lock_name) VALUES ('RuoyiScheduler', 'TRIGGER_ACCESS');
INSERT INTO public.qrtz_locks (sched_name, lock_name) VALUES ('RuoyiScheduler', 'STATE_ACCESS');


--
-- Data for Name: qrtz_paused_trigger_grps; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: qrtz_scheduler_state; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.qrtz_scheduler_state (sched_name, instance_name, last_checkin_time, checkin_interval) VALUES ('RuoyiScheduler', 'DESKTOP-LF7U23B1718260059028', 1718263033436, 15000);


--
-- Data for Name: qrtz_simple_triggers; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.qrtz_simple_triggers (sched_name, trigger_name, trigger_group, repeat_count, repeat_interval, times_triggered) VALUES ('RuoyiScheduler', 'MT_51tqg0b4h4kq6', 'DEFAULT', 0, 0, 0);
INSERT INTO public.qrtz_simple_triggers (sched_name, trigger_name, trigger_group, repeat_count, repeat_interval, times_triggered) VALUES ('RuoyiScheduler', 'MT_1cds4o1h960o0', 'DEFAULT', 0, 0, 0);


--
-- Data for Name: qrtz_simprop_triggers; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: qrtz_triggers; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.qrtz_triggers (sched_name, trigger_name, trigger_group, job_name, job_group, description, next_fire_time, prev_fire_time, priority, trigger_state, trigger_type, start_time, end_time, calendar_name, misfire_instr, job_data) VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 1718263022288, -1, 5, 'WAITING', 'CRON', 1718262424000, 0, NULL, 1, '\x');
INSERT INTO public.qrtz_triggers (sched_name, trigger_name, trigger_group, job_name, job_group, description, next_fire_time, prev_fire_time, priority, trigger_state, trigger_type, start_time, end_time, calendar_name, misfire_instr, job_data) VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 1718260065000, -1, 5, 'PAUSED', 'CRON', 1718260060000, 0, NULL, 2, '\x');
INSERT INTO public.qrtz_triggers (sched_name, trigger_name, trigger_group, job_name, job_group, description, next_fire_time, prev_fire_time, priority, trigger_state, trigger_type, start_time, end_time, calendar_name, misfire_instr, job_data) VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 1718260080000, -1, 5, 'PAUSED', 'CRON', 1718260061000, 0, NULL, 2, '\x');
INSERT INTO public.qrtz_triggers (sched_name, trigger_name, trigger_group, job_name, job_group, description, next_fire_time, prev_fire_time, priority, trigger_state, trigger_type, start_time, end_time, calendar_name, misfire_instr, job_data) VALUES ('RuoyiScheduler', 'MT_51tqg0b4h4kq6', 'DEFAULT', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 1718263022628, -1, 5, 'WAITING', 'SIMPLE', 1718262574037, 0, NULL, 0, '\xaced0005737200156f72672e71756172747a2e4a6f62446174614d61709fb083e8bfa9b0cb020000787200266f72672e71756172747a2e7574696c732e537472696e674b65794469727479466c61674d61708208e8c3fbc55d280200015a0013616c6c6f77735472616e7369656e74446174617872001d6f72672e71756172747a2e7574696c732e4469727479466c61674d617013e62ead28760ace0200025a000564697274794c00036d617074000f4c6a6176612f7574696c2f4d61703b787001737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f4000000000000c7708000000100000000174000f5441534b5f50524f5045525449455373720027636f6d2e72756f79692e70726f6a6563742e6d6f6e69746f722e646f6d61696e2e5379734a6f6200000000000000010200084c000a636f6e63757272656e747400124c6a6176612f6c616e672f537472696e673b4c000e63726f6e45787072657373696f6e71007e00094c000c696e766f6b6554617267657471007e00094c00086a6f6247726f757071007e00094c00056a6f6249647400104c6a6176612f6c616e672f4c6f6e673b4c00076a6f624e616d6571007e00094c000d6d697366697265506f6c69637971007e00094c000673746174757371007e000978720029636f6d2e72756f79692e6672616d65776f726b2e7765622e646f6d61696e2e42617365456e7469747900000000000000010200074c0008637265617465427971007e00094c000a63726561746554696d657400104c6a6176612f7574696c2f446174653b4c0006706172616d7371007e00034c000672656d61726b71007e00094c000b73656172636856616c756571007e00094c0008757064617465427971007e00094c000a75706461746554696d6571007e000c787074000561646d696e7372000e6a6176612e7574696c2e44617465686a81014b5974190300007870770800000179a8519b1878707400007070707400013174000e302f3130202a202a202a202a203f74001172795461736b2e72794e6f506172616d7374000744454641554c547372000e6a6176612e6c616e672e4c6f6e673b8be490cc8f23df0200014a000576616c7565787200106a6176612e6c616e672e4e756d62657286ac951d0b94e08b02000078700000000000000001740018e7b3bbe7bb9fe9bb98e8aea4efbc88e697a0e58f82efbc8974000132740001307800');
INSERT INTO public.qrtz_triggers (sched_name, trigger_name, trigger_group, job_name, job_group, description, next_fire_time, prev_fire_time, priority, trigger_state, trigger_type, start_time, end_time, calendar_name, misfire_instr, job_data) VALUES ('RuoyiScheduler', 'MT_1cds4o1h960o0', 'DEFAULT', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 1718263034277, -1, 5, 'WAITING', 'SIMPLE', 1718262899162, 0, NULL, 0, '\xaced0005737200156f72672e71756172747a2e4a6f62446174614d61709fb083e8bfa9b0cb020000787200266f72672e71756172747a2e7574696c732e537472696e674b65794469727479466c61674d61708208e8c3fbc55d280200015a0013616c6c6f77735472616e7369656e74446174617872001d6f72672e71756172747a2e7574696c732e4469727479466c61674d617013e62ead28760ace0200025a000564697274794c00036d617074000f4c6a6176612f7574696c2f4d61703b787001737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f4000000000000c7708000000100000000174000f5441534b5f50524f5045525449455373720027636f6d2e72756f79692e70726f6a6563742e6d6f6e69746f722e646f6d61696e2e5379734a6f6200000000000000010200084c000a636f6e63757272656e747400124c6a6176612f6c616e672f537472696e673b4c000e63726f6e45787072657373696f6e71007e00094c000c696e766f6b6554617267657471007e00094c00086a6f6247726f757071007e00094c00056a6f6249647400104c6a6176612f6c616e672f4c6f6e673b4c00076a6f624e616d6571007e00094c000d6d697366697265506f6c69637971007e00094c000673746174757371007e000978720029636f6d2e72756f79692e6672616d65776f726b2e7765622e646f6d61696e2e42617365456e7469747900000000000000010200074c0008637265617465427971007e00094c000a63726561746554696d657400104c6a6176612f7574696c2f446174653b4c0006706172616d7371007e00034c000672656d61726b71007e00094c000b73656172636856616c756571007e00094c0008757064617465427971007e00094c000a75706461746554696d6571007e000c787074000561646d696e7372000e6a6176612e7574696c2e44617465686a81014b5974190300007870770800000179a8519b1878707400007070707400013174000e302f3130202a202a202a202a203f74001172795461736b2e72794e6f506172616d7374000744454641554c547372000e6a6176612e6c616e672e4c6f6e673b8be490cc8f23df0200014a000576616c7565787200106a6176612e6c616e672e4e756d62657286ac951d0b94e08b02000078700000000000000001740018e7b3bbe7bb9fe9bb98e8aea4efbc88e697a0e58f82efbc8974000132740001307800');


--
-- Data for Name: sys_config; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.sys_config (config_id, config_name, config_key, config_value, config_type, create_by, create_time, update_by, update_time, remark) VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2021-05-26 18:56:31', 'admin', '2021-05-27 09:07:43.532263', '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO public.sys_config (config_id, config_name, config_key, config_value, config_type, create_by, create_time, update_by, update_time, remark) VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2021-05-26 18:56:31', 'admin', '2021-05-27 10:15:52.394492', '初始化密码 123456');
INSERT INTO public.sys_config (config_id, config_name, config_key, config_value, config_type, create_by, create_time, update_by, update_time, remark) VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2021-05-26 18:56:31', 'admin', NULL, '深色主题theme-dark，浅色主题theme-light');
INSERT INTO public.sys_config (config_id, config_name, config_key, config_value, config_type, create_by, create_time, update_by, update_time, remark) VALUES (4, '账号自助-验证码开关', 'sys.account.captchaEnabled', 'true', 'Y', 'admin', '2024-05-05 20:02:00.031971', 'admin', NULL, '是否开启验证码功能（true开启，false关闭）');
INSERT INTO public.sys_config (config_id, config_name, config_key, config_value, config_type, create_by, create_time, update_by, update_time, remark) VALUES (5, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2024-05-05 20:02:00.031971', 'admin', NULL, '是否开启注册用户功能（true开启，false关闭）');
INSERT INTO public.sys_config (config_id, config_name, config_key, config_value, config_type, create_by, create_time, update_by, update_time, remark) VALUES (6, '用户登录-黑名单列表', 'sys.login.blackIPList', '', 'Y', 'admin', '2024-05-05 20:04:30', '', NULL, '设置登录IP黑名单限制，多个匹配项以;分隔，支持匹配（*通配、网段）');


--
-- Data for Name: sys_dept; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.sys_dept (dept_id, parent_id, ancestors, dept_name, order_num, leader, phone, email, status, del_flag, create_by, create_time, update_by, update_time) VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-26 18:56:27', '', NULL);
INSERT INTO public.sys_dept (dept_id, parent_id, ancestors, dept_name, order_num, leader, phone, email, status, del_flag, create_by, create_time, update_by, update_time) VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-26 18:56:28', '', NULL);
INSERT INTO public.sys_dept (dept_id, parent_id, ancestors, dept_name, order_num, leader, phone, email, status, del_flag, create_by, create_time, update_by, update_time) VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-26 18:56:28', '', NULL);
INSERT INTO public.sys_dept (dept_id, parent_id, ancestors, dept_name, order_num, leader, phone, email, status, del_flag, create_by, create_time, update_by, update_time) VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-26 18:56:27', '', NULL);
INSERT INTO public.sys_dept (dept_id, parent_id, ancestors, dept_name, order_num, leader, phone, email, status, del_flag, create_by, create_time, update_by, update_time) VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-26 18:56:27', '', NULL);
INSERT INTO public.sys_dept (dept_id, parent_id, ancestors, dept_name, order_num, leader, phone, email, status, del_flag, create_by, create_time, update_by, update_time) VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-26 18:56:28', '', NULL);
INSERT INTO public.sys_dept (dept_id, parent_id, ancestors, dept_name, order_num, leader, phone, email, status, del_flag, create_by, create_time, update_by, update_time) VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-26 18:56:28', '', NULL);
INSERT INTO public.sys_dept (dept_id, parent_id, ancestors, dept_name, order_num, leader, phone, email, status, del_flag, create_by, create_time, update_by, update_time) VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-26 18:56:27', 'admin', '2024-05-15 21:56:01.547175');
INSERT INTO public.sys_dept (dept_id, parent_id, ancestors, dept_name, order_num, leader, phone, email, status, del_flag, create_by, create_time, update_by, update_time) VALUES (101, 100, '0,100', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-26 18:56:27', 'admin', '2024-05-13 22:49:03.849264');
INSERT INTO public.sys_dept (dept_id, parent_id, ancestors, dept_name, order_num, leader, phone, email, status, del_flag, create_by, create_time, update_by, update_time) VALUES (100, 0, '0', '若依科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-26 18:56:27', 'admin', '2021-05-27 10:00:30.143076');


--
-- Data for Name: sys_dict_data; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '性别男');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '性别女');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '性别未知');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '显示菜单');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '隐藏菜单');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '正常状态');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '停用状态');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '正常状态');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '停用状态');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '默认分组');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '系统分组');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '系统默认是');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '系统默认否');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '通知');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '公告');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '正常状态');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '关闭状态');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (29, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '停用状态');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (28, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '正常状态');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (27, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '清空操作');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (26, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '生成操作');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (25, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '强退操作');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (24, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '导入操作');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (23, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '导出操作');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (22, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '授权操作');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (21, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '删除操作');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (20, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '修改操作');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (19, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '新增操作');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (18, 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2024-05-05 20:04:30', '', NULL, '其他操作');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (559013316943877, 1, '应用', '1', 'sys_notice_platform', NULL, 'default', 'N', '0', '', '2024-06-17 16:51:02.343704', '', '2024-06-17 22:56:51.93334', '应用端');
INSERT INTO public.sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) VALUES (559013380935685, 2, '后端管理', '2', 'sys_notice_platform', NULL, 'default', 'N', '0', '', '2024-06-17 16:51:17.965978', '', '2024-06-17 22:56:54.643204', '后端管理');


--
-- Data for Name: sys_dict_type; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2021-05-26 18:56:30', '', NULL, '菜单状态列表');
INSERT INTO public.sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2021-05-26 18:56:30', '', NULL, '系统开关列表');
INSERT INTO public.sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2021-05-26 18:56:30', '', NULL, '任务状态列表');
INSERT INTO public.sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2021-05-26 18:56:30', '', NULL, '任务分组列表');
INSERT INTO public.sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2021-05-26 18:56:30', '', NULL, '系统是否列表');
INSERT INTO public.sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2021-05-26 18:56:30', '', NULL, '通知类型列表');
INSERT INTO public.sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2021-05-26 18:56:30', '', NULL, '通知状态列表');
INSERT INTO public.sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2021-05-26 18:56:30', '', NULL, '操作类型列表');
INSERT INTO public.sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2021-05-26 18:56:30', '', NULL, '登录状态列表');
INSERT INTO public.sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2021-05-26 18:56:30', 'admin', '2021-05-27 10:07:12.015926', '用户性别列表');
INSERT INTO public.sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) VALUES (559012689203205, '通知显示平台', 'sys_notice_platform', '0', '', '2024-06-17 16:48:29.088128', '', '2024-06-17 22:56:45.552196', '通知显示平台');


--
-- Data for Name: sys_job; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.sys_job (job_id, job_name, job_group, invoke_target, cron_expression, misfire_policy, concurrent, status, create_by, create_time, update_by, update_time, remark) VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(''ry'', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2021-05-26 18:56:31', '', NULL, '');
INSERT INTO public.sys_job (job_id, job_name, job_group, invoke_target, cron_expression, misfire_policy, concurrent, status, create_by, create_time, update_by, update_time, remark) VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '2', '1', '1', 'admin', '2021-05-26 18:56:31', '', '2024-06-14 23:05:06.929954', '');
INSERT INTO public.sys_job (job_id, job_name, job_group, invoke_target, cron_expression, misfire_policy, concurrent, status, create_by, create_time, update_by, update_time, remark) VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(''ry'')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2021-05-26 18:56:31', 'admin', '2024-06-27 23:02:49.16423', '');


--
-- Data for Name: sys_job_log; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.sys_job_log (job_log_id, job_name, job_group, invoke_target, job_message, status, exception_info, create_time) VALUES (558043056668741, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '系统默认（无参） 总共耗时：9毫秒', '0', '', '2024-06-14 23:02:59.287147');
INSERT INTO public.sys_job_log (job_log_id, job_name, job_group, invoke_target, job_message, status, exception_info, create_time) VALUES (558043086901253, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '系统默认（无参） 总共耗时：0毫秒', '0', '', '2024-06-14 23:03:06.646331');
INSERT INTO public.sys_job_log (job_log_id, job_name, job_group, invoke_target, job_message, status, exception_info, create_time) VALUES (558043127853061, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '系统默认（无参） 总共耗时：0毫秒', '0', '', '2024-06-14 23:03:16.641573');
INSERT INTO public.sys_job_log (job_log_id, job_name, job_group, invoke_target, job_message, status, exception_info, create_time) VALUES (558043168808965, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '系统默认（无参） 总共耗时：0毫秒', '0', '', '2024-06-14 23:03:26.639224');
INSERT INTO public.sys_job_log (job_log_id, job_name, job_group, invoke_target, job_message, status, exception_info, create_time) VALUES (558043209748485, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '系统默认（无参） 总共耗时：0毫秒', '0', '', '2024-06-14 23:03:36.635102');
INSERT INTO public.sys_job_log (job_log_id, job_name, job_group, invoke_target, job_message, status, exception_info, create_time) VALUES (558043250757637, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '系统默认（无参） 总共耗时：0毫秒', '0', '', '2024-06-14 23:03:46.646742');
INSERT INTO public.sys_job_log (job_log_id, job_name, job_group, invoke_target, job_message, status, exception_info, create_time) VALUES (558043291676677, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '系统默认（无参） 总共耗时：0毫秒', '0', '', '2024-06-14 23:03:56.638069');
INSERT INTO public.sys_job_log (job_log_id, job_name, job_group, invoke_target, job_message, status, exception_info, create_time) VALUES (558043332673541, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '系统默认（无参） 总共耗时：0毫秒', '0', '', '2024-06-14 23:04:06.646708');
INSERT INTO public.sys_job_log (job_log_id, job_name, job_group, invoke_target, job_message, status, exception_info, create_time) VALUES (558043373604869, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '系统默认（无参） 总共耗时：0毫秒', '0', '', '2024-06-14 23:04:16.639184');
INSERT INTO public.sys_job_log (job_log_id, job_name, job_group, invoke_target, job_message, status, exception_info, create_time) VALUES (558043414548485, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '系统默认（无参） 总共耗时：0毫秒', '0', '', '2024-06-14 23:04:26.637221');
INSERT INTO public.sys_job_log (job_log_id, job_name, job_group, invoke_target, job_message, status, exception_info, create_time) VALUES (558043455520773, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '系统默认（无参） 总共耗时：0毫秒', '0', '', '2024-06-14 23:04:36.640363');
INSERT INTO public.sys_job_log (job_log_id, job_name, job_group, invoke_target, job_message, status, exception_info, create_time) VALUES (558043496501253, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '系统默认（无参） 总共耗时：0毫秒', '0', '', '2024-06-14 23:04:46.645407');
INSERT INTO public.sys_job_log (job_log_id, job_name, job_group, invoke_target, job_message, status, exception_info, create_time) VALUES (558043537481733, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '系统默认（无参） 总共耗时：0毫秒', '0', '', '2024-06-14 23:04:56.64784');
INSERT INTO public.sys_job_log (job_log_id, job_name, job_group, invoke_target, job_message, status, exception_info, create_time) VALUES (562643707731973, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '系统默认（无参） 总共耗时：1毫秒', '0', '', '2024-06-27 23:02:59.919357');


--
-- Data for Name: sys_logininfor; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sys_menu; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1, '系统管理', 0, 1, 'system', NULL, '', 1, 0, 'M', '0', '0', '', 'system', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '系统管理目录', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (2, '系统监控', 0, 2, 'monitor', NULL, '', 1, 0, 'M', '0', '0', '', 'monitor', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '系统监控目录', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (3, '系统工具', 0, 3, 'tool', NULL, '', 1, 0, 'M', '0', '0', '', 'tool', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '系统工具目录', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', '', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '用户管理菜单', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', '', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '角色管理菜单', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', '', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '菜单管理菜单', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', '', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '部门管理菜单', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', '', 1, 0, 'C', '0', '0', 'system:post:list', 'post', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '岗位管理菜单', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', '', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '字典管理菜单', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', '', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '参数设置菜单', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', '', 1, 0, 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '通知公告菜单', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (108, '日志管理', 1, 9, 'log', '', '', 1, 0, 'M', '0', '0', '', 'log', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '日志管理菜单', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', '', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '在线用户菜单', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', '', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '定时任务菜单', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (111, '数据监控', 2, 3, 'druid', 'monitor/druid/index', '', 1, 0, 'C', '0', '0', 'monitor:druid:list', 'druid', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '数据监控菜单', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (112, '服务监控', 2, 4, 'server', 'monitor/server/index', '', 1, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '服务监控菜单', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (113, '缓存监控', 2, 5, 'cache', 'monitor/cache/index', '', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '缓存监控菜单', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (114, '缓存列表', 2, 6, 'cacheList', 'monitor/cache/list', '', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis-list', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '缓存列表菜单', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (115, '表单构建', 3, 1, 'build', 'tool/build/index', '', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '表单构建菜单', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (116, '代码生成', 3, 2, 'gen', 'tool/gen/index', '', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '代码生成菜单', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (117, '系统接口', 3, 3, 'swagger', 'tool/swagger/index', '', 1, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '系统接口菜单', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (500, '操作日志', 108, 1, 'operlog', 'monitor/operlog/index', '', 1, 0, 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '操作日志菜单', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (501, '登录日志', 108, 2, 'logininfor', 'monitor/logininfor/index', '', 1, 0, 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '登录日志菜单', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1000, '用户查询', 100, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1001, '用户新增', 100, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1002, '用户修改', 100, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1003, '用户删除', 100, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1004, '用户导出', 100, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1005, '用户导入', 100, 6, '', '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1006, '重置密码', 100, 7, '', '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1007, '角色查询', 101, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1008, '角色新增', 101, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1009, '角色修改', 101, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1010, '角色删除', 101, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1011, '角色导出', 101, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1012, '菜单查询', 102, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1013, '菜单新增', 102, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1014, '菜单修改', 102, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1015, '菜单删除', 102, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1016, '部门查询', 103, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1017, '部门新增', 103, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1018, '部门修改', 103, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1019, '部门删除', 103, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1020, '岗位查询', 104, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1021, '岗位新增', 104, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1022, '岗位修改', 104, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1023, '岗位删除', 104, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1024, '岗位导出', 104, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1025, '字典查询', 105, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1026, '字典新增', 105, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1027, '字典修改', 105, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1028, '字典删除', 105, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1029, '字典导出', 105, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1030, '参数查询', 106, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1031, '参数新增', 106, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1032, '参数修改', 106, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1033, '参数删除', 106, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1034, '参数导出', 106, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1035, '公告查询', 107, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1036, '公告新增', 107, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1037, '公告修改', 107, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1038, '公告删除', 107, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1039, '操作查询', 500, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1040, '操作删除', 500, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1041, '日志导出', 500, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1042, '登录查询', 501, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1043, '登录删除', 501, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1044, '日志导出', 501, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1045, '账户解锁', 501, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:unlock', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1046, '在线查询', 109, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1047, '批量强退', 109, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1048, '单条强退', 109, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1049, '任务查询', 110, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1050, '任务新增', 110, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1051, '任务修改', 110, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1052, '任务删除', 110, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1053, '状态修改', 110, 5, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1054, '任务导出', 110, 6, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1055, '生成查询', 116, 1, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1056, '生成修改', 116, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1057, '生成删除', 116, 3, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1058, '导入代码', 116, 4, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1059, '预览代码', 116, 5, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1060, '生成代码', 116, 6, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2024-05-05 20:02:00.031971', '', NULL, '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (2000, '文件管理', 0, 0, 'cms', '', NULL, 1, 0, 'M', '1', '0', '', 'education', 'admin', '2023-06-25 16:02:40', 'admin', '2023-06-27 22:31:08', '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (2001, '文件夹列表', 2000, 1, 'folder', '', NULL, 1, 0, 'F', '0', '0', 'cms:folder:list', '#', 'admin', '2023-06-25 16:09:00', 'admin', '2023-06-27 22:25:56', '文件夹菜单', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (2002, '文件夹查询', 2000, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:folder:query', '#', 'admin', '2023-06-25 16:09:00', 'admin', '2023-06-27 22:28:02', '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (2003, '文件夹新增', 2000, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:folder:add', '#', 'admin', '2023-06-25 16:09:00', 'admin', '2023-06-27 22:28:11', '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (2004, '文件夹修改', 2000, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:folder:edit', '#', 'admin', '2023-06-25 16:09:00', 'admin', '2023-06-27 22:28:18', '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (2005, '文件夹删除', 2000, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:folder:remove', '#', 'admin', '2023-06-25 16:09:00', 'admin', '2023-06-27 22:28:24', '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (2006, '文件夹导出', 2000, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:folder:export', '#', 'admin', '2023-06-25 16:09:00', 'admin', '2023-06-27 22:28:41', '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (2007, '文件列表', 2000, 1, 'docs', '', NULL, 1, 0, 'F', '0', '0', 'cms:docs:list', '#', 'admin', '2023-06-25 23:08:05', 'admin', '2023-06-27 22:30:17', '文件菜单', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (2008, '文件查询', 2000, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:docs:query', '#', 'admin', '2023-06-25 23:08:05', 'admin', '2023-06-27 22:29:25', '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (2009, '文件新增', 2000, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:docs:add', '#', 'admin', '2023-06-25 23:08:05', 'admin', '2023-06-27 22:29:30', '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (2010, '文件修改', 2000, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:docs:edit', '#', 'admin', '2023-06-25 23:08:05', 'admin', '2023-06-27 22:29:35', '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (2011, '文件删除', 2000, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:docs:remove', '#', 'admin', '2023-06-25 23:08:05', 'admin', '2023-06-27 22:29:40', '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (2012, '文件导出', 2000, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:docs:export', '#', 'admin', '2023-06-25 23:08:05', 'admin', '2023-06-27 22:29:46', '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (2013, '世界地图', 0, 5, 'map', 'cms/map/index', NULL, 1, 0, 'C', '0', '0', '', 'international', 'admin', '2023-07-05 18:11:04', 'admin', '2023-07-05 23:52:41', '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1793297310835277824, '图表', 0, 6, 'echarts_demo', 'index_v1', NULL, 1, 0, 'C', '0', '0', '', 'chart', 'admin', '2024-05-22 23:06:11.509519', 'admin', '2024-05-22 23:40:39.434788', '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (4, '若依官网', 0, 4, 'http://ruoyi.vip', NULL, '', 0, 0, 'M', '0', '1', '', 'guide', 'admin', '2024-05-05 20:02:00.031971', 'admin', '2024-05-22 23:28:52.856795', '若依官网地址', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1793306891313479680, '若依首页', 0, 7, 'index_ruoyi', 'index', NULL, 1, 0, 'C', '0', '0', '', 'guide', 'admin', '2024-05-22 23:44:15.704352', 'admin', '2024-05-22 23:45:00.972084', '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (553323397529605, '应用监控', 2, 7, 'monitoring', 'monitor/monitoring/index', NULL, 1, 0, 'C', '0', '0', 'monitor:monitoring:list', 'server', '', '2024-06-01 14:58:41.984881', '', '2024-06-14 23:31:12.865908', '', '');
INSERT INTO public.sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark, route_name) VALUES (1700, '在线接口', 3, 4, 'magicapi', 'tool/magicapi/index', NULL, 1, 0, 'C', '0', '0', 'tool:magicapi:list', 'code', 'admin', '2022-07-28 21:42:04', '', '2024-06-14 23:25:29.95848', '', '');


--
-- Data for Name: sys_notice; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.sys_notice (notice_id, notice_title, notice_type, notice_content, status, create_by, create_time, update_by, update_time, remark, content_type, notice_content_markdown, platform) VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', '<div data-inline-code-theme="red" data-code-block-theme="tomorrow-night"><h1 data-lines="1" data-sign="f3b04c868f6084825cdf0a05af8ad027" id="222" class="cherry-highlight-line"><a href="#222" class="anchor"></a>222</h1></div>', '0', 'admin', '2022-07-28 16:00:45', 'admin', '2024-06-17 16:20:27.359318', '管理员', '2', '# 222', '1');
INSERT INTO public.sys_notice (notice_id, notice_title, notice_type, notice_content, status, create_by, create_time, update_by, update_time, remark, content_type, notice_content_markdown, platform) VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', '<div data-inline-code-theme="red" data-code-block-theme="tomorrow-night"><h1 data-lines="1" data-sign="7c0bed222793d956e16311983d7ef818" id="111" class="cherry-highlight-line"><a href="#111" class="anchor"></a>111</h1><p data-lines="1" data-type="p" data-sign="bcbe3365e6ac95ea2c0343a2395834dd1">222</p></div>', '1', 'admin', '2022-07-28 16:00:45', 'admin', '2024-06-17 16:38:53.221241', '管理员', '2', '# 111
222', '2');


--
-- Data for Name: sys_oper_log; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sys_post; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.sys_post (post_id, post_code, post_name, post_sort, status, create_by, create_time, update_by, update_time, remark) VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2021-05-26 18:56:28', '', NULL, '');
INSERT INTO public.sys_post (post_id, post_code, post_name, post_sort, status, create_by, create_time, update_by, update_time, remark) VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2021-05-26 18:56:28', '', NULL, '');
INSERT INTO public.sys_post (post_id, post_code, post_name, post_sort, status, create_by, create_time, update_by, update_time, remark) VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2021-05-26 18:56:28', '', NULL, '');
INSERT INTO public.sys_post (post_id, post_code, post_name, post_sort, status, create_by, create_time, update_by, update_time, remark) VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2021-05-26 18:56:28', 'admin', '2021-05-27 09:07:17.160973', '');
INSERT INTO public.sys_post (post_id, post_code, post_name, post_sort, status, create_by, create_time, update_by, update_time, remark) VALUES (559644304175109, 'wechat', '微信小程序', 5, '0', 'admin', '2024-06-19 11:38:32.112689', '', NULL, '微信小程序用户专用');


--
-- Data for Name: sys_role; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.sys_role (role_id, role_name, role_key, role_sort, data_scope, menu_check_strictly, dept_check_strictly, status, del_flag, create_by, create_time, update_by, update_time, remark) VALUES (1, '超级管理员', 'admin', 1, '1', true, true, '0', '0', 'admin', '2021-05-26 18:56:28', '', NULL, '超级管理员');
INSERT INTO public.sys_role (role_id, role_name, role_key, role_sort, data_scope, menu_check_strictly, dept_check_strictly, status, del_flag, create_by, create_time, update_by, update_time, remark) VALUES (1790255266126626816, '测试', 'user:role:role', 0, '1', true, true, '0', '2', 'admin', '2024-05-14 13:38:11.276337', 'admin', '2024-05-14 13:38:35.03793', 'ce');
INSERT INTO public.sys_role (role_id, role_name, role_key, role_sort, data_scope, menu_check_strictly, dept_check_strictly, status, del_flag, create_by, create_time, update_by, update_time, remark) VALUES (2, '普通角色', 'common', 2, '2', false, false, '0', '0', 'admin', '2021-05-26 18:56:28', 'admin', '2024-06-14 23:43:48.842868', '普通角色');


--
-- Data for Name: sys_role_dept; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.sys_role_dept (role_id, dept_id) VALUES (2, 100);
INSERT INTO public.sys_role_dept (role_id, dept_id) VALUES (2, 101);
INSERT INTO public.sys_role_dept (role_id, dept_id) VALUES (2, 105);


--
-- Data for Name: sys_role_menu; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 100);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1000);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1001);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1002);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1003);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1004);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1005);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1006);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 101);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1007);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1008);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1009);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1010);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1011);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 102);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1012);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1013);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1014);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1015);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 103);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1016);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1017);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1018);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1019);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 104);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1020);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1021);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1022);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1023);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1024);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 105);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1025);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1026);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1027);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1028);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1029);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 106);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1030);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1031);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1032);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1033);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1034);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 107);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1035);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1036);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1037);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1038);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 108);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 500);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1039);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1040);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1041);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 501);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1042);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1043);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1044);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1045);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 2);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 109);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1046);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1047);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1048);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 110);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1049);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1050);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1051);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1052);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1053);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1054);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 111);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 112);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 113);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 114);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 3);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 115);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 116);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1055);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1056);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1057);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1058);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1059);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 1060);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 117);
INSERT INTO public.sys_role_menu (role_id, menu_id) VALUES (2, 4);


--
-- Data for Name: sys_user; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.sys_user (user_id, dept_id, user_name, nick_name, user_type, email, phonenumber, sex, avatar, password, status, del_flag, login_ip, login_date, create_by, create_time, update_by, update_time, remark, openid, init_nick_name, init_avatar) VALUES (1790255613083648000, 100, '测试', '测试', '00', NULL, '13842636942', '0', NULL, '$2a$10$Jl0F7T3h.jg6HFAR5uzKce0SnJUA00ud17XcyfJtaXR/cYno9XTlS', '0', '0', NULL, NULL, 'admin', '2024-05-14 13:39:33.995463', NULL, NULL, '测试', NULL, NULL, NULL);
INSERT INTO public.sys_user (user_id, dept_id, user_name, nick_name, user_type, email, phonenumber, sex, avatar, password, status, del_flag, login_ip, login_date, create_by, create_time, update_by, update_time, remark, openid, init_nick_name, init_avatar) VALUES (2, 105, 'ry', '若依1', '00', 'ry@qq.com', '15666666667', '0', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2021-05-26 18:56:28', 'admin', '2021-05-26 18:56:28', 'admin', '2024-06-13 14:41:21.902101', '测试员', NULL, NULL, NULL);
INSERT INTO public.sys_user (user_id, dept_id, user_name, nick_name, user_type, email, phonenumber, sex, avatar, password, status, del_flag, login_ip, login_date, create_by, create_time, update_by, update_time, remark, openid, init_nick_name, init_avatar) VALUES (1792228715418091520, 1, '43666669290', '微信用户waiivwleo', '00', '', '', '2', 'https://mmbiz.qpic.cn/mmbiz/icTdbqWNOwNRna42FI242Lcia07jQodd2FJGIYQfG0LAJGFxM4FbnQP6yfMxBgJ0F3YRqJCJ1aPAK2dQagdusBZg/0', '', '0', '0', '127.0.0.1', '2024-05-20 00:20:11.214', '', '2024-05-20 00:19:58.44314', '', '2024-05-20 00:20:11.289819', NULL, '', 0, 0);
INSERT INTO public.sys_user (user_id, dept_id, user_name, nick_name, user_type, email, phonenumber, sex, avatar, password, status, del_flag, login_ip, login_date, create_by, create_time, update_by, update_time, remark, openid, init_nick_name, init_avatar) VALUES (1, 103, 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', '', '$2a$10$T4Bd4MafZbSsVdvfbRKmoe5O26b2Sj95ze1uSGCdnhNpveI0W3mgG', '0', '0', '127.0.0.1', '2024-06-28 15:18:23.215', 'admin', '2021-05-26 18:56:28', 'admin', '2024-06-28 15:18:21.341996', '管理员', NULL, NULL, NULL);


--
-- Data for Name: sys_user_post; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.sys_user_post (user_id, post_id) VALUES (1, 1);
INSERT INTO public.sys_user_post (user_id, post_id) VALUES (1790255613083648000, 2);
INSERT INTO public.sys_user_post (user_id, post_id) VALUES (1792228715418091520, 1);
INSERT INTO public.sys_user_post (user_id, post_id) VALUES (2, 2);


--
-- Data for Name: sys_user_role; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.sys_user_role (user_id, role_id) VALUES (1, 1);
INSERT INTO public.sys_user_role (user_id, role_id) VALUES (1790255613083648000, 2);
INSERT INTO public.sys_user_role (user_id, role_id) VALUES (1792228715418091520, 1);
INSERT INTO public.sys_user_role (user_id, role_id) VALUES (2, 2);


--
-- Name: cms_docs cms_docs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cms_docs
    ADD CONSTRAINT cms_docs_pkey PRIMARY KEY (docs_id);


--
-- Name: cms_folder cms_folder_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cms_folder
    ADD CONSTRAINT cms_folder_pkey PRIMARY KEY (folder_id);


--
-- Name: flyway_schema_history flyway_schema_history_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.flyway_schema_history
    ADD CONSTRAINT flyway_schema_history_pk PRIMARY KEY (installed_rank);


--
-- Name: gen_table_column gen_table_column_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gen_table_column
    ADD CONSTRAINT gen_table_column_pkey PRIMARY KEY (column_id);


--
-- Name: gen_table gen_table_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gen_table
    ADD CONSTRAINT gen_table_pkey PRIMARY KEY (table_id);


--
-- Name: magic_api_backup magic_api_backup_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.magic_api_backup
    ADD CONSTRAINT magic_api_backup_pkey PRIMARY KEY (id, create_date);


--
-- Name: magic_api_file magic_api_file_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.magic_api_file
    ADD CONSTRAINT magic_api_file_pkey PRIMARY KEY (file_path);


--
-- Name: qrtz_blob_triggers qrtz_blob_triggers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qrtz_blob_triggers
    ADD CONSTRAINT qrtz_blob_triggers_pkey PRIMARY KEY (sched_name, trigger_name, trigger_group);


--
-- Name: qrtz_calendars qrtz_calendars_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qrtz_calendars
    ADD CONSTRAINT qrtz_calendars_pkey PRIMARY KEY (sched_name, calendar_name);


--
-- Name: qrtz_cron_triggers qrtz_cron_triggers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qrtz_cron_triggers
    ADD CONSTRAINT qrtz_cron_triggers_pkey PRIMARY KEY (sched_name, trigger_name, trigger_group);


--
-- Name: qrtz_fired_triggers qrtz_fired_triggers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qrtz_fired_triggers
    ADD CONSTRAINT qrtz_fired_triggers_pkey PRIMARY KEY (sched_name, entry_id);


--
-- Name: qrtz_job_details qrtz_job_details_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qrtz_job_details
    ADD CONSTRAINT qrtz_job_details_pkey PRIMARY KEY (sched_name, job_name, job_group);


--
-- Name: qrtz_locks qrtz_locks_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qrtz_locks
    ADD CONSTRAINT qrtz_locks_pkey PRIMARY KEY (sched_name, lock_name);


--
-- Name: qrtz_paused_trigger_grps qrtz_paused_trigger_grps_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qrtz_paused_trigger_grps
    ADD CONSTRAINT qrtz_paused_trigger_grps_pkey PRIMARY KEY (sched_name, trigger_group);


--
-- Name: qrtz_scheduler_state qrtz_scheduler_state_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qrtz_scheduler_state
    ADD CONSTRAINT qrtz_scheduler_state_pkey PRIMARY KEY (sched_name, instance_name);


--
-- Name: qrtz_simple_triggers qrtz_simple_triggers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qrtz_simple_triggers
    ADD CONSTRAINT qrtz_simple_triggers_pkey PRIMARY KEY (sched_name, trigger_name, trigger_group);


--
-- Name: qrtz_simprop_triggers qrtz_simprop_triggers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qrtz_simprop_triggers
    ADD CONSTRAINT qrtz_simprop_triggers_pkey PRIMARY KEY (sched_name, trigger_name, trigger_group);


--
-- Name: qrtz_triggers qrtz_triggers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qrtz_triggers
    ADD CONSTRAINT qrtz_triggers_pkey PRIMARY KEY (sched_name, trigger_name, trigger_group);


--
-- Name: sys_config sys_config_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_config
    ADD CONSTRAINT sys_config_pkey PRIMARY KEY (config_id);


--
-- Name: sys_dept sys_dept_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_dept
    ADD CONSTRAINT sys_dept_pkey PRIMARY KEY (dept_id);


--
-- Name: sys_dict_data sys_dict_data_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_dict_data
    ADD CONSTRAINT sys_dict_data_pkey PRIMARY KEY (dict_code);


--
-- Name: sys_dict_type sys_dict_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_dict_type
    ADD CONSTRAINT sys_dict_type_pkey PRIMARY KEY (dict_id);


--
-- Name: sys_job_log sys_job_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_job_log
    ADD CONSTRAINT sys_job_log_pkey PRIMARY KEY (job_log_id);


--
-- Name: sys_job sys_job_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_job
    ADD CONSTRAINT sys_job_pkey PRIMARY KEY (job_id, job_name, job_group);


--
-- Name: sys_logininfor sys_logininfor_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_logininfor
    ADD CONSTRAINT sys_logininfor_pkey PRIMARY KEY (info_id);


--
-- Name: sys_menu sys_menu_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_menu
    ADD CONSTRAINT sys_menu_pkey PRIMARY KEY (menu_id);


--
-- Name: sys_notice sys_notice_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_notice
    ADD CONSTRAINT sys_notice_pkey PRIMARY KEY (notice_id);


--
-- Name: sys_oper_log sys_oper_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_oper_log
    ADD CONSTRAINT sys_oper_log_pkey PRIMARY KEY (oper_id);


--
-- Name: sys_post sys_post_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_post
    ADD CONSTRAINT sys_post_pkey PRIMARY KEY (post_id);


--
-- Name: sys_role_dept sys_role_dept_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_role_dept
    ADD CONSTRAINT sys_role_dept_pkey PRIMARY KEY (role_id, dept_id);


--
-- Name: sys_role_menu sys_role_menu_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_role_menu
    ADD CONSTRAINT sys_role_menu_pkey PRIMARY KEY (role_id, menu_id);


--
-- Name: sys_role sys_role_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_role
    ADD CONSTRAINT sys_role_pkey PRIMARY KEY (role_id);


--
-- Name: sys_user sys_user_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_user
    ADD CONSTRAINT sys_user_pkey PRIMARY KEY (user_id);


--
-- Name: sys_user_post sys_user_post_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_user_post
    ADD CONSTRAINT sys_user_post_pkey PRIMARY KEY (user_id, post_id);


--
-- Name: sys_user_role sys_user_role_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_user_role
    ADD CONSTRAINT sys_user_role_pkey PRIMARY KEY (user_id, role_id);


--
-- Name: sys_dict_type unique_sys_dict_type_dt; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_dict_type
    ADD CONSTRAINT unique_sys_dict_type_dt UNIQUE (dict_type);


--
-- Name: dict_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX dict_type ON public.sys_dict_type USING btree (dict_type);


--
-- Name: flyway_schema_history_s_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX flyway_schema_history_s_idx ON public.flyway_schema_history USING btree (success);


--
-- Name: idx_sys_logininfor_lt; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_sys_logininfor_lt ON public.sys_logininfor USING btree (login_time);


--
-- Name: idx_sys_logininfor_s; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_sys_logininfor_s ON public.sys_logininfor USING btree (status);


--
-- Name: idx_sys_oper_log_bt; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_sys_oper_log_bt ON public.sys_oper_log USING btree (business_type);


--
-- Name: idx_sys_oper_log_ot; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_sys_oper_log_ot ON public.sys_oper_log USING btree (oper_time);


--
-- Name: idx_sys_oper_log_s; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_sys_oper_log_s ON public.sys_oper_log USING btree (status);


--
-- Name: sched_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sched_name ON public.qrtz_triggers USING btree (sched_name, job_name, job_group);


--
-- Name: qrtz_blob_triggers qrtz_blob_triggers_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qrtz_blob_triggers
    ADD CONSTRAINT qrtz_blob_triggers_ibfk_1 FOREIGN KEY (sched_name, trigger_name, trigger_group) REFERENCES public.qrtz_triggers(sched_name, trigger_name, trigger_group);


--
-- Name: qrtz_cron_triggers qrtz_cron_triggers_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qrtz_cron_triggers
    ADD CONSTRAINT qrtz_cron_triggers_ibfk_1 FOREIGN KEY (sched_name, trigger_name, trigger_group) REFERENCES public.qrtz_triggers(sched_name, trigger_name, trigger_group);


--
-- Name: qrtz_simple_triggers qrtz_simple_triggers_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qrtz_simple_triggers
    ADD CONSTRAINT qrtz_simple_triggers_ibfk_1 FOREIGN KEY (sched_name, trigger_name, trigger_group) REFERENCES public.qrtz_triggers(sched_name, trigger_name, trigger_group);


--
-- Name: qrtz_simprop_triggers qrtz_simprop_triggers_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qrtz_simprop_triggers
    ADD CONSTRAINT qrtz_simprop_triggers_ibfk_1 FOREIGN KEY (sched_name, trigger_name, trigger_group) REFERENCES public.qrtz_triggers(sched_name, trigger_name, trigger_group);


--
-- Name: qrtz_triggers qrtz_triggers_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qrtz_triggers
    ADD CONSTRAINT qrtz_triggers_ibfk_1 FOREIGN KEY (sched_name, job_name, job_group) REFERENCES public.qrtz_job_details(sched_name, job_name, job_group);


--
-- PostgreSQL database dump complete
--

