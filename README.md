<p align="center">
    <img alt="logo" src="public/faviconBaiZe.ico">
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">BaiZe-Vue-fast (RuoYi) v3.8.8</h1>
<h4 align="center">基于SpringBoot + Vue2 前后端分离的Java快速开发框架 后端项目</h4>
<p align="center">
	<a href="https://gitee.com/chaoscat/BaiZe-Vue-fast/stargazers"><img src="https://gitee.com/y_project/RuoYi-Vue/badge/star.svg?theme=dark"></a>
	<a href="https://gitee.com/chaoscat/BaiZe-Vue-fast"><img src="https://img.shields.io/badge/RuoYi-v3.8.8-brightgreen.svg"></a>
	<a href="https://gitee.com/chaoscat/BaiZe-Vue-fast/blob/master/LICENSE"><img src="https://img.shields.io/github/license/mashape/apistatus.svg"></a>
</p>

## BaiZe-Vue-fast
> **有用的话请鼓励❤️下作者，右上角☝️watch、star、fork三连点🙏🙏🙏一波**

## 🌵平台简介

基于 SpringBoot2（JDK1.8）+ MybatiPlus + SaToken + Postgresql12.4技术的前后端分离的一款简单、轻量级的 后台管理系统脚手架 。 内置用户管理、权限管理、部门管理、岗位管理、菜单管理、角色管理、字典管理、参数管理、应用监控、Api文档、前后端代码生成、动态定时任务等。

* 后端基于 [RuoYi-Vue-fast v3.8.8](https://github.com/yangzongzhuan/RuoYi-Vue-fast)
* 配套前端代码地址 [BaiZe-ui](https://gitee.com/chaoscat/BaiZe-ui)，技术栈（[Vue2](https://cn.vuejs.org) + [Element](https://github.com/ElemeFE/element) + [Vue CLI](https://cli.vuejs.org/zh)）。
* 支持加载动态权限菜单，多方式轻松权限控制。
* 高效率开发，使用代码生成器可以一键生成前后端代码。

**在原有框架的基础上，修改，增加了以下功能：** 
部分集成来源于 Ruoyi Vue 官方集成文档：[参考文档](http://doc.ruoyi.vip/ruoyi-vue/document/cjjc.html)
#### 🌴 功能集成

* **集成 [Sa-Token](https://sa-token.cc/) ：**  移除 Spring Security（再也不用百度各种博客了），控制逻辑更简单。
* **集成 [Hutool](https://hutool.cn/)：** 工具类一站式配齐，不用东查西挪。
* **集成 [Magic-Api](https://www.ssssssss.org/magic-api/)：** 在浏览器中就可以 编写接口，省去Controller、Service、DAO等繁琐代码，保存即发布，极大得提高了开发效率。
* **任务调度：** 可以使用 magic-api 的 job，也可以使用系统自带的 job。
* **集成 [Lombok](https://projectlombok.org/)：** 利用注解方式自动生成 java bean 中 getter、setter、equals 等方法，还能自动生成 logger、toString、hashCode、builder 等 日志相关变量、Object 类方法或设计模式相关的方法，能够让你的 代码更简洁，更美观。
* **集成 [Mybatis-Plus](https://baomidou.com//)：** 简化 CRUD 操作。
* **集成 [Yitter](https://github.com/yitter/idgenerator)：** 自增id改为雪花算法，使用 Yitter 可以生成更短的雪花ID，速度更快。
* **数据库：** 改为 Postgresql 12.4+ 。
* **集成 browscap 1.4.3：** 读取浏览器用户代理，识别浏览器版本，操作系统版本,解决第一次使用慢的问题。
* **前端集成 Watermark：** 实现页面添加水印。
* **前端集成 aj-captcha：** 实现滑块验证码。
* **集成 Undertow：** 替代 Tomcat 容器。
* **集成 [Jasypt](http://www.jasypt.org/)：** 配置文件敏感信息加密 ： [参考1](https://blog.csdn.net/yucaifu1989/article/details/124199659) 、 [参考2](https://www.jb51.net/program/287448sb4.htm) 、 [参考3](https://www.cnblogs.com/qcq0703/p/14232950.html)。
* **对接微信小程序：** 登录获取openid、设置昵称、设置头像。
* **集成 [Forest](https://forest.dtflyx.com)：** 极简 HTTP 调用 API 框架。
* **集成 [文档便利店(markdown/drawio)](https://gitee.com/Ning310975876/ruo-yi-vue-docHub)：** 支持world、excel、markdown、画图、思维导图、流程图等多种文档类型，支持基于 Markdown 的幻灯片制作、在线代码编写。接入大语言模型：ChatGPT-3.5、ChatGPT-4 （需要使用OpenAI KEY）。
* **集成 [ip2region](https://blog.csdn.net/putMap/article/details/137778395)：** 离线地址定位。
* **集成 redisson：** 实现redis分布式锁。
* **集成 jsencrypt：** 实现密码加密传输方式。
* **集成 [easyexcel](https://github.com/alibaba/easyexcel)：** 实现excel表格增强。
* **集成 minio：** 实现分布式文件存储。
* **自动填充：**  createBy、createTime、updateBy、updateTime 等字段自动填充。见 [CreateAndUpdateMetaObjectHandler.java](/src/main/java/com/ruoyi/framework/config/CreateAndUpdateMetaObjectHandler.java) 。
* **集成 flywaydb 数据库版本管理：** [教程1](https://blog.csdn.net/wx17343624830/article/details/127787074) / [教程2](https://www.jianshu.com/p/b2411239485f) / [教程3](https://documentation.red-gate.com/flyway/getting-started-with-flyway/getting-started) / [教程4](https://documentation.red-gate.com/fd/migrations-184127470.html) 。
* **Swagger2：** 补全所有实体的 ApiModel , 所有 Controller 增加 Swagger注解。
* **公告编辑器：** 提供markdown编辑器选项。
* **集成 [Knife4j替换swagger](https://blog.csdn.net/abu935009066/article/details/115512988)：** 基于knife4j-Openapi2 文档集成。
* **[Javamelody-应用程序监控](https://blog.csdn.net/abu935009066/article/details/116936366)：** 应用性能监控。

#### 插件版本

| 插件名称 | 版本 |更新日期|
| --- | --- | --- | 
| Sa-Token | 1.38.0 | |
| Hutool | 5.8.28 | 2024-7-2 |
| Magic-Api | 2.1.1 | |
| Mybatis-Plus | 3.5.7 | 2024-7-2|
| pagehelper | 2.1.0 | 2024-7-2|
| fastjson2 | 2.0.51 | 2024-7-2|
| Yitter | 1.0.6 | |
| Jasypt | 3.0.3 | |
| Forest | 1.5.36 | |
| easyexcel | 4.0.1 | 2024-7-2|
| minio | 8.2.1 | |
| knife4j | 4.4.0 | |
| javamelody | 1.99.0 | |


## 内置功能

1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3.  岗位管理：配置系统用户所属担任职务。
4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7.  参数管理：对系统动态配置常用参数。
8.  通知公告：系统通知公告信息发布维护。
9.  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
10. 登录日志：系统登录日志记录查询包含登录异常。
11. 在线用户：当前系统中活跃用户状态监控。
12. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
13. 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
14. 系统接口：根据业务代码自动生成相关的api接口文档。
15. 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
16. 缓存监控：对系统的缓存信息查询，命令统计等。
17. 在线构建器：拖动表单元素生成相应的HTML代码。
18. 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。
19. 文档便利店：支持world、excel、markdown、画图、思维导图、流程图等多种文档类型，支持基于 Markdown 的幻灯片制作、在线代码编写。

## 在线体验

- admin/admin123   

![](public/sysImg/pay.jpg)

## 演示图

<table>
    <tr> 
        <td><img src="public/sysImg/login.jpg"/></td>
        <td><img src="public/sysImg/login-captcha.png"/></td>
    </tr>
    <tr> 
    <td><img src="public/sysImg/desktop.jpg"/></td>
    <td><img src="public/sysImg/monitoring.png"/></td>
    </tr>
    <tr> 
    <td><img src="public/sysImg/knife4j.png"/></td>
    <td><img src="public/sysImg/magicapi.png"/></td>
    </tr>
    <tr>
        <td><img src="public/sysImg/txt.jpg"/></td>
        <td><img src="public/sysImg/markdown.jpg"/></td>
    </tr>
    <tr>
        <td><img src="public/sysImg/world.jpg"/></td>
        <td><img src="public/sysImg/excel.jpg"/></td>
    </tr>
    <tr>
        <td><img src="public/sysImg/ppt.jpg"/></td>
        <td><img src="public/sysImg/img.jpg"/></td>
    </tr>
    <tr>
        <td><img src="public/sysImg/xmaind.jpg"/></td>
        <td><img src="public/sysImg/bpmn.jpg"/></td>
    </tr>
    <tr>
        <td><img src="public/sysImg/code.jpg"/></td>
        <td><img src="public/sysImg/map.jpg"/></td>
    </tr>
    <tr>
        <td><img src="public/sysImg/openai.jpg"/></td>
        <td><img src="public/sysImg/toolbox.jpg"/></td>
    </tr>
    <tr>
        <td><img src="public/sysImg/svg-edit.jpg"/></td>
        <td><img src="public/sysImg/drawio.jpg"/></td>
    </tr>
    <tr>
        <td><img src="public/sysImg/notice.png"/></td>
        <td><img src="public/sysImg/echarts.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-8074972883b5ba0622e13246738ebba237a.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-9f88719cdfca9af2e58b352a20e23d43b12.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-39bf2584ec3a529b0d5a3b70d15c9b37646.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-936ec82d1f4872e1bc980927654b6007307.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-b2d62ceb95d2dd9b3fbe157bb70d26001e9.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-d67451d308b7a79ad6819723396f7c3d77a.png"/></td>
    </tr>	 
    <tr>
        <td><img src="public/sysImg/profile.png"/></td>
        <td><img src="public/sysImg/avatar.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-8370a0d02977eebf6dbf854c8450293c937.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-49003ed83f60f633e7153609a53a2b644f7.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-d4fe726319ece268d4746602c39cffc0621.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-c195234bbcd30be6927f037a6755e6ab69c.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/b6115bc8c31de52951982e509930b20684a.jpg"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-5e4daac0bb59612c5038448acbcef235e3a.png"/></td>
    </tr>
</table>


## 若依前后端分离交流群

QQ群： [![加入QQ群](https://img.shields.io/badge/已满-937441-blue.svg)](https://jq.qq.com/?_wv=1027&k=5bVB1og) [![加入QQ群](https://img.shields.io/badge/已满-887144332-blue.svg)](https://jq.qq.com/?_wv=1027&k=5eiA4DH) [![加入QQ群](https://img.shields.io/badge/已满-180251782-blue.svg)](https://jq.qq.com/?_wv=1027&k=5AxMKlC) [![加入QQ群](https://img.shields.io/badge/已满-104180207-blue.svg)](https://jq.qq.com/?_wv=1027&k=51G72yr) [![加入QQ群](https://img.shields.io/badge/已满-186866453-blue.svg)](https://jq.qq.com/?_wv=1027&k=VvjN2nvu) [![加入QQ群](https://img.shields.io/badge/已满-201396349-blue.svg)](https://jq.qq.com/?_wv=1027&k=5vYAqA05) [![加入QQ群](https://img.shields.io/badge/已满-101456076-blue.svg)](https://jq.qq.com/?_wv=1027&k=kOIINEb5) [![加入QQ群](https://img.shields.io/badge/已满-101539465-blue.svg)](https://jq.qq.com/?_wv=1027&k=UKtX5jhs) [![加入QQ群](https://img.shields.io/badge/已满-264312783-blue.svg)](https://jq.qq.com/?_wv=1027&k=EI9an8lJ) [![加入QQ群](https://img.shields.io/badge/已满-167385320-blue.svg)](https://jq.qq.com/?_wv=1027&k=SWCtLnMz) [![加入QQ群](https://img.shields.io/badge/已满-104748341-blue.svg)](https://jq.qq.com/?_wv=1027&k=96Dkdq0k) [![加入QQ群](https://img.shields.io/badge/已满-160110482-blue.svg)](https://jq.qq.com/?_wv=1027&k=0fsNiYZt) [![加入QQ群](https://img.shields.io/badge/已满-170801498-blue.svg)](https://jq.qq.com/?_wv=1027&k=7xw4xUG1) [![加入QQ群](https://img.shields.io/badge/已满-108482800-blue.svg)](https://jq.qq.com/?_wv=1027&k=eCx8eyoJ) [![加入QQ群](https://img.shields.io/badge/已满-101046199-blue.svg)](https://jq.qq.com/?_wv=1027&k=SpyH2875) [![加入QQ群](https://img.shields.io/badge/已满-136919097-blue.svg)](https://jq.qq.com/?_wv=1027&k=tKEt51dz) [![加入QQ群](https://img.shields.io/badge/已满-143961921-blue.svg)](http://qm.qq.com/cgi-bin/qm/qr?_wv=1027&k=0vBbSb0ztbBgVtn3kJS-Q4HUNYwip89G&authKey=8irq5PhutrZmWIvsUsklBxhj57l%2F1nOZqjzigkXZVoZE451GG4JHPOqW7AW6cf0T&noverify=0&group_code=143961921) [![加入QQ群](https://img.shields.io/badge/已满-174951577-blue.svg)](http://qm.qq.com/cgi-bin/qm/qr?_wv=1027&k=ZFAPAbp09S2ltvwrJzp7wGlbopsc0rwi&authKey=HB2cxpxP2yspk%2Bo3WKTBfktRCccVkU26cgi5B16u0KcAYrVu7sBaE7XSEqmMdFQp&noverify=0&group_code=174951577) [![加入QQ群](https://img.shields.io/badge/已满-161281055-blue.svg)](http://qm.qq.com/cgi-bin/qm/qr?_wv=1027&k=Fn2aF5IHpwsy8j6VlalNJK6qbwFLFHat&authKey=uyIT%2B97x2AXj3odyXpsSpVaPMC%2Bidw0LxG5MAtEqlrcBcWJUA%2FeS43rsF1Tg7IRJ&noverify=0&group_code=161281055) [![加入QQ群](https://img.shields.io/badge/已满-138988063-blue.svg)](http://qm.qq.com/cgi-bin/qm/qr?_wv=1027&k=XIzkm_mV2xTsUtFxo63bmicYoDBA6Ifm&authKey=dDW%2F4qsmw3x9govoZY9w%2FoWAoC4wbHqGal%2BbqLzoS6VBarU8EBptIgPKN%2FviyC8j&noverify=0&group_code=138988063) [![加入QQ群](https://img.shields.io/badge/151450850-blue.svg)](http://qm.qq.com/cgi-bin/qm/qr?_wv=1027&k=DkugnCg68PevlycJSKSwjhFqfIgrWWwR&authKey=pR1Pa5lPIeGF%2FFtIk6d%2FGB5qFi0EdvyErtpQXULzo03zbhopBHLWcuqdpwY241R%2F&noverify=0&group_code=151450850) 点击按钮入群。